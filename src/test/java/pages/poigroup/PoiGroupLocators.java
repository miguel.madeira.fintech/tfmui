package pages.poigroup;

import org.openqa.selenium.By;

public class PoiGroupLocators {

    public static final By NO_POI_GROUPS_ARE_DEFINED_MESSAGE = By.xpath("//div[text()='No POI Groups are defined']");

    public static final By BUTTON_ADD_POI_GROUP = By.cssSelector("button[data-automation-id='btnPoiGroupAdd']");

    public static final By DIV_POI_GROUP_LIST_HEADER = By.cssSelector("div[data-automation-id='poiGroup-list-header']");

    public static final By POI_GROUP_ALREADY_EXISTS_ERROR_MESSAGE =
            By.xpath("//p[text()='Invalid input: This poi group already exists']");

    public static final By BUTTON_REMOVE_POI_GROUP = By.cssSelector("button[data-automation-id='btnPoiGroupDelete']");

    public static final By DIV_POP_UP_DELETE_POI_GROUP = By.id("alert-dialog-description");

    public static final By DIV_POI_GROUP_NAME = By.cssSelector("div[data-automation-id='poiGroup-cell-1']");


}