package pages.poigroup;

import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class PoiGroupWebElements {

    private DriverHelper driverHelper;

    public PoiGroupWebElements(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement noPoiGroupsAreDefinedMessage() throws IOException {
        return driverHelper.createWebElement(PoiGroupLocators.NO_POI_GROUPS_ARE_DEFINED_MESSAGE);
    }

    public WebElement buttonAddPoiGroup() throws IOException {
        return driverHelper.createWebElement(PoiGroupLocators.BUTTON_ADD_POI_GROUP);
    }

    public WebElement divPoiGroupListHeader() throws IOException {
        return driverHelper.createWebElement(PoiGroupLocators.DIV_POI_GROUP_LIST_HEADER);
    }

    public WebElement poiGroupAlreadyExistsErrorMessage() throws IOException {
        return driverHelper.createWebElement(PoiGroupLocators.POI_GROUP_ALREADY_EXISTS_ERROR_MESSAGE);
    }

    public WebElement buttonRemovePoiGroup() throws IOException {
        return driverHelper.createWebElement(PoiGroupLocators.BUTTON_REMOVE_POI_GROUP);
    }

    public WebElement divPopUpDeletePoiGroup() throws IOException {
        return driverHelper.createWebElement(PoiGroupLocators.DIV_POP_UP_DELETE_POI_GROUP);
    }

    public List<WebElement> divPoiGroupNameList() throws IOException {
        return driverHelper.createListWebElement(PoiGroupLocators.DIV_POI_GROUP_NAME);
    }

}