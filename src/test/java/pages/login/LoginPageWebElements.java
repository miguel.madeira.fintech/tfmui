package pages.login;

import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class LoginPageWebElements {

    private DriverHelper driverHelper;

    public LoginPageWebElements(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement inputUserName() throws IOException {
        return driverHelper.createWebElement(LoginPageLocators.INPUT_USERNAME);
    }

    public WebElement inputPassword() throws IOException {
        return driverHelper.createWebElement(LoginPageLocators.INPUT_PASSWORD);
    }

    public WebElement buttonLogin() throws IOException {
        return driverHelper.createWebElement(LoginPageLocators.BUTTON_LOGIN);
    }
}