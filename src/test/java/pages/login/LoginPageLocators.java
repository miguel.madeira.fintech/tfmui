package pages.login;

import org.openqa.selenium.By;

public class LoginPageLocators {

    public static final By INPUT_USERNAME = By.cssSelector("input[name='username']");

    public static final By INPUT_PASSWORD = By.cssSelector("input[name='password']");

    public static final By BUTTON_LOGIN = By.xpath("//button[text()='Login']");
}