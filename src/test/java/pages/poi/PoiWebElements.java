package pages.poi;

import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class PoiWebElements {

    private DriverHelper driverHelper;

    public PoiWebElements(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement divComponentContent() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_COMPONENT_CONTENT);
    }

    public WebElement divTabsContent() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_TABS_CONTENT);
    }

    public WebElement buttonDetailsAndProperties() throws IOException {
        return driverHelper.createWebElement(PoiLocators.BUTTON_DETAILS_AND_PROPERTIES);
    }

    public WebElement buttonParameters() throws IOException {
        return driverHelper.createWebElement(PoiLocators.BUTTON_PARAMETERS);
    }

    public WebElement buttonComponentsAndCapabilities() throws IOException {
        return driverHelper.createWebElement(PoiLocators.BUTTON_COMPONENTS_AND_CAPABILITIES);
    }

    public WebElement divCreated() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_CREATED);
    }

    public WebElement divModified() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_MODIFIED);
    }

    public WebElement divFirstPoiInList() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_FIRST_POI_IN_LIST);
    }

    public WebElement divMerchantPin() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_MERCHANT_PIN);
    }

    public WebElement divMerchantPinDefaultPolicy() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_MERCHANT_PIN_DEFAULT_POLICY);
    }

    public WebElement divPoiIdInformation() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_ID_INFORMATION);
    }

    public WebElement divPoiState() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_STATE);
    }

    public WebElement divPoiConfigStatus() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_CONFIG_STATUS);
    }

    public WebElement divPoiEnable() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_ENABLE);
    }

    public WebElement divPoiDelete() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_DELETE);
    }

    public WebElement divPoiDetailsR1() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_DETAILS_R1);
    }

    public WebElement divPoiDetailsR2() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_DETAILS_R2);
    }

    public WebElement divPoiDetailsR3() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_DETAILS_R3);
    }

    public WebElement divPoiDetailsR4() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_DETAILS_R4);
    }

    public List<WebElement> divMerchantPinEditableContent() {
        return driverHelper.createListWebElement(PoiLocators.DIV_MERCHANT_PIN_EDITABLE_CONTENT);
    }

    public WebElement divMerchantPinEditableContentSpanSave() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_MERCHANT_PIN_EDITABLE_CONTENT_SPAN_SAVE);
    }

    public WebElement divMerchantPinEditableContentSpanCancel() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_MERCHANT_PIN_EDITABLE_CONTENT_SPAN_CANCEL);
    }

    public List<WebElement> divsEditableContentText(){
        return driverHelper.createListWebElement(PoiLocators.DIV_MERCHANT_PIN_EDITABLE_CONTENT_TEXT);
    }

    public WebElement buttonDeployDisabled() throws IOException {
        return driverHelper.createWebElement(PoiLocators.BUTTON_DEPLOY_DISABLED);
    }

    public WebElement buttonDeploy() throws IOException {
        return driverHelper.createWebElement(PoiLocators.BUTTON_DEPLOY);
    }

    public List<WebElement> inputSelectPoiList(){
        return driverHelper.createListWebElement(PoiLocators.INPUT_SELECT_POI);
    }

    public WebElement divDeployPopUpText() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_DEPLOY_POP_UP_TEXT);
    }

    public List <WebElement> buttonsDeployPopUp(){
        return driverHelper.createListWebElement(PoiLocators.BUTTONS_DEPLOY_POI_POP_UP);
    }

    public List<WebElement> divPoiConfigurationStatusTable(){
        return driverHelper.createListWebElement(PoiLocators.DIV_POI_CONFIGURATION_STATUS_TABLE);
    }

    public List<WebElement> divPoiPoiIdTable(){
        return driverHelper.createListWebElement(PoiLocators.DIV_POI_POI_ID_TABLE);
    }

    public WebElement textInvalidInputRequiredField() throws IOException {
        return driverHelper.createWebElement(PoiLocators.TEXT_INVALID_INPUT);
    }

    public WebElement divPoiLabel() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_LABEL);
    }

    public List<WebElement> divPoiLabelDropdown(){
        return driverHelper.createListWebElement(PoiLocators.DIV_POI_LABEL_DROPDOWN);
    }

    public List<WebElement> divAllLabels(){
        return driverHelper.createListWebElement(PoiLocators.DIV_ALL_LABELS);
    }

    public WebElement textInvalidInputOnlyDigitsAreAllowed() throws IOException {
        return driverHelper.createWebElement(PoiLocators.TEXT_INVALID_INPUT_ONLY_DIGITS_ARE_ALLOWED);
    }

    public WebElement buttonDeletePoi() throws IOException {
        return driverHelper.createWebElement(PoiLocators.BUTTON_POI_DELETE);
    }

    public WebElement spanSelectAll() throws IOException {
        return driverHelper.createWebElement(PoiLocators.SPAN_SELECT_ALL);
    }

    public WebElement divPoiEnabled() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_POI_POI_ID_TABLE_ENABLED);
    }

    public List<WebElement> divPoiEnabledList() {
        return driverHelper.createListWebElement(PoiLocators.DIV_POI_POI_ID_TABLE_ENABLED);
    }

    public WebElement divNotes() throws IOException {
        return driverHelper.createWebElement(PoiLocators.DIV_NOTES);
    }

    public WebElement textInvalidInpuSize() throws IOException {
        return driverHelper.createWebElement(PoiLocators.TEXT_INVALID_INPUT_SIZE);
    }
}