package pages.poi;

import org.openqa.selenium.By;

public class PoiLocators {

    public static final By DIV_COMPONENT_CONTENT = By.cssSelector("div[data-automation-id='poi-tab']");

    public static final By DIV_TABS_CONTENT = By.cssSelector("div[data-automation-id='tabs-poi-tab']");

    public static final By BUTTON_DETAILS_AND_PROPERTIES =
            By.cssSelector("button[data-automation-id='Details & properties-poi-tab']");

    public static final By BUTTON_PARAMETERS = By.cssSelector("button[data-automation-id='Parameters-poi-tab']");

    public static final By BUTTON_COMPONENTS_AND_CAPABILITIES =
            By.cssSelector("button[data-automation-id='Components & Capabilities-poi-tab']");

    public static final By DIV_CREATED = By.cssSelector("div[data-automation-id='Created']");

    public static final By DIV_MODIFIED = By.cssSelector("div[data-automation-id='Created']");

    public static final By DIV_FIRST_POI_IN_LIST = By.cssSelector("div[data-automation-id='poi-row-0']");

    public static final By DIV_MERCHANT_PIN = By.cssSelector("div[data-automation-id='MerchantPINValue']");

    public static final By DIV_MERCHANT_PIN_DEFAULT_POLICY = By.cssSelector("div[data-automation-id='MerchantPINDefaultPolicy']");

    public static final By DIV_POI_ID_INFORMATION = By.cssSelector("div[data-automation-id='poi-id']");

    public static final By DIV_POI_STATE = By.cssSelector("div[data-automation-id='poi-state']");

    public static final By DIV_POI_CONFIG_STATUS = By.cssSelector("div[data-automation-id='poi-config-status']");

    public static final By DIV_POI_ENABLE = By.cssSelector("div[data-automation-id='poi-enable']");

    public static final By DIV_POI_DELETE = By.cssSelector("div[data-automation-id='poi-delete']");

    public static final By DIV_POI_DETAILS_R1 = By.cssSelector("div[data-automation-id='R1']");

    public static final By DIV_POI_DETAILS_R2 = By.cssSelector("div[data-automation-id='R2']");

    public static final By DIV_POI_DETAILS_R3 = By.cssSelector("div[data-automation-id='R3']");

    public static final By DIV_POI_DETAILS_R4 = By.cssSelector("div[data-automation-id='R4']");

    public static final By DIV_MERCHANT_PIN_EDITABLE_CONTENT = By.cssSelector("div[class$='multiline']");

    public static final By DIV_MERCHANT_PIN_EDITABLE_CONTENT_SPAN_SAVE = By.xpath("//span[text()='Save']");

    public static final By DIV_MERCHANT_PIN_EDITABLE_CONTENT_SPAN_CANCEL = By.xpath("//span[text()='Cancel']");

    public static final By DIV_MERCHANT_PIN_EDITABLE_CONTENT_TEXT = By.cssSelector("textarea[class$='jss27']");

    public static final By BUTTON_DEPLOY_DISABLED = By.cssSelector("button[class$='Mui-disabled']");

    public static final By BUTTON_DEPLOY = By.xpath("//span[text()='Deploy']");

    public static final By INPUT_SELECT_POI = By.cssSelector("input[type='checkbox']");

    public static final By DIV_DEPLOY_POP_UP_TEXT = By.id("alert-dialog-description");

    public static final By BUTTONS_DEPLOY_POI_POP_UP = By.cssSelector("button[class$='gRhvOc']");

    public static final By DIV_POI_CONFIGURATION_STATUS_TABLE = By.cssSelector("div[data-automation-id='poi-cell-6']");

    public static final By DIV_POI_POI_ID_TABLE = By.cssSelector("div[data-automation-id='poi-cell-1']");

    public static final By TEXT_INVALID_INPUT = By.xpath("//p[text()='Invalid input: Required Field']");

    public static final By TEXT_INVALID_INPUT_ONLY_DIGITS_ARE_ALLOWED = By.xpath("//p[text()='Invalid input: Only digits are allowed']");

    public static final By DIV_POI_LABEL = By.cssSelector("div[data-automation-id='label-multi-select']");

    public static final By DIV_POI_LABEL_DROPDOWN = By.cssSelector("ul[class$='MuiList-padding']");

    public static final By DIV_ALL_LABELS = By.cssSelector("div[data-automation-id='poi-cell-4']");

    public static final By BUTTON_POI_DELETE = By.cssSelector("button[data-automation-id='poi-delete']");

    public static final By SPAN_SELECT_ALL = By.xpath("//span[text()='Select all']");

    public static final By DIV_POI_POI_ID_TABLE_ENABLED = By.cssSelector("div[data-automation-id='poi-cell-5']");

    public static final By DIV_NOTES = By.cssSelector("div[data-automation-id='editable-form-editable-detail-notes']");

    public static final By TEXT_INVALID_INPUT_SIZE = By.xpath("//p[text()='Invalid input: Max length is 512 chars']");



}