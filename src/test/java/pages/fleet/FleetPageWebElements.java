package pages.fleet;

import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class FleetPageWebElements {

    private DriverHelper driverHelper;

    public FleetPageWebElements(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement buttonAddFleet() throws IOException {
        return driverHelper.createWebElement(FleetPageLocators.BUTTON_ADD_FLEET);
    }

    public WebElement buttonCloseFleet() throws IOException {
        return driverHelper.createWebElement(FleetPageLocators.BUTTON_CLOSE_FLEET);
    }
}