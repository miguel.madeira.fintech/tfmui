package pages.fleet;

import org.openqa.selenium.By;

public class FleetPageLocators {

    public static final By BUTTON_ADD_FLEET = By.cssSelector("button[data-automation-id='btnFleetAdd']");

    public static final By BUTTON_CLOSE_FLEET = By.cssSelector("button[data-automation-id='createFleetDialog-modal-close']");
}