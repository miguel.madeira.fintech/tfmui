package pages.fleetdetails;

import org.openqa.selenium.By;

public class FleetDetailsLocators {

    public static final By BUTTON_ADD_STORE_GROUP = By.cssSelector("button[class$='hyecnm']");

    public static final By DIV_NO_STORE_GROUP = By.xpath("//div[contains(string(), 'No Store Groups are defined')]");

    public static final By DIV_NR_OF_STORE_GROUPS = By.cssSelector("div[data-automation-id='fleetNrStoreGroups']");

    public static final By CELL_STORE_GROUPS = By.cssSelector("div[data-automation-id^='store-group-row-']");

}


