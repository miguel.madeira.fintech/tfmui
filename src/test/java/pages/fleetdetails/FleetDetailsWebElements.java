package pages.fleetdetails;

import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class FleetDetailsWebElements {

    private DriverHelper driverHelper;

    public FleetDetailsWebElements(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement buttonAddStoreGroup() throws IOException {
        return driverHelper.createWebElement(FleetDetailsLocators.BUTTON_ADD_STORE_GROUP);
    }

    public WebElement divNoStoreGroup() throws IOException {
        return driverHelper.createWebElement(FleetDetailsLocators.DIV_NO_STORE_GROUP);
    }

    public WebElement divNrStoreGroup() throws IOException {
        return driverHelper.createWebElement(FleetDetailsLocators.DIV_NR_OF_STORE_GROUPS);
    }

    public List<WebElement> cellStoreGroupRow(){
        return driverHelper.createListWebElement(FleetDetailsLocators.CELL_STORE_GROUPS);
    }
}