package pages.storedetails;

import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class StoreDetailsWebElements {

    private DriverHelper driverHelper;

    public StoreDetailsWebElements(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement divStoreName() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_STORE_NAME);
    }

    public WebElement divStoreGroupName() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_STORE_GROUP_NAME);
    }

    public WebElement divFleet() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_FLEET_NAME);
    }

    public WebElement divPoiProfile() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_POI_PROFILE);
    }

    public WebElement divRetailer() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_RETAILER);
    }

    public WebElement divNumberOfPoiS() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_NUMBER_OF_POI_S);
    }

    public WebElement divCreatedDate() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_CREATED_DATE);
    }

    public WebElement divModifiedDate() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_MODIFIED_DATE);
    }

    public WebElement divNotes() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_NOTES);
    }

    public WebElement divNoPoiSAreDefined() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_NO_POI_S_ARE_DEFINED);
    }

    public WebElement buttonAddPoi() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.BUTTON_ADD_POI);
    }

    public WebElement divStoreAcceptanceServiceContracts() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.DIV_STORE_ACCEPTANCE_SERVICE_CONTRACTS);
    }

    public WebElement buttonEditStore() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.BUTTON_EDIT_STORE);
    }

    public WebElement buttonAddMid() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.BUTTON_ADD_MID);
    }

    public List<WebElement> buttonAddMidInList() {
        return driverHelper.createListWebElement(StoreDetailsLocators.BUTTON_ADD_MID_IN_LIST);
    }

    public WebElement buttonRemoveAcceptanceServiceContract() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.BUTTON_REMOVE_ACCEPTANCE_SERVICE_CONTRACT);
    }

    public WebElement popUpRemoveMid() throws IOException {
        return driverHelper.createWebElement(StoreDetailsLocators.POP_UP_REMOVE_MID);
    }
}