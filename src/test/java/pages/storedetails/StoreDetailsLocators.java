package pages.storedetails;

import org.openqa.selenium.By;

public class StoreDetailsLocators {
    public static final By DIV_STORE_NAME = By.cssSelector("div[data-automation-id='StoreName']");

    public static final By DIV_STORE_GROUP_NAME = By.cssSelector("div[data-automation-id='StoreGroupName']");

    public static final By DIV_FLEET_NAME = By.cssSelector("div[data-automation-id='Fleet001']");

    public static final By DIV_POI_PROFILE = By.cssSelector("div[data-automation-id='storePoiProfile']");

    public static final By DIV_RETAILER = By.cssSelector("div[data-automation-id='fleetRetailer']");

    public static final By DIV_NUMBER_OF_POI_S = By.cssSelector("div[data-automation-id='storeNrOfPois']");

    public static final By DIV_CREATED_DATE = By.cssSelector("div[data-automation-id='storeCreated']");

    public static final By DIV_MODIFIED_DATE = By.cssSelector("div[data-automation-id='storeModifed']");

    public static final By DIV_NOTES = By.cssSelector("div[data-automation-id='storeNotes']");

    public static final By DIV_NO_POI_S_ARE_DEFINED = By.xpath("//div[text()='No POIs are defined']");

    public static final By DIV_STORE_ACCEPTANCE_SERVICE_CONTRACTS =
            By.cssSelector("div[data-automation-id='acceptanceServiceContracts']");

    public static final By BUTTON_EDIT_STORE = By.cssSelector("button[class$='Mui-disabled']");

    public static final By BUTTON_ADD_POI = By.xpath("//span[text()='ADD POI']");

    public static final By BUTTON_ADD_MID = By.xpath("//span[text()='ADD MID']");

    public static final By BUTTON_ADD_MID_IN_LIST = By.cssSelector("button[class^='MuiButtonBase-root MuiButton-root']");

    public static final By BUTTON_REMOVE_ACCEPTANCE_SERVICE_CONTRACT =
            By.cssSelector("div[data-automation-id='acceptance-services-cell-3']");

    public static final By POP_UP_REMOVE_MID = By.xpath("//p[text()='Are you sure you want remove this MID?']");
}