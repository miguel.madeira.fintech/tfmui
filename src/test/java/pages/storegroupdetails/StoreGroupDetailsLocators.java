package pages.storegroupdetails;

import org.openqa.selenium.By;

public class StoreGroupDetailsLocators {
    public static final By DIV_STORE_GROUP_NAME = By.cssSelector("div[data-automation-id='StoreGroupname']");

    public static final By DIV_FLEET = By.cssSelector("div[data-automation-id='Fleet001']");

    public static final By DIV_POI_PROFILE = By.cssSelector("div[data-automation-id='fleetPoiProfile']");

    public static final By DIV_RETAILER = By.cssSelector("div[data-automation-id='fleetRetailer']");

    public static final By DIV_NUMBER_OF_STORES = By.cssSelector("div[data-automation-id='Nr_of_stores']");

    public static final By DIV_NUMBER_OF_POIS = By.cssSelector("div[data-automation-id='storeGroupNrOfPois']");

    public static final By DIV_CREATED_DATE = By.cssSelector("div[data-automation-id='storeGroupCreated']");

    public static final By DIV_MODIFIED_DATE = By.cssSelector("div[data-automation-id='storeGroupModifed']");

    public static final By DIV_NOTES = By.cssSelector("div[data-automation-id='fleetNotes']");

    public static final By DIV_NO_STORES_ARE_DEFINED = By.xpath("//div[text()='No Stores are defined']");

    public static final By BUTTON_ADD_STORE = By.xpath("//span[text()='ADD STORE']");

    public static final By DIV_NR_OF_STORES = By.cssSelector("div[data-automation-id='Nr_of_stores']");

    public static final By BUTTON_ADD_STORE_GROUP = By.xpath("//span[text()='ADD STORE GROUP']");

    public static final By CELL_NUMBER_OF_STORES =  By.cssSelector("div[data-automation-id^='store-row-']");


}