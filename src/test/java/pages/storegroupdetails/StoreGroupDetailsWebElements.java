package pages.storegroupdetails;

import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class StoreGroupDetailsWebElements {
    private DriverHelper driverHelper;

    public StoreGroupDetailsWebElements(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement divStoreGroupName() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_STORE_GROUP_NAME);
    }

    public WebElement divFleet() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_FLEET);
    }

    public WebElement divPoiProfile() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_POI_PROFILE);
    }

    public WebElement divRetailer() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_RETAILER);
    }

    public WebElement divNumberOfStores() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_NUMBER_OF_STORES);
    }

    public WebElement divNumberOfPois() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_NUMBER_OF_POIS);
    }

    public WebElement divCreatedDate() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_CREATED_DATE);
    }

    public WebElement divModifiedDate() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_MODIFIED_DATE);
    }

    public WebElement divNotes() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_NOTES);
    }

    public WebElement buttonAddStore() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.BUTTON_ADD_STORE);
    }

    public WebElement buttonAddStoreGroup() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.BUTTON_ADD_STORE_GROUP);
    }
    public WebElement divNoStoresAreDefined() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_NO_STORES_ARE_DEFINED);
    }

    public WebElement divNrStores() throws IOException {
        return driverHelper.createWebElement(StoreGroupDetailsLocators.DIV_NR_OF_STORES);
    }

    public List<WebElement> cellNrOfStores() {
        return driverHelper.createListWebElement(StoreGroupDetailsLocators.CELL_NUMBER_OF_STORES);
    }
}