package drivermanager;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;


import java.util.HashMap;
import java.util.logging.Level;

public class ChromeDriverManager extends DriverManager {

   // String chromePath;

   // public ChromeDriverManager(String chromePath){
      //  this.chromePath = chromePath;
   // }

    @Override
    public void createDriver() {
     //   System.setProperty("webdriver.chrome.driver", chromePath);

        HashMap<String, Object> chromePrefs = new HashMap<>();

        chromePrefs.put("profile.default_content_settings.popups", 0);

        chromePrefs.put("safebrowsing.enabled", "true");

        chromePrefs.put("download.prompt_for_download", "false");

        ChromeOptions options = new ChromeOptions().setHeadless(true);

        options.addArguments("--disable-extensions");

        options.addArguments("--disable-popup-blocking");

        options.addArguments("--disable-default-apps");

        options.addArguments("--enable-automation");

        options.addArguments("--disable-infobars");

        options.addArguments("--disable-extensions");

        options.addArguments("--disable-gpu");

        options.addArguments("--safebrowsing-disable-download-protection");

        options.setExperimentalOption("useAutomationExtension", false);

        options.setExperimentalOption("prefs", chromePrefs);

        DesiredCapabilities caps = DesiredCapabilities.chrome();

        LoggingPreferences logPrefs = new LoggingPreferences();

        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);

        options.setCapability("goog:loggingPrefs", logPrefs);

        logPrefs.enable(LogType.BROWSER, Level.ALL);

        driver = new ChromeDriver(options);

        driver.manage().window().maximize();
    }
}