package drivermanager;

import org.openqa.selenium.WebDriver;

import java.io.IOException;

public abstract class DriverManager {

    protected WebDriver driver;

    protected abstract void createDriver() throws IOException;

    public void quitDriver() {
        if (null != driver) {
            driver.quit();
            driver = null;
        }

    }

    public WebDriver getDriver() throws IOException {
        if (null == driver) {
            createDriver();
        }
        return driver;
    }
}