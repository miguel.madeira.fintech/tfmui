package drivermanager;

import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverManager extends DriverManager {

    String firefoxPath;

    public FirefoxDriverManager(String firefoxPath){
        this.firefoxPath = firefoxPath;
    }

    @Override
    public void createDriver(){
        System.setProperty("webdriver.gecko.driver", firefoxPath);

        driver =  new FirefoxDriver();

        driver.manage().window().maximize();
    }
}