package fakermanager;

public interface IFakerManager {

    String getValidSolutionContractName();

    String getInvalidSolutionContractName();

    String getValidFleetName();

    String getValidNote();

    String getValidStoreGroupName();

    String getValidStoreGroupNameWithOneCharacter();

    String getValidStoreName();

    String getInvalidStoreName();

    String getValidMerchantIdWithAlphanumeric();

    String getValidMerchantCategoryCode();

    String getValidMerchantNameAndLocation();

    String getValidAcquiredId();

    String getInvalidMerchantId();

    String getInvalidMerchantCategoryCode();

    String getPoiGroupName();

    String getValidLabelName();

    String getInvalidNote();

    String getValidUsername();

}