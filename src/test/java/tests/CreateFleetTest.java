package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ModalCommonsComponent;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;

import java.io.IOException;
import java.util.NoSuchElementException;

public class CreateFleetTest extends NewWindow {

    //Tests from ff9fa_2638 to ff9fa_2644 related to US-2470

    private static FleetSteps fleetSteps;

    private static CommonSteps commonSteps;

    private static IFakerManager iFakerManager;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        Properties properties = iReadProperties.readFromYaml();

        iFakerManager = new FakerManager();

        fleetSteps = new FleetSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);
    }

    @BeforeEach
    public void initialTestConditions() throws IOException, InterruptedException {
        fleetSteps.clickButtonAddFleet();

        iHelpers.sleepOneSecond();
    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepOneSecond();
    }

    @Test
    @DisplayName("Validate fleet modal component")
    void ff9fa_2638() throws IOException {
        Assertions.assertTrue(fleetSteps.validateCreateFleetModal(), "It was expected true");

        fleetSteps.clickButtonCloseModalDialogFleet();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Click button cancel after informing the fields in modal component")
    void ff9fa_2639() throws IOException, InterruptedException {
        fleetSteps.insertFieldsCreateFleetModal(iFakerManager.getValidFleetName(),"");

        commonSteps.clickButtonCancelInModal();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate click on 'X' button after informing the fields in modal component")
    void ff9fa_2640() throws IOException, InterruptedException {
        fleetSteps.insertFieldsCreateFleetModal(iFakerManager.getValidFleetName(),"");

        fleetSteps.clickButtonCloseModalDialogFleet();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate trying to create a new fleet with empty fleet name")
    void ff9fa_2641() throws IOException, InterruptedException {
        fleetSteps.insertFieldsCreateFleetModal("","");

        commonSteps.clickButtonCreateInModal();

        Assertions.assertEquals("Invalid input: Required Field", commonSteps.getTextRequiredFieldMessage(),
                "It was expected the same message");

        Assertions.assertTrue(commonSteps.validateRequiredField(), "It was expected displayed and enabled");

        fleetSteps.clickButtonCloseModalDialogFleet();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate trying to create a new fleet with only one character on fleet name")
    void ff9fa_2642() throws IOException, InterruptedException {
        fleetSteps.insertFieldsCreateFleetModal("a","");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals("Invalid input: Min length is 3 chars",
                commonSteps.getTextModalFeedbackErrorMessageMinLength3());

        fleetSteps.clickButtonCloseModalDialogFleet();
    }

    @Test
    @DisplayName("Create new fleet successfully without informing a notes")
    void ff9fa_2643() throws IOException, InterruptedException {
        String fleetName = iFakerManager.getValidFleetName();

        fleetSteps.insertFieldsCreateFleetModal(fleetName,"");

        iHelpers.sleepOneSecond();

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));
    }

    @Test
    @DisplayName("Create new fleet successfully")
    void ff9fa_2644() throws IOException, InterruptedException {
        String fleetName = iFakerManager.getValidFleetName();

        iHelpers.sleepTwoSeconds();

        fleetSteps.insertFieldsCreateFleetModal(fleetName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));
    }

    @Test
    @DisplayName("Delete fleet successfully")
    void ff9fa_3704() throws IOException, InterruptedException {
        String fleetName = iFakerManager.getValidFleetName();

        iHelpers.sleepTwoSeconds();

        fleetSteps.insertFieldsCreateFleetModal(fleetName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName),
                "It was expected to validate fleet was created");

        commonSteps.clickFirstCheckbox();

        Assertions.assertTrue(commonSteps.validateButtonDeleteFleetsDisplayed(),
                "It was expected to validate Delete fleet");

        fleetSteps.clickButtonDeleteFleet();

        Assertions.assertEquals("Are you sure? Removing fleet can not be undone" , commonSteps.getTextFromPopUp(),
                "It was expected to validate second message on pop up");

        commonSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepFiveSeconds();

        Assertions.assertFalse(fleetSteps.validateFleetCreatedInFleetList(fleetName),
                "It was expected to validate fleet was deleted");

    }
}