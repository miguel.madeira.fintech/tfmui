package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import helpers.IHelpers;
import newwindowdifferentusers.NewWindowVendor;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.poi.PoiSteps;
import steps.poigroup.PoiGroupSteps;
import steps.poiprofile.PoiProfileSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;

public class VendorTest extends NewWindowVendor {

    private static FleetSteps fleetSteps;

    private static StoreGroupSteps storeGroupSteps;

    private static StoreSteps storeSteps;

    private static PoiSteps poiSteps;

    private static PoiGroupSteps poiGroupSteps;

    private static PoiProfileSteps poiProfileSteps;

    private static CommonSteps commonSteps;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        Properties properties = iReadProperties.readFromYaml();

        fleetSteps = new FleetSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        storeSteps = new StoreSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        poiSteps = new PoiSteps(driver, properties);

        poiGroupSteps = new PoiGroupSteps(driver, properties);

        poiProfileSteps = new PoiProfileSteps(driver, properties);
    }

    @BeforeEach
    public void initialTestConditions() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();
    }

    @Test
    @DisplayName("Validate permissions for vendor")
    void ff9fa_3176() throws IOException, InterruptedException {
        Assertions.assertTrue(fleetSteps.validateFleetListPresent(),
                "It was expected the fleet list present");

        Assertions.assertTrue(poiSteps.validatePoiTabPresent(),
                "It was expected the poiTab Present");

        Assertions.assertTrue(poiGroupSteps.validateTabPoiGroupNotPresent(),
                "It was expected the poiGroupTab not present");

        Assertions.assertTrue(poiProfileSteps.validateTabPoiProfileDisplayedAndEnabled(),
                "It was expected the poiProfile displayed and enabled");

        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        Assertions.assertTrue(poiSteps.validatePoiTab(),
                "It was expected to validate poi tab");

        Assertions.assertTrue(poiSteps.validateMerchantPinNotPresent(),
                "It was expected the merchant pin not present");

        Assertions.assertTrue(poiSteps.validateMerchantPinDefaultPolicyNotPresent(),
                "It was expected the merchant pin default policy not present");
    }

    @Test
    @DisplayName("Validate operation actions for vendor")
    void ff9fa_3173() throws InterruptedException, IOException {
        Assertions.assertTrue(fleetSteps.validateFleetListPresent(),
                "It was expected the fleet list present");

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button add fleet displayed and enabled");

        Assertions.assertTrue(commonSteps.validateButtonDeleteFleetsDisplayed(),
                "It was expected the button remove fleet displayed");

        commonSteps.selectElementInList("BEFleetwqei49");

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeGroupSteps.validateButtonAddStoreGroupDisplayedAndEnabled(),
                "It was expected the button add store group displayed and enabled");

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeSteps.validateButtonAddStoreDisplayedAndEnabled(),
                "It was expected the button add store displayed and enabled");

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateButtonAddPoiNotPresent(),
                "It was expected the button add poi not present");

        Assertions.assertTrue(poiSteps.validateButtonAddMidDisplayedAndEnabled(),
                "It was expected the button add mid displayed and enabled");

        iHelpers.sleepTwoSeconds();

        poiSteps.clickFirstPoiInList();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateMerchantPinNotPresent(),
                "It was expected the merchant Pin not present");

        Assertions.assertTrue(poiSteps.validateMerchantPinDefaultPolicyNotPresent(),
                "It was expected the merchant Pin default policy not present");

        Assertions.assertTrue(poiSteps.validateMerchantPinEditableContentNotPresent(),
                "It was expected the Merchant Pin Editable Content not present");

        Assertions.assertTrue(poiGroupSteps.validateButtonPoiGroupNotPresent(),
                "It was expected the button poi group not present");

        Assertions.assertTrue(commonSteps.validateButtonDeleteFleetNotPresent(),
                "It was expected the button remove poi group not present");

        Assertions.assertTrue(poiSteps.validateInputSelectPoiNotPresent(),
                "It was expected any checkbox found");

        iHelpers.sleepTwoSeconds();

        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateInputSelectPoiNotPresent(),
                "It was expected any checkbox found");
    }

    @Test
    @DisplayName("Validate vendor does not have permission to create POI label")
    void ff9fa_3361_3587() throws IOException, InterruptedException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateButtonAddLabelNotPresent(),
                "It was expected the button add label not present");

        Assertions.assertTrue(poiSteps.validateButtonDeletePoiNotPresent(),
                "It was expected the button delete poi not present");

    }
}