package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ModalCommonsComponent;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;

public class CreateStoreTest extends NewWindow {

    private static StoreGroupSteps storeGroupSteps;

    private static StoreSteps storeSteps;

    private static CommonSteps commonSteps;

    private static IFakerManager iFakerManager;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        Properties properties = iReadProperties.readFromYaml();

        iFakerManager = new FakerManager();

        storeSteps = new StoreSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);
    }

    @BeforeEach
    public void initialTestConditions() throws IOException, InterruptedException {
        commonSteps.clickFirstColumnInList();

        iHelpers.sleepOneSecond();

        storeGroupSteps.createStoreGroupIfNecessary(iFakerManager.getValidStoreGroupName());

        iHelpers.sleepOneSecond();

        commonSteps.clickFirstColumnInList();

        iHelpers.sleepFiveSeconds();
    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepTwoSeconds();

        commonSteps.clickTabFleet();

        iHelpers.sleepTwoSeconds();
    }

    @Test
    @DisplayName("Validate access to pop up to create store and validate its elements")
    void ff9fa_2773_2774() throws IOException {
        storeGroupSteps.clickAddStoreButton();

        Assertions.assertTrue(storeSteps.validateButtonAddStoreDisplayedAndEnabled(),
                "It was expected to validate Add store button");

        Assertions.assertTrue(storeSteps.validateCreateStoreModal(),
                "It was expected to validate create store pop up");
    }

    @Test
    @DisplayName("Validate information not saved when clicking on Cancel button after create store only with the name")
    void ff9fa_2775_2778() throws IOException, InterruptedException {
        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(storeName,"");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals(storeName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name saved");

        storeGroupSteps.clickAddStoreButton();

        String secondStoreName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(secondStoreName,iFakerManager.getValidNote());

        commonSteps.clickButtonCancelInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertNotEquals(secondStoreName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name not saved");
    }

    @Test
    @DisplayName("Validate information not saved when clicking on X button after create one store with name and notes")
    void ff9fa_2776_2779() throws IOException, InterruptedException {
        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals(storeName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name saved");

        storeGroupSteps.clickAddStoreButton();

        String secondStoreName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(secondStoreName,iFakerManager.getValidNote());

        commonSteps.clickButtonCloseModalDialog();

        iHelpers.sleepTwoSeconds();

        Assertions.assertNotEquals(secondStoreName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name not saved");
    }

    @Test
    @DisplayName("Validate that it is not possible to create a store without informing store name")
    void ff9fa_2777() throws IOException, InterruptedException {
        storeGroupSteps.clickAddStoreButton();

        storeSteps.insertFieldsCreateStoreModal("",iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(commonSteps.validateRequiredField(),
                "It was expected to validate required field message");
    }

    @Test
    @DisplayName("Validate that it is not possible to create a store with duplicated name")
    void ff9fa_2780() throws IOException, InterruptedException {
        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals(storeName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name saved");

        storeGroupSteps.clickAddStoreButton();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(storeSteps.validateInvalidInputMessageStoreNameAlreadyExists(),
                "It was expected to validate this store already exists message");

    }

    @Test
    @DisplayName("Validate that it is not possible to create a store with special characters on name")
    void ff9fa_2848() throws IOException {
        storeGroupSteps.clickAddStoreButton();

        storeSteps.insertFieldsCreateStoreModal("store*/-",iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        Assertions.assertTrue(storeSteps.validateInvalidInputMessagesFieldMustBeAlphanumeric(),
                "It was expected to validate this store already exists message");
    }

    @Test
    @DisplayName("Validate that it is not possible to create a store with name with more than 14 chars")
    void NoJiraTest() throws IOException {
        storeGroupSteps.clickAddStoreButton();

        storeSteps.insertFieldsCreateStoreModal(iFakerManager.getInvalidStoreName(),iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        Assertions.assertTrue(storeSteps.validateInvalidInputMessageMaxLength(),
                "It was expected to validate max length message");
    }
}