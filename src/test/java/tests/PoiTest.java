package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ModalCommonsComponent;
import driverhelper.DriverHelper;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.poi.PoiSteps;
import steps.store.StoreSteps;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class PoiTest extends NewWindow {
    private static CommonSteps commonSteps;

    private static StoreSteps storeSteps;

    private static PoiSteps poiSteps;

    private static Properties properties;

    private static IHelpers iHelpers;

    private static DriverHelper driverHelper;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        properties = iReadProperties.readFromYaml();

        commonSteps = new CommonSteps(driver, properties);

        storeSteps = new StoreSteps(driver, properties);

        poiSteps = new PoiSteps(driver, properties);

        iHelpers = injector.getInstance(IHelpers.class);

        driverHelper = new DriverHelper(driver, properties);
    }

    @BeforeEach
    public void initialTestConditions() throws IOException, InterruptedException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();
    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepOneSecond();

        commonSteps.clickTabFleet();

    }

    @Test
    @DisplayName("Validate POI Header and Size")
    void ff9fa_2974() throws InterruptedException, IOException {
        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals("Select all POI ID Terminal ID Profile Label State Configuration status Enabled",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextFirstListHeader()),
                "It was expected to validate POI header");

        Assertions.assertTrue(commonSteps.getListContentSize() > 0,
                "It was expected to validate list size");

        Assertions.assertTrue(poiSteps.validateButtonDeployDisplayedAndDisabled(),
                "It was expected the button deploy displayed and disabled");
    }

    @Test
    @DisplayName("Validate Tabs Component and switching functionality")
    void ff9fa_2974_2() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        poiSteps.clickFirstPoiInList();

        Assertions.assertTrue(poiSteps.validatePoiTab(), "It was expected to validate poi tab");

        Assertions.assertTrue(poiSteps.validateDetailsAndPropertiesTab(),
                "It was expected to validate details properties tab");

        poiSteps.clickButtonParameters();

        iHelpers.sleepTwoSeconds();

        Assertions.assertFalse(poiSteps.validateDetailsAndPropertiesTab(),
                "It was expected to validate details properties tab not present");

        Assertions.assertTrue(poiSteps.validateParametersTab(),
                "It was expected to validate Parameters tab");

        poiSteps.clickButtonComponentsAndCapabilities();

        iHelpers.sleepTwoSeconds();

        Assertions.assertFalse(poiSteps.validateDetailsAndPropertiesTab(),
                "It was expected to validate details properties tab not present");

        Assertions.assertTrue(poiSteps.validateComponentsAndCapabilitiesTab(),
                "It was expected to validate Parameters tab");
    }

    @Test
    @DisplayName("Validate Poi tab - End to End test (new poi)")
    void NoJiraTest() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList("Fleet Name564");

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        storeSteps.clickAddPoiButton();

        iHelpers.sleepOneSecond();

        poiSteps.insertFieldsCreatePoiModal("");

        iHelpers.sleepTenSeconds();

        String poiAndTerminalId = poiSteps.getFirstPoiInList();

        String poiId = poiAndTerminalId.substring(0,11);

        String terminalId = poiAndTerminalId.substring(11,20);

        iHelpers.sleepFiveSeconds();

        commonSteps.clickTabPoi();

        iHelpers.sleepTenSeconds();

        Assertions.assertTrue(poiSteps.getTextPoiLine(poiId).contains(poiId),
                "It was expected to validate line contains poi Id");

        Assertions.assertTrue(poiSteps.getTextPoiLine(poiId).contains(terminalId),
                "It was expected to validate line contains terminal Id");

        Assertions.assertTrue(poiSteps.getTextPoiLine(poiId).contains("NEW"),
                "It was expected to validate line contains state NEW");

        Assertions.assertTrue(poiSteps.getTextPoiLine(poiId).contains("Available to Poi"),
                "It was expected to validate line contains Available to Poi");

        commonSteps.selectElementInList(poiId);

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validatePoiTab(),
                "It was expected to validate poi tab");

        Assertions.assertTrue(poiSteps.validateDetailsAndPropertiesTab(),
                "It was expected to validate details properties tab");

        Assertions.assertTrue(poiSteps.validatePoiLabelIsPresent(poiId),
                "It was expected to validate poi present");
    }

    @Test
    @DisplayName("Check and unCheck all checkboxes")
    void ff9fa_3230() throws InterruptedException {
        iHelpers.sleepFiveSeconds();

        poiSteps.inputSelectAllPoiSInList();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateCheckboxesAreSelected(),
                "It was expected all checkboxes selected");

        iHelpers.sleepOneSecond();

        driverHelper.scrollUp();

        driverHelper.scrollUp();

        poiSteps.inputUnSelectAllPoiSInList();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateAnyOneCheckboxIsSelected(),
                "It was expected any one checkbox is selected");

        driverHelper.scrollUp();
    }

    @Test
    @DisplayName("Validate deploy button enabled when the poi is selected and each item has a checkbox")
    void ff9fa_3234() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateCheckboxInAllPoiS(),
                "It was expected the same number of items");

        poiSteps.inputSelectFirstPoiInList();

        Assertions.assertTrue(poiSteps.validateFirstCheckboxIsSelected(),
                "It was expected the first checkbox is selected");

        iHelpers.sleepOneSecond();

        poiSteps.inputSelectFirstPoiInList();

        Assertions.assertFalse(poiSteps.validateFirstCheckboxIsSelected(),
                "It was expected the first checkbox is not selected");

        iHelpers.sleepOneSecond();

        poiSteps.inputSelectFirstPoiInList();

        Assertions.assertTrue(poiSteps.validateFirstCheckboxIsSelected(),
                "It was expected the first checkbox is selected");

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateButtonDeployDisplayedAndEnabled(),
                "It was expected the button deploy displayed and enabled");
    }

    @Test
    @DisplayName("Validate message when click on deploy button and validate by default any checkbox is selected")
    void ff9fa_3235() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateAnyOneCheckboxIsSelected(),
                "It was expected any one checkbox is selected");

        poiSteps.inputSelectFirstPoiInList();

        iHelpers.sleepTwoSeconds();

        poiSteps.clickButtonDeploy();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateDivDeployChangesText("Are you sure you want to deploy the" +
                " configuration changes?"),"It was expected to validate text");

        poiSteps.clickConfirmationChangesButtonClose();
    }

    @Test
    @DisplayName("Click on deploy button and on pop-up select No")
    void ff9fa_3236() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateAnyOneCheckboxIsSelected(),
                "It was expected any one checkbox is selected");

        Assertions.assertTrue(poiSteps.validateButtonDeployDisplayedAndEnabled(),
                "It was expected the button deploy displayed and enabled");

        poiSteps.inputSelectFirstPoiInList();

        iHelpers.sleepTwoSeconds();

        poiSteps.clickButtonDeploy();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickConfirmationChangesSpanNo();

        List<String> listComplete = iHelpers.networkRequestsComplete(driver);

        Assertions.assertEquals(0, poiSteps.validateListContainsCallToBackEnd(listComplete,
                properties.getPoiForFLeetForRetailerAndDeployPoi()).size(),
                "It was expected to validate call to back end to deploy POI");
    }

    @Test
    @DisplayName("Click on deploy button and on pop-up select Yes")
    void ff9fa_3237() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateAnyOneCheckboxIsSelected(),
                "It was expected any one checkbox is selected");

        iHelpers.sleepTenSeconds();

        String selectedPoiId = poiSteps.clickFirstPoiCheckboxByConfigurationStatusWithIntStream("Available to Poi");

        iHelpers.sleepFiveSeconds();

        poiSteps.clickButtonDeploy();

        iHelpers.sleepTwoSeconds();

        poiSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFirstPoiByPoiId(selectedPoiId);

        iHelpers.sleepFiveSeconds();

        Assertions.assertEquals("Available to Poi", poiSteps.getTextDivConfigStatus(),
                "It was expected Available to Poi");

        iHelpers.sleepFiveSeconds();
    }

    @Test
    @DisplayName("Validate POI locally modified deployed successfully")
    void NoJiraTest_1() throws InterruptedException, IOException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFirstPoiByConfigurationStatus("Locally Modified");

        Assertions.assertTrue(poiSteps.validateDivConfigurationStatus("Locally Modified"),
                "It was expected to validate status");

        poiSteps.clickButtonDeploy();

        iHelpers.sleepTwoSeconds();

        poiSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateDivConfigurationStatus("Available to Poi"),
                "It was expected to validate status");
    }

    @Test
    @DisplayName("Validate the presence of the filter panel on poi tab")
    void ff9fa_3238() throws InterruptedException, IOException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateFilterPanelOnPoiTab(),
                "It was expected the filter panel present");
    }

    @Test
    @DisplayName("Validate list fleet filter element")
    void ff9fa_3239() throws InterruptedException, IOException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFilterPanelFleet();

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(poiSteps.validateFilterPanelFilterListBoxItemsHasCheckBoxes(),
                "It was expected all items with checkboxes");
    }

    @Test
    @DisplayName("Validate list store group filter element")
    void ff9fa_3240() throws InterruptedException, IOException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFilterPanelStoreGroup();

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(poiSteps.validateFilterPanelFilterListBoxItemsHasCheckBoxes(),
                "It was expected all items with checkboxes");
    }

    @Test
    @DisplayName("Validate list store filter element")
    void ff9fa_3241() throws InterruptedException, IOException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFilterPanelStore();

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(poiSteps.validateFilterPanelFilterListBoxItemsHasCheckBoxes(),
                "It was expected all items with checkboxes");
    }

    @Test
    @DisplayName("Validate list platform filter element")
    void ff9fa_3242() throws InterruptedException, IOException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFilterPanelPlatform();

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(poiSteps.validateFilterPanelFilterListBoxItemsHasCheckBoxes(),
                "It was expected all items with checkboxes");
    }

    @Test
    @DisplayName("Validate list configuration status filter element")
    void ff9fa_3243() throws InterruptedException, IOException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFilterPanelConfigurationStatus();

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(poiSteps.validateFilterPanelFilterListBoxItemsHasCheckBoxes(),
                "It was expected all items with checkboxes");
    }

    @Test
    @DisplayName("Validate select filter element")
    void ff9fa_3265() throws InterruptedException, IOException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFilterPanelFleet();

        iHelpers.sleepOneSecond();

        poiSteps.selectFilterPanelFilterListBoxSpecificItem("BEFleetvazb22");

        poiSteps.clickFilterPanelFleet();

        iHelpers.sleepOneSecond();

        poiSteps.clickFilterPanelStoreGroup();

        iHelpers.sleepOneSecond();

        poiSteps.selectFilterPanelFilterListBoxSpecificItem("Earnest Trantow");

        iHelpers.sleepOneSecond();

        poiSteps.clickFilterPanelStoreGroup();

        iHelpers.sleepOneSecond();

        poiSteps.clickFilterPanelStore();

        iHelpers.sleepOneSecond();

        poiSteps.selectFilterPanelFilterListBoxSpecificItem("stndteyl");

        poiSteps.clickFilterPanelStore();

        iHelpers.sleepOneSecond();

        poiSteps.clickFirstPoiInList();

        iHelpers.sleepTenSeconds();

        Assertions.assertTrue(poiSteps.validateDetailsAndPropertiesTab(),
                "It was expected to validate details properties tab");
    }

    @Test
    @DisplayName("Validate select filter element doesn't belong")
    void ff9fa_3266() throws InterruptedException, IOException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFilterPanelFleet();

        iHelpers.sleepFiveSeconds();

        poiSteps.selectFilterPanelFilterListBoxSpecificItem("BEFleetvazb22");

        poiSteps.clickFilterPanelFleet();

        iHelpers.sleepOneSecond();

        poiSteps.clickFilterPanelStoreGroup();

        iHelpers.sleepOneSecond();

        poiSteps.selectFilterPanelFilterListBoxSpecificItem("Earnest Trantow");

        iHelpers.sleepOneSecond();

        poiSteps.clickFilterPanelStoreGroup();

        iHelpers.sleepTwoSeconds();

        poiSteps.clickFilterPanelStore();

        iHelpers.sleepFiveSeconds();

        poiSteps.selectFilterPanelFilterListBoxSpecificItem("ST733");

        poiSteps.clickFilterPanelStore();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeSteps.validateDivNoPoiSAreDefined(),
                "It was expected the div no poi's displayed");
    }

    @Test
    @DisplayName("Validate POI with platform PAX D190")
    void ff9fa_3423() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList(properties.getFleetForPlatforms());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickPoiInList("01318539508");

        Assertions.assertTrue(poiSteps.validatePlatformChosen("PAX D190"),
                "It was expected to validate platform");

        Assertions.assertTrue(poiSteps.validateImgPaxD190(),"It was expected to validate img present");
    }

    @Test
    @DisplayName("Validate POI with platform PAX S920")
    void ff9fa_3424() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList(properties.getFleetForPlatforms());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickPoiInList("01317681200");

        Assertions.assertTrue(poiSteps.validatePlatformChosen("PAX S920"),
                "It was expected to validate platform");

        Assertions.assertTrue(poiSteps.validateImgPaxS920(),"It was expected to validate img present");
    }


    @Test
    @DisplayName("Validate Add Label Funcionality")
    void ff9fa_3399_3401() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList(properties.getFleetForPlatforms());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickPoiInList("01318539508");

        String[] initialLabels = poiSteps.getLabels();

        poiSteps.clickDivLabel();

        iHelpers.sleepTwoSeconds();

        String[] totalLabels = poiSteps.getLabels();

        List<String> listOne = Arrays.asList(initialLabels);

        if(listOne.contains(totalLabels[2])){

            poiSteps.clickDivLabel();

            poiSteps.clickOnLabel(totalLabels[2]);

            iHelpers.sleepFiveSeconds();

            String[] labelsAfterClick = poiSteps.getLabels();

            Assertions.assertTrue(labelsAfterClick.length == initialLabels.length - 1,
                    "It was expected to validate labels");
        }

        else {
            poiSteps.clickDivLabel();

            poiSteps.clickOnLabel(totalLabels[2]);

            iHelpers.sleepFiveSeconds();

            String[] labelsAfterClick = poiSteps.getLabels();

            Assertions.assertTrue(labelsAfterClick.length == initialLabels.length + 1,
                    "It was expected to validate labels");
        }
    }

    @Test
    @DisplayName("Validate Add Label Cancel button")
    void ff9fa_3402() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList("BEFleetxktl68");

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickPoiInList("01327607512");

        String[] initialLabels = poiSteps.getLabels();

        poiSteps.clickDivLabel();

        iHelpers.sleepTwoSeconds();

        String[] totalLabels = poiSteps.getLabels();

        poiSteps.clickDivLabel();

        poiSteps.clickOnLabelCancel(totalLabels[2]);

        iHelpers.sleepTwoSeconds();

        String[] labelsAfterClick = poiSteps.getLabels();

        Assertions.assertTrue(labelsAfterClick.length == initialLabels.length,
                    "It was expected to validate labels");
    }

    @Test
    @DisplayName("Validate Label filter")
    void ff9fa_3368_3369() throws IOException, InterruptedException {
        Assertions.assertTrue(poiSteps.getTextFromLabelDropdown("No Selection"),
                "It was expected to validate text from label dropdown");

        poiSteps.clickLabelDropdown();

        List<String> lstWithLabels = Arrays.asList(poiSteps.getTextFromLabelDropdown().get(0).split("\n"));

        int random = (int) (Math.random() * (8 - 0));

        String selectedLabel = lstWithLabels.get(random);

        poiSteps.clickOnSpecificLabel(selectedLabel);

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.getTextFromLabelDropdown(selectedLabel),
                "It was expected to validate text from label dropdown");

        List<String> lstWIthAllLabels = poiSteps.getTextFromDivAllLabels();

        Assertions.assertTrue(lstWIthAllLabels.stream().allMatch(x -> x.contains(selectedLabel)),
                "It was expected to validate all labels have filter option");

    }

    @Test
    @DisplayName("Validate two Labels selected on filter")
    void ff9fa_3371() throws IOException, InterruptedException {
        Assertions.assertTrue(poiSteps.getTextFromLabelDropdown("No Selection"),
                "It was expected to validate text from label dropdown");

        poiSteps.clickLabelDropdown();

        List<String> lstWithLabels = Arrays.asList(poiSteps.getTextFromLabelDropdown().get(0).split("\n"));

        int random = (int) (Math.random() * (8 - 0));

        String selectedLabel = lstWithLabels.get(random);

        poiSteps.clickOnSpecificLabel(selectedLabel);

        iHelpers.sleepFiveSeconds();

        String secondSelectedLabel = lstWithLabels.get(random + 1);

        poiSteps.clickLabelDropdown();

        iHelpers.sleepOneSecond();

        poiSteps.clickOnSpecificLabel(secondSelectedLabel);

        Assertions.assertTrue(poiSteps.getTextFromLabelDropdown(selectedLabel.concat(", ")+secondSelectedLabel),
                "It was expected to validate text from label dropdown");

        List<String> lstWIthAllLabels = poiSteps.getTextFromDivAllLabels();

        Assertions.assertTrue(lstWIthAllLabels.stream().allMatch(x -> x.contains(selectedLabel) ||
                        x.contains(secondSelectedLabel)),"It was expected to validate all labels have filter option");
    }

}