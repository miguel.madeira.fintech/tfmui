package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ModalCommonsComponent;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.poigroup.PoiGroupSteps;

import java.io.IOException;
import java.util.List;

public class PoiGroupTest extends NewWindow {

    private static PoiGroupSteps poiGroupSteps;

    private static CommonSteps commonSteps;

    private static IHelpers iHelpers;

    private static IFakerManager iFakerManager;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        Properties properties = iReadProperties.readFromYaml();

        poiGroupSteps = new PoiGroupSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        iHelpers = injector.getInstance(IHelpers.class);

        iFakerManager = injector.getInstance(IFakerManager.class);

    }

    @BeforeEach
    public void initialTestConditions() throws IOException, InterruptedException {
        commonSteps.clickTabPoiGroup();

        iHelpers.sleepTwoSeconds();
    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepOneSecond();
    }

    @Test
    @DisplayName("Validate message no poi groups are defined")
    void ff9fa_3123_A() throws IOException {
        Assertions.assertTrue(poiGroupSteps.validateNoPoiGroupsAreDefinedMessage());

        //If we have poi groups in the system the test fails automatically,  delete all poi groups and run the test :)
    }

    @Test
    @DisplayName("Poi group list component")
    void ff9fa_3124() throws IOException, InterruptedException {
        poiGroupSteps.clickButtonAddPoi();

        iHelpers.sleepTwoSeconds();

        poiGroupSteps.createPoiGroup(iFakerManager.getPoiGroupName());

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals("Select\n" +
                "POI Group Name\n" +
                "POI Group Type\n" +
                "Nr. of Pois", poiGroupSteps.getPoiGroupListHeaderText());
    }

    @Test
    @DisplayName("Validate create poi group pop-up")
    void ff9fa_3128() throws IOException, InterruptedException {
        poiGroupSteps.clickButtonAddPoi();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiGroupSteps.validateCreatePoiGroupModal());
    }

    @Test
    @DisplayName("Inform poi group name and click on cancel button")
    void ff9fa_3129() throws IOException, InterruptedException {
        poiGroupSteps.clickButtonAddPoi();

        iHelpers.sleepTwoSeconds();

        String poiGroupName = iFakerManager.getPoiGroupName();

        poiGroupSteps.insertPoiGroupName(poiGroupName);

        commonSteps.clickButtonCancelInModal();

        Assertions.assertFalse(poiGroupSteps.validatePoiGroupCreatedInPoiGroupList(poiGroupName),
                "It was expected the poi group not present");
    }

    @Test
    @DisplayName("Inform poi group name and click on X button")
    void ff9fa_3130() throws IOException, InterruptedException {
        poiGroupSteps.clickButtonAddPoi();

        iHelpers.sleepTwoSeconds();

        String poiGroupName = iFakerManager.getPoiGroupName();

        poiGroupSteps.insertPoiGroupName(poiGroupName);

        commonSteps.clickOnButtonClosePoiGroup();

        Assertions.assertFalse(poiGroupSteps.validatePoiGroupCreatedInPoiGroupList(poiGroupName),
                "It was expected the poi group not present");
    }

    @Test
    @DisplayName("Try to create a poi group without informing poi group name")
    void ff9fa_3131() throws IOException, InterruptedException {
        poiGroupSteps.clickButtonAddPoi();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickButtonCreateInModal();

        Assertions.assertTrue(poiGroupSteps.validateInputPoiGroupNameIsRequired(),
                "It was expected the field required");
    }

    @Test
    @DisplayName("Validate error message duplicated poi group name")
    void ff9fa_3132() throws IOException, InterruptedException {
        poiGroupSteps.clickButtonAddPoi();

        iHelpers.sleepTwoSeconds();

        String poiGroupName = iFakerManager.getPoiGroupName();

        poiGroupSteps.createPoiGroup(poiGroupName);

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiGroupSteps.validatePoiGroupCreatedInPoiGroupList(poiGroupName),
                "It was expected the poi group not present");

        poiGroupSteps.clickButtonAddPoi();

        iHelpers.sleepTwoSeconds();

        poiGroupSteps.createPoiGroup(poiGroupName);

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(poiGroupSteps.validatePoiGroupAlreadyExistsErrorMessage(),
                "It was expected the poi group already exists message");
    }

    @Test
    @DisplayName("Validate error message duplicated poi group name")
    void ff9fa_3133() throws IOException, InterruptedException {
        poiGroupSteps.clickButtonAddPoi();

        iHelpers.sleepTwoSeconds();

        String poiGroupName = iFakerManager.getPoiGroupName();

        poiGroupSteps.createPoiGroup(poiGroupName);

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiGroupSteps.validatePoiGroupCreatedInPoiGroupList(poiGroupName),
                "It was expected the poi group not present");

        Assertions.assertTrue(poiGroupSteps.validatePoiGroupCreatedInPoiGroupListIsInclusionBased(),
                "It was expected the poi group Inclusion-based present");
    }

    @Test
    @DisplayName("Validate selecting 1 POI Groups and canceling removing them")
    void ff9fa_3431_3432() throws IOException {
        Assertions.assertTrue(poiGroupSteps.validateButtonRemoveNotEnabled(),
                "It was expected to validate button not enabled");

        List<String> initialList = poiGroupSteps.getPoiGroupNames();

        commonSteps.clickFirstCheckbox();

        Assertions.assertTrue(poiGroupSteps.validateButtonRemoveDisplayedAndEnabled(),
                "It was expected to validate button enabled");

        poiGroupSteps.clickButtonRemovePoiGroup();

        Assertions.assertTrue(poiGroupSteps.validateDivPopUpDeletePoiGroup("Are you sure you want remove POI Group"),
                "It was expected to validate message");

        commonSteps.clickConfirmationChangesSpanNo();

        List<String> secondList = poiGroupSteps.getPoiGroupNames();

        Assertions.assertEquals(initialList.size(), secondList.size(),
                "It was expected to validate POI Group names list size");
    }

    @Test
    @DisplayName("Validate selecting 2 POI Groups and removing them")
    void ff9fa_3435() throws IOException, InterruptedException {
        Assertions.assertTrue(poiGroupSteps.validateButtonRemoveNotEnabled(),
                "It was expected to validate button not enabled");

        List<String> initialList = poiGroupSteps.getPoiGroupNames();

        commonSteps.clickFirstCheckbox();

        poiGroupSteps.clickSecondCheckbox();

        Assertions.assertTrue(poiGroupSteps.validateButtonRemoveDisplayedAndEnabled(),
                "It was expected to validate button enabled");

        poiGroupSteps.clickButtonRemovePoiGroup();

        Assertions.assertTrue(poiGroupSteps.validateDivPopUpDeletePoiGroup(
                "Are you sure you want to remove 2 POI Groups?"), "It was expected to validate message");

        commonSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepFiveSeconds();

        List<String> secondList = poiGroupSteps.getPoiGroupNames();

        Assertions.assertEquals(secondList.size(), initialList.size() - 2,
                "It was expected to validate POI Group names list size");

    }
}

