package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ModalCommonsComponent;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import pages.storegroupdetails.StoreGroupDetailsWebElements;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;
import java.text.ParseException;

public class StoreGroupDetailsTest extends NewWindow {

    //Tests from ff9fa_2789 to ff9fa_2790 related to US-2681

    private static FleetSteps fleetSteps;

    private static StoreGroupSteps storeGroupSteps;

    private static StoreSteps storeSteps;

    private static CommonSteps commonSteps;

    private static StoreGroupDetailsWebElements storeGroupDetailsWebElements;

    private static IFakerManager iFakerManager;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException, InterruptedException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        Properties properties = iReadProperties.readFromYaml();

        iFakerManager = new FakerManager();

        fleetSteps = new FleetSteps(driver, properties);

        storeSteps = new StoreSteps(driver, properties);

        storeGroupDetailsWebElements = new StoreGroupDetailsWebElements(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        iHelpers.sleepFiveSeconds();
    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepTwoSeconds();

        commonSteps.clickTabFleet();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Create new fleet, add a store group and validate store group details with dynamic data")
    void ff9fa_2762_2767() throws IOException, InterruptedException, ParseException {
        fleetSteps.clickButtonAddFleet();

        iHelpers.sleepOneSecond();

        String fleetName = iFakerManager.getValidFleetName();

        String solutionContractName = fleetSteps.insertFieldsCreateFleetModal(fleetName,"");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        commonSteps.selectElementInList(fleetName);

        iHelpers.sleepTwoSeconds();

        fleetSteps.clickButtonAddStoreGroup();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        String notes = iFakerManager.getValidNote();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(storeGroupName, notes);

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals(storeGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");

        commonSteps.selectElementInList(storeGroupName);

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(storeGroupSteps.validateStoreGroupDetails(solutionContractName, fleetName, storeGroupName,
          "None", notes, "0","0"),"It was expected to validate all fields");

        Assertions.assertTrue(commonSteps.validateDateIsBeforeNow(storeGroupDetailsWebElements.divCreatedDate()),
                "It was expected to validate created date is before now");

        Assertions.assertTrue(commonSteps.validateDateIsBeforeNow(storeGroupDetailsWebElements.divModifiedDate()),
                "It was expected to validate modified date is before now");

        Assertions.assertTrue(storeGroupSteps.validateThereAreNoStoresAvailable(),
                "It was expected to validate there are no stores available");
    }

    @Test
    @DisplayName("Validate search element")
    void ff9fa_2789() throws IOException, InterruptedException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        storeGroupSteps.createStoreGroupIfNecessary(iFakerManager.getValidStoreGroupName());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(commonSteps.validateSearchComponentOnStoreGroupDetails("Search by Store, Profile"),
                "It was expected the search component present");
    }

    @Test
    @DisplayName("Validate action buttons next the search component")
    void ff9fa_2790() throws IOException, InterruptedException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        storeGroupSteps.createStoreGroupIfNecessary(iFakerManager.getValidStoreGroupName());

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(commonSteps.validateButtonDisplayedAndEnabledWithExpectedText(
                storeGroupDetailsWebElements.buttonAddStore(), "ADD STORE"),
                "It was expected the button Add store displayed and enabled");

        Assertions.assertTrue(commonSteps.validateButtonDeleteStoreIsDisplayed(),
                "It was expected the button delete stores displayed");
    }

    @Test
    @DisplayName("Validate data table component")
    void ff9fa_2768() throws IOException, InterruptedException {
        commonSteps.clickFirstColumnInList();

        iHelpers.sleepOneSecond();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.createStoreGroupIfNecessary(storeGroupName);

        iHelpers.sleepOneSecond();

        commonSteps.clickFirstColumnInList();

        iHelpers.sleepFiveSeconds();

        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(storeName + " None 0", commonSteps.getTextFirstElementInList(),
                "It was expected store name, poi profile name and nr of pois to be correct");

        Assertions.assertEquals("Select all Store Profile Nr of POIs", commonSteps.replaceNewLineForSpace(
                commonSteps.getTextFirstListHeader()),"It was expected to validate header");

    }

    @Test
    @DisplayName("Validate sorting functionality on store")
    void ff9fa_2769() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        String fleetName = iFakerManager.getValidFleetName();

        fleetSteps.clickButtonAddFleet();

        fleetSteps.insertFieldsCreateFleetModal(fleetName,"");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList(fleetName);

        iHelpers.sleepOneSecond();

        fleetSteps.clickButtonAddStoreGroup();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(storeGroupName, iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList(storeGroupName);

        iHelpers.sleepTwoSeconds();

        storeGroupSteps.clickAddStoreButton();

        storeSteps.insertFieldsCreateStoreModal("Amandius",iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals("Amandius", commonSteps.getTextFirstStoreList(),
                "It was expected Amandius on store");

        iHelpers.sleepOneSecond();

        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(storeName, commonSteps.getTextFirstStoreList(),
                "It was expected store name at first line");

        Assertions.assertTrue(commonSteps.getListContentSize() > 1,
                "It was expected the list size grater than 1");

        commonSteps.clickDivStore();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals("Amandius", commonSteps.getTextFirstStoreList(),
                "It was expected Amandius at first line");
    }

    @Test
    @DisplayName("Not possible to create store with same name than other in same fleet but different store group")
    void NoJiraTest() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        String fleetName = iFakerManager.getValidFleetName();

        fleetSteps.clickButtonAddFleet();

        fleetSteps.insertFieldsCreateFleetModal(fleetName,"");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList(fleetName);

        iHelpers.sleepOneSecond();

        fleetSteps.clickButtonAddStoreGroup();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(storeGroupName, iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList(storeGroupName);

        iHelpers.sleepTwoSeconds();

        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(storeName, commonSteps.getTextFirstStoreList(),
                "It was expected store name");

        iHelpers.sleepOneSecond();

        commonSteps.clickTabFleet();

        iHelpers.sleepTwoSeconds();

        commonSteps.selectElementInList(fleetName);

        iHelpers.sleepOneSecond();

        fleetSteps.clickButtonAddStoreGroup();

        String secondStoreGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(secondStoreGroupName, iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList(secondStoreGroupName);

        iHelpers.sleepTwoSeconds();

        storeGroupSteps.clickAddStoreButton();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        Assertions.assertTrue(storeSteps.validateInvalidInputMessageStoreNameAlreadyExists(),
                "It was expected to validate this store already exists message");
    }

    @Test
    @DisplayName("Validate action buttons select all")
    void ff9fa_3627() throws IOException, InterruptedException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        storeGroupSteps.createStoreGroupIfNecessary(iFakerManager.getValidStoreGroupName());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        storeGroupSteps.clickAddStoreButton();

        String secondStoreName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(secondStoreName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(commonSteps.getListContentSize() >= 2,
                "It was expected the list size grater or equal than 2");

        int numberOfStores = commonSteps.getListContentSize();

        commonSteps.clickOnDivSelectAll();

        Assertions.assertEquals(numberOfStores,commonSteps.numberOfCheckboxesAreSelected(),
                "It was expected to validate select all worked");
    }

    @Test
    @DisplayName("Validate action buttons select only first checkbox")
    void ff9fa_3626() throws IOException, InterruptedException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        storeGroupSteps.createStoreGroupIfNecessary(iFakerManager.getValidStoreGroupName());

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        int initialNumberOfStores = Integer.parseInt(storeGroupSteps.getTextFromNrOfStores()[1]);

        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        storeGroupSteps.clickAddStoreButton();

        String secondStoreName = iFakerManager.getValidStoreName();

        iHelpers.sleepTwoSeconds();

        storeSteps.insertFieldsCreateStoreModal(secondStoreName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        int finalNumberOfStores = Integer.parseInt(storeGroupSteps.getTextFromNrOfStores()[1]);

        Assertions.assertTrue(commonSteps.getListContentSize() >= 2,
                "It was expected the list size grater or equal than 2");

        int numberOfStores = commonSteps.getListContentSize();

        commonSteps.clickFirstCheckbox();

        Assertions.assertTrue(numberOfStores > commonSteps.numberOfCheckboxesAreSelected(),
                "It was expected to validate only one checkbox selected");

        Assertions.assertEquals(1, commonSteps.numberOfCheckboxesAreSelected(),
                "It was expected to validate only one checkbox selected");

        Assertions.assertEquals(initialNumberOfStores + 2,finalNumberOfStores,
                "It was expected to validate Nr of stores updated");

        //TODO BUG FF9FA_3658
    }

    @Test
    @DisplayName("Delete Store without pois successfully")
    void ff9fa_3641_3691() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        String fleetName = iFakerManager.getValidFleetName();

        iHelpers.sleepTwoSeconds();

        fleetSteps.clickButtonAddFleet();

        fleetSteps.insertFieldsCreateFleetModal(fleetName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        commonSteps.selectElementInList(fleetName);

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(commonSteps.validateButtonDeleteStoreGroupIsDisplayed(),
                "It was expected to validate button delete store group displayed");

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.createStoreGroupIfNecessary(storeGroupName);

        iHelpers.sleepTwoSeconds();

        commonSteps.selectElementInList(storeGroupName);

        iHelpers.sleepTwoSeconds();

        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(storeName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickFirstCheckbox();

        iHelpers.sleepTwoSeconds();

        storeGroupSteps.clickAddStoreButton();

        String secondStoreName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(secondStoreName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(commonSteps.validateButtonDeleteStoreIsDisplayed(),
                "It was expected to validate button delete store  displayed and enabled");

        Assertions.assertTrue(storeGroupSteps.validateNrOfStores(2),
                "It was expected to validate 2 Stores");

        storeGroupSteps.clickButtonDeleteStore();

        Assertions.assertEquals("Are you sure? Removing store can not be undone", commonSteps.getTextFromPopUp(),
                "It was expected to validate button delete stores displayed and enabled");

        commonSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeGroupSteps.validateNrOfStores(1),
                "It was expected to validate 1 SG");

        commonSteps.clickFirstCheckbox();

        storeGroupSteps.clickAddStoreButton();

        String thirdStoreName = iFakerManager.getValidStoreName();

        storeSteps.insertFieldsCreateStoreModal(thirdStoreName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickFirstCheckbox();

        storeGroupSteps.clickButtonDeleteStore();

        Assertions.assertEquals("Are you sure? Removing stores can not be undone", commonSteps.getTextFromPopUp(),
                "It was expected to validate button delete store group displayed and enabled");

        commonSteps.clickConfirmationChangesSpanNo();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(storeGroupSteps.validateNrOfStores(2),
                "It was expected to validate 2 SGs");
    }

    @Test
    @DisplayName("Delete Store with poi successfully")
    void ff9fa_3691() throws IOException, InterruptedException {
        commonSteps.clickTabFleet();

        commonSteps.selectElementInList("BEFleetrewv19");

        iHelpers.sleepTwoSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickFirstCheckbox();

        int initialNumberOfStores = storeGroupSteps.getNumberOfStores();

        storeGroupSteps.clickButtonDeleteStore();

        Assertions.assertEquals("Are you sure? Removing store can not be undone", commonSteps.getTextFromPopUp(),
                "It was expected to validate button delete stores displayed and enabled");

        commonSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals("At least one selected store has POIs, Deleting the store will also remove the " +
                        "associated POIs. Do you want to continue?", commonSteps.getTextFromPopUp(),
                "It was expected to validate second message on pop up");

        commonSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeGroupSteps.validateNrOfStores(initialNumberOfStores - 1),
                "It was expected to validate number of stores");

        commonSteps.clickFirstCheckbox();

        storeGroupSteps.clickButtonDeleteStore();

        commonSteps.clickConfirmationChangesSpanYes();

        commonSteps.clickConfirmationChangesSpanNo();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(storeGroupSteps.validateNrOfStores(initialNumberOfStores - 1),
                "It was expected to validate stores");
    }
}