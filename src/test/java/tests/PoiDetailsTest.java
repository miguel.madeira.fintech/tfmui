package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import driverhelper.DriverHelper;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.poi.PoiSteps;

import java.io.IOException;
import java.util.List;

public class PoiDetailsTest extends NewWindow {

    private static CommonSteps commonSteps;

    private static PoiSteps poiSteps;

    private static Properties properties;

    private static IHelpers iHelpers;

    private static IFakerManager iFakerManager;

    private static DriverHelper driverHelper;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        properties = iReadProperties.readFromYaml();

        iFakerManager = injector.getInstance(IFakerManager.class);

        commonSteps = new CommonSteps(driver, properties);

        poiSteps = new PoiSteps(driver, properties);

        driverHelper = new DriverHelper(driver,properties);

        iHelpers = injector.getInstance(IHelpers.class);

    }

    @BeforeEach
    public void initialTestConditions() throws IOException, InterruptedException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

    }

    @Test
    @DisplayName("Validate that 'Merchant PIN' and 'Merchant PIN default policy in poi details'")
    void ff9fa_3082() throws InterruptedException, IOException {
        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateMerchantPin(), "It was expected not empty");

        Assertions.assertTrue(poiSteps.validateMerchantPinDefaultPolicy(),
                "It was expected POI ID or Fixed");
    }

    @Test
    @DisplayName("Validate that 'Merchant PIN' recovered fleet level")
    void ff9fa_3083() throws InterruptedException, IOException {
        commonSteps.selectElementInList(properties.getPoiActivePropertyFleetLevel());

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateMerchantPinCountDigits(), "It was expected 4");

        Assertions.assertTrue(poiSteps.validateMerchantPinDefaultPolicyWithExpectedValue("POI ID"),
                "It was expected POI ID");
    }

    @Test
    @DisplayName("Validate that 'Merchant PIN' recovered poi level")
    void ff9fa_3085() throws InterruptedException, IOException {
        commonSteps.selectElementInList(properties.getPoiActivePropertyPoiLevel());

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateMerchantPinCountDigits(), "It was expected @poi");

        Assertions.assertTrue(poiSteps.validateMerchantPinDefaultPolicyWithExpectedValue("POI ID"),
                "It was expected POI Id");
    }

    @Test
    @DisplayName("Validate that 'Merchant PIN' recovered poi level")
    void ff9fa_3138() throws InterruptedException, IOException {
        commonSteps.selectElementInList(properties.getPoiActivePropertyFleetLevel());

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateDivPoiIdInformationDisplayed(),
                "It was expected the div poi information displayed");

        Assertions.assertTrue(poiSteps.getTextDivPoiState().equals("NEW") ||
                poiSteps.getTextDivPoiState().equals("IN USE"));

        Assertions.assertTrue(poiSteps.getTextDivConfigStatus().equals("Available to Poi") ||
                poiSteps.getTextDivConfigStatus().equals("Locally Modified"));

        Assertions.assertEquals("ENABLE\nDISABLE", poiSteps.getTextDivPoiEnable());

        Assertions.assertEquals("DELETE\nDEPLOY", poiSteps.getTextDivPoiDelete());
    }

    @Test
    @DisplayName("Merchant Pin marked as editable")
    void ff9fa_3091() throws InterruptedException, IOException {
        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        poiSteps.clickMerchantPinEditableContent();

        Assertions.assertTrue(poiSteps.validateDivMerchantPinEditableContentSpanSave(),
                "It was expected the span Save displayed");

        Assertions.assertTrue(poiSteps.validateDivMerchantPinEditableContentSpanCancel(),
                "It was expected the span Cancel displayed");
    }

    @Test
    @DisplayName("Validate fields available")
    void ff9fa_3134() throws InterruptedException, IOException {
        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateDivPoiIdInformationDisplayed(),
                "It was expected the div poi information displayed");

        String r1 = poiSteps.getTextPoiDetailsColumns("R1");

        String r2 = poiSteps.getTextPoiDetailsColumns("R2");

        String r3 = poiSteps.getTextPoiDetailsColumns("R3");

        String r4 = poiSteps.getTextPoiDetailsColumns("R4");

        Assertions.assertTrue(r1.contains("Store") && r1.contains("Terminal ID") && r1.contains("Created"));

        Assertions.assertTrue(r2.contains("Store Group") && r2.contains("Applications") &&
                r2.contains("Modified"));

        Assertions.assertTrue(r3.contains("Fleet") && r3.contains("Platform"));

        Assertions.assertTrue(r4.contains("Label") && r4.contains("Profile") && r4.contains("Last Contact") &&
                r4.contains("Note"));
    }

    @Test
    @DisplayName("Edit merchant Pin and click on cancel")
    void ff9fa_3093() throws InterruptedException, IOException {
        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        poiSteps.clickMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        poiSteps.clickEditableContentSpanCancel();

        iHelpers.sleepOneSecond();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertEquals(merchantPin, merchantPinAfterEdit, "It was expected the same value");
    }

    @Test
    @DisplayName("Send escape key to merchant pin")
    void ff9fa_3094() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        iHelpers.sleepTwoSeconds();

        poiSteps.sendEscapeKeyToDivMerchantPinEditableContentText();

        iHelpers.sleepTwoSeconds();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertEquals(merchantPin, merchantPinAfterEdit, "It was expected the same value");
    }

    @Test
    @DisplayName("click outside merchant pin")
    void ff9fa_3095() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        poiSteps.clickMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        driverHelper.scrollBy();

        iHelpers.sleepOneSecond();

        poiSteps.clickDivMerchantPinDefaultPolicy();

        iHelpers.sleepTwoSeconds();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertEquals(merchantPin, merchantPinAfterEdit, "It was expected the same value");
    }

    @Test
    @DisplayName("Clear merchant and click on save button")
    void ff9fa_3096() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        poiSteps.clickMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        poiSteps.clearDivMerchantPinEditableContentText();

        poiSteps.clickDivMerchantPinDefaultPolicy();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertEquals(merchantPin, merchantPinAfterEdit, "It was expected the same value");
    }

    @Test
    @DisplayName("Merchant pin with alphanumeric value and click on save button")
    void ff9fa_3097() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        poiSteps.clickMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        poiSteps.clearDivMerchantPinEditableContentText();

        iHelpers.sleepTwoSeconds();

        poiSteps.insertTextToDivMerchantPinEditableContentText("123t");

        Assertions.assertTrue(poiSteps.validateTextInvalidInputOnlyDigitsAreAllowed(),
                "It was expected validate input text");
        iHelpers.sleepOneSecond();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertEquals(merchantPin, merchantPinAfterEdit, "It was expected the same value");
    }

    @Test
    @DisplayName("Merchant pin with valid value and click on save button on confirmation choose no")
    void ff9fa_3098() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        poiSteps.clickMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        driverHelper.scrollBy();

        iHelpers.sleepOneSecond();

        poiSteps.clearDivMerchantPinEditableContentText();

        iHelpers.sleepTwoSeconds();

        poiSteps.insertTextToDivMerchantPinEditableContentText("9785");

        iHelpers.sleepOneSecond();

        poiSteps.clickEditableContentSpanSave();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateModalConfirmationChangesDisplayed(),
                "It was expected the modal confirmation changes displayed");

        poiSteps.clickConfirmationChangesSpanNo();

        iHelpers.sleepOneSecond();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertEquals(merchantPin, merchantPinAfterEdit, "It was expected the same value");
    }

    @Test
    @DisplayName("Merchant pin with empty value and click on Enter")
    void ff9fa_3099_1() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        poiSteps.clickMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        poiSteps.clearDivMerchantPinEditableContentText();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateTextInvalidInputRequiredField(),
                "It was expected validate input text");

        poiSteps.clickPropertiesLabel();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertEquals(merchantPin, merchantPinAfterEdit, "It was expected the same value");
    }

    @Deprecated
    @DisplayName("Merchant pin with empty value and click on Enter confirmation Yes")
    void ff9fa_3099_2() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        driverHelper.scrollBy();

        iHelpers.sleepOneSecond();

        poiSteps.clickMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        poiSteps.clearDivMerchantPinEditableContentText();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateTextInvalidInputRequiredField(),
                "It was expected the modal confirmation changes displayed");

        poiSteps.clickPropertiesLabel();

        iHelpers.sleepOneSecond();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertEquals(merchantPin, merchantPinAfterEdit, "It was expected the same value");
    }

    @Test
    @DisplayName("Merchant pin with alphanumeric value and click on Enter")
    void ff9fa_3121() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        poiSteps.clickMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        driverHelper.scrollBy();

        iHelpers.sleepOneSecond();

        poiSteps.clearDivMerchantPinEditableContentText();

        iHelpers.sleepTwoSeconds();

        poiSteps.insertTextToDivMerchantPinEditableContentText("123t");

        iHelpers.sleepOneSecond();

        poiSteps.sendEnterKeyToDivMerchantPinEditableContentText();

        Assertions.assertTrue(poiSteps.validateTextInvalidInputOnlyDigitsAreAllowed(),
                "It was expected validate input text");
        iHelpers.sleepOneSecond();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertEquals(merchantPin, merchantPinAfterEdit, "It was expected the same value");
    }

    @Test
    @DisplayName("Merchant pin with valid value and click on save confirmation Yes")
    void ff9fa_3122() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPin = poiSteps.getTextMerchantPinEditableContent();

        poiSteps.clickMerchantPinEditableContent();

        iHelpers.sleepOneSecond();

        driverHelper.scrollBy();

        iHelpers.sleepOneSecond();

        poiSteps.clearDivMerchantPinEditableContentText();

        iHelpers.sleepTwoSeconds();

        String newMerchantPinValue = iHelpers.generateRandomMerchantPin();

        poiSteps.insertTextToDivMerchantPinEditableContentText(newMerchantPinValue);

        iHelpers.sleepOneSecond();

        poiSteps.clickEditableContentSpanSave();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateModalConfirmationChangesDisplayed(),
                "It was expected the modal confirmation changes displayed");

        poiSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepOneSecond();

        driverHelper.scrollBy();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        String merchantPinAfterEdit = poiSteps.getTextMerchantPinEditableContent();

        Assertions.assertNotEquals(merchantPin, merchantPinAfterEdit,
                "It was expected the value different");

        Assertions.assertEquals(newMerchantPinValue, merchantPinAfterEdit,
                "It was expected the value different");
    }

    @Test
    @DisplayName("Validate delete poi is disabled when there is no POI selected")
    void ff9fa_3567_3572() throws IOException {
        Assertions.assertTrue(poiSteps.validateDeletePoiButtonIsDisabled(),
                "It was expected button disabled");

    }

    @Test
    @DisplayName("Validate delete poi is enabled when there is at least one POI selected and all")
    void ff9fa_3573_3574() throws IOException {
        poiSteps.inputSelectFirstPoiInList();

        Assertions.assertTrue(poiSteps.validateDeletePoiButtonIsEnabled(),
                "It was expected button enabled");

        poiSteps.clickOnDeletePoi();

        poiSteps.validateDivPopUpDeletePoi("Are you sure? Removing a POI cannot be undone");

        commonSteps.clickConfirmationChangesSpanNo();

        poiSteps.inputSelectFirstPoiInList();

        Assertions.assertTrue(poiSteps.validateDeletePoiButtonIsDisabled(),
                "It was expected button disabled");

        poiSteps.inputSelectAllPoiSInList();

        Assertions.assertTrue(poiSteps.validateDeletePoiButtonIsEnabled(),
                "It was expected button enabled");

        poiSteps.clickOnDeletePoi();

        poiSteps.validateDivPopUpDeletePoi("Some hidden POIs are selected. Are you sure? Removing POIs cannot be undone");

        commonSteps.clickConfirmationChangesSpanNo();

    }

    @Test
    @DisplayName("Validate delete poi is disabled when there is no POI selected")
    void ff9fa_3579() throws IOException, InterruptedException {
        List<String> initialList = poiSteps.getPoiIdList();

        String firstPoi = initialList.get(0);

        poiSteps.inputSelectFirstPoiInList();

        poiSteps.clickOnDeletePoi();

        commonSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepFiveSeconds();

        List<String> listAfterDeleteFirstPoi = poiSteps.getPoiIdList();

        Assertions.assertFalse(listAfterDeleteFirstPoi.contains(firstPoi),"It was expected to validate list " +
                "does not contain first POI");

    }

    @Test
    @DisplayName("Open POI Details page and validate note field is editable. Confirm adding a note")
    void ff9fa_3606_3643() throws IOException, InterruptedException {
        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpansAreNotDisplayed(),
                "It was expected to validate span Save and Cancel not displayed");

        poiSteps.clickDivNotes();

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpanCancel(),
                "It was expected to validate span Save and Cancel not displayed");

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpanSave(),
                "It was expected to validate span Save and Cancel not displayed");

        if(poiSteps.getTextDivNotesEditableContent().length() == 0){

            String notes = iFakerManager.getValidNote();

            poiSteps.sendNotesToDivNotesEditableText(notes);

            poiSteps.clickEditableContentSpanSave();

            iHelpers.sleepTwoSeconds();

            Assertions.assertTrue(poiSteps.validateModalConfirmationChangesDisplayed(),
                    "It was expected the modal confirmation changes displayed");

            poiSteps.clickConfirmationChangesSpanYes();

            iHelpers.sleepTwoSeconds();

            Assertions.assertEquals(poiSteps.getTextDivNotesEditableContent(), notes,
                    "It was expected to validate notes");
        }

        else {
            poiSteps.clearDivNotesEditableContentText();

            poiSteps.clickEditableContentSpanSave();

            Assertions.assertTrue(poiSteps.validateModalConfirmationChangesDisplayed(),
                    "It was expected the modal confirmation changes displayed");

            poiSteps.clickConfirmationChangesSpanYes();

            iHelpers.sleepTwoSeconds();

            Assertions.assertEquals(0, poiSteps.getTextDivNotesEditableContent().length(),
                    "It was expected to validate notes");
        }
    }

    @Test
    @DisplayName("Open POI Details page and validate note field is editable. Cancel adding a note")
    void ff9fa_3607() throws IOException, InterruptedException {
        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpansAreNotDisplayed(),
                "It was expected to validate span Save and Cancel not displayed");

        poiSteps.clickDivNotes();

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpanCancel(),
                "It was expected to validate span Save and Cancel not displayed");

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpanSave(),
                "It was expected to validate span Save and Cancel not displayed");

        if (poiSteps.getTextDivNotesEditableContent().length() == 0) {

            String notes = iFakerManager.getValidNote();

            poiSteps.sendNotesToDivNotesEditableText(notes);

            poiSteps.clickEditableContentSpanSave();

            iHelpers.sleepTwoSeconds();

            Assertions.assertTrue(poiSteps.validateModalConfirmationChangesDisplayed(),
                    "It was expected the modal confirmation changes displayed");

            poiSteps.clickConfirmationChangesSpanNo();

            iHelpers.sleepTwoSeconds();

            Assertions.assertEquals(poiSteps.getTextDivNotesEditableContent().length(), 0,
                    "It was expected to validate notes");
        }
        else {
            String note = poiSteps.getTextDivNotesEditableContent();

            poiSteps.clearDivNotesEditableContentText();

            poiSteps.clickEditableContentSpanSave();

            Assertions.assertTrue(poiSteps.validateModalConfirmationChangesDisplayed(),
                    "It was expected the modal confirmation changes displayed");

            poiSteps.clickConfirmationChangesSpanNo();

            iHelpers.sleepTwoSeconds();

            Assertions.assertEquals(note, poiSteps.getTextDivNotesEditableContent(),
                    "It was expected to validate notes");
        }
    }

    @Test
    @DisplayName("Open POI Details page and validate note field is editable. Cancel adding a note")
    void ff9fa_3607_1() throws IOException, InterruptedException {
        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpansAreNotDisplayed(),
                "It was expected to validate span Save and Cancel not displayed");

        poiSteps.clickDivNotes();

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpanCancel(),
                "It was expected to validate span Save and Cancel not displayed");

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpanSave(),
                "It was expected to validate span Save and Cancel not displayed");

        String notes = poiSteps.getTextDivNotesEditableContent();

        poiSteps.sendNotesToDivNotesEditableText(iFakerManager.getValidNote());

        poiSteps.clickEditableContentSpanCancel();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals(poiSteps.getTextDivNotesEditableContent(), notes,
                "It was expected to validate notes");

    }

    @Test
    @DisplayName("Send invalid size notes")
    void NoJiraTest() throws IOException, InterruptedException {
        commonSteps.selectElementInList();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpansAreNotDisplayed(),
                "It was expected to validate span Save and Cancel not displayed");

        poiSteps.clickDivNotes();

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpanCancel(),
                "It was expected to validate span Save and Cancel not displayed");

        Assertions.assertTrue(poiSteps.validateDivNotesEditableContentSpanSave(),
                "It was expected to validate span Save and Cancel not displayed");

        String notes = poiSteps.getTextDivNotesEditableContent();

        iHelpers.sleepOneSecond();

        poiSteps.sendNotesToDivNotesEditableText(iFakerManager.getInvalidNote());

        Assertions.assertTrue(poiSteps.validateTextInvalidInputNotesSize(),
                "It was expected validate input text");

        poiSteps.sendEscapeKeyToDivNotesEditableContentText();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals(poiSteps.getTextDivNotesEditableContent(), notes,
                "It was expected to validate notes");

    }
}