package tests;

import appinjector.AppInjector;
import client.ClientCreateFleet;
import client.ClientCreateStore;
import client.ClientCreateStoreGroup;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ModalCommonsComponent;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.mid.MidSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;

public class CreateMidTest extends NewWindow {

    //Tests from ff9fa_2817 to ff9fa_2828 related to US-2963

    private static ClientCreateFleet clientCreateFleet;

    private static ClientCreateStoreGroup clientCreateStoreGroup;

    private static ClientCreateStore clientCreateStore;

    private static FleetSteps fleetSteps;

    private static StoreGroupSteps storeGroupSteps;

    private static StoreSteps storeSteps;

    private static MidSteps midSteps;

    private static CommonSteps commonSteps;

    private static IFakerManager iFakerManager;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        Properties properties = iReadProperties.readFromYaml();

        iFakerManager = new FakerManager();

        fleetSteps = new FleetSteps(driver, properties);

        storeSteps = new StoreSteps(driver, properties);

        midSteps = new MidSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        clientCreateFleet = new ClientCreateFleet(driver, properties);

        clientCreateStoreGroup = new ClientCreateStoreGroup(driver, properties);

        clientCreateStore = new ClientCreateStore(driver, properties);
    }

    @BeforeEach
    public void initTestConditions() throws InterruptedException {
        iHelpers.sleepFiveSeconds();

    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepTwoSeconds();

        commonSteps.clickTabFleet();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate mid modal component")
    void ff9fa_2817() throws IOException, InterruptedException {
        commonSteps.selectElementInList("BEFleetqrgx03");

        commonSteps.clickFirstColumnInList();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickOnStore();

        iHelpers.sleepTwoSeconds();

        storeSteps.clickAddMidButton();

        Assertions.assertTrue(midSteps.validateCreateMidModal(), "It was expected the modal create mid valid");
    }

    @Test
    @DisplayName("Create MID if there is no Acceptance Service Contract")
    void FF9FA_3553_3559() throws IOException, InterruptedException {
        commonSteps.clickFirstColumnInList();

        iHelpers.sleepOneSecond();

        storeGroupSteps.createStoreGroupIfNecessary(iFakerManager.getValidStoreGroupName());

        iHelpers.sleepOneSecond();

        commonSteps.clickFirstColumnInList();

        iHelpers.sleepTwoSeconds();

        storeSteps.createStoreIfNecessary(iFakerManager.getValidStoreName(), "");

        iHelpers.sleepTwoSeconds();

        commonSteps.clickOnStore();

        iHelpers.sleepTwoSeconds();

        if(storeSteps.acceptanceServiceContractsStore()[3].equals("No Acceptance services Contracts are defined") ||
                !midSteps.validateAddMidButtonIsDisabled()) {
            Assertions.assertTrue(midSteps.validateAddMidButtonIsEnabled(),
                    "It was expected to validate create mid button is enabled");

            storeSteps.clickAddMidButton();

            String merchantId = iFakerManager.getValidMerchantIdWithAlphanumeric();

            String merchantCategoryCode = iFakerManager.getValidMerchantCategoryCode();

            String merchantNameAndLocation = iFakerManager.getValidMerchantNameAndLocation();

            String acquiredId = iFakerManager.getValidAcquiredId();

            midSteps.insertMidFieldsAndValidateValues(merchantId, merchantCategoryCode, merchantNameAndLocation, acquiredId);

            midSteps.clickButtonCreateMid();

            iHelpers.sleepTwoSeconds();

            int length = storeSteps.acceptanceServiceContractsStore().length;

            String [] contractInformation = storeSteps.acceptanceServiceContractsStore()[length-1].split(" ");

            Assertions.assertEquals(contractInformation[0], merchantId,
                    "It was expected to validate merchantId");

            Assertions.assertEquals(contractInformation[1], merchantCategoryCode,
                    "It was expected to validate merchantCategoryCode created");

            Assertions.assertEquals(contractInformation[2], merchantNameAndLocation,
                    "It was expected to validate merchantNameAndLocation created");

            Assertions.assertTrue(midSteps.validateAddMidButtonIsDisabled(),
                    "It was expected to validate create mid button is disabled");

        }

        else {
            Assertions.assertNotEquals("No Acceptance services Contracts are defined",
                    storeSteps.acceptanceServiceContractsStore()[3],
                    "It was expected to validate create mid button is disabled");
        }
    }

    @Deprecated
    @DisplayName("Select VISA radio button")
    void ff9fa_2818() throws IOException{
        midSteps.selectVisaRadioButton();

        Assertions.assertTrue(midSteps.validateCreateMidModalAfterAcceptanceServiceSelection(),
                "It was expected the modal create mid fields valid");
    }

    @Deprecated
    @DisplayName("Select MASTERCARD radio button")
    void ff9fa_2819() throws IOException, InterruptedException {
        midSteps.selectMastercardRadioButton();

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(midSteps.validateCreateMidModalAfterAcceptanceServiceSelection(),
                "It was expected the modal create mid fields valid");
    }

    @Deprecated
    @DisplayName("Select CB radio button")
    void ff9fa_2820() throws IOException{
        midSteps.selectCbRadioButton();

        Assertions.assertTrue(midSteps.validateCreateMidModalAfterAcceptanceServiceSelection(),
                "It was expected the modal create mid fields valid");
    }

    @Deprecated
    @DisplayName("Change the selection to different radio button value")
    void ff9fa_2821() throws IOException, InterruptedException {
        String merchantId = iFakerManager.getValidMerchantIdWithAlphanumeric();

        String merchantCategoryCode = iFakerManager.getValidMerchantCategoryCode();

        String merchantNameAndLocation = iFakerManager.getValidMerchantNameAndLocation();

        String acquiredId = iFakerManager.getValidAcquiredId();

        midSteps.selectVisaRadioButton();

        midSteps.insertMidFieldsAndValidateValues(merchantId, merchantCategoryCode, merchantNameAndLocation, acquiredId);

        midSteps.selectMastercardRadioButton();

        midSteps.validateEmptyRequiredFields();

        midSteps.insertMidFieldsAndValidateValues(merchantId, merchantCategoryCode, merchantNameAndLocation, acquiredId);

        midSteps.selectCbRadioButton();

        midSteps.validateEmptyRequiredFields();

        midSteps.insertMidFieldsAndValidateValues(merchantId, merchantCategoryCode, merchantNameAndLocation, acquiredId);

        midSteps.selectVisaRadioButton();

        midSteps.validateEmptyRequiredFields();
    }

    @Deprecated
    @DisplayName("Informing meta fields and click cancel button")
    void ff9fa_2823() throws IOException, InterruptedException {
        midSteps.selectVisaRadioButton();

        String merchantId = iFakerManager.getValidMerchantIdWithAlphanumeric();

        String merchantCategoryCode = iFakerManager.getValidMerchantCategoryCode();

        String merchantNameAndLocation = iFakerManager.getValidMerchantNameAndLocation();

        String acquiredId = iFakerManager.getValidAcquiredId();

        midSteps.insertMidFields(merchantId, merchantCategoryCode, merchantNameAndLocation, acquiredId);

        iHelpers.sleepOneSecond();

        commonSteps.clickButtonCancelInModal();

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(storeSteps.validateEditButtonDisplayedAndDisabled() &&
                        storeSteps.validateEditButtonText(),
                "It was expected to validate edit button displayed and disabled with expected text");
    }

    @Test
    @DisplayName("Empty mandatory fields and click create MID")
    void ff9fa_2824() throws IOException, InterruptedException {
        commonSteps.selectElementInList("BEFleetqrgx03");

        commonSteps.clickFirstColumnInList();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickOnStore();

        iHelpers.sleepTwoSeconds();

        storeSteps.clickAddMidButton();

        midSteps.clickButtonCreateMid();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(4, commonSteps.getRequiredFieldsListSize(),
                "It was expected 4 required fields");
    }

    @Test
    @DisplayName("Validate required fields alert")
    void NoJiraTest() throws IOException, InterruptedException {
        commonSteps.selectElementInList("BEFleetqrgx03");

        commonSteps.clickFirstColumnInList();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickOnStore();

        iHelpers.sleepTwoSeconds();

        storeSteps.clickAddMidButton();

        midSteps.clickButtonCreateMid();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(4, commonSteps.getRequiredFieldsListSize(),
                "It was expected 4 required fields");

        String merchantId = iFakerManager.getValidMerchantIdWithAlphanumeric();

        midSteps.insertMerchantIdField(merchantId);

        Assertions.assertEquals(3, commonSteps.getRequiredFieldsListSize(),
                "It was expected 3 required fields");

        String merchantCategoryCode = iFakerManager.getValidMerchantCategoryCode();

        midSteps.insertMerchantCategoryCode(merchantCategoryCode);

        Assertions.assertEquals(2, commonSteps.getRequiredFieldsListSize(),
                "It was expected 2 required fields");

        String merchantNameAndLocation = iFakerManager.getValidMerchantNameAndLocation();

        midSteps.insertNameAndLocation(merchantNameAndLocation);

        Assertions.assertEquals(1, commonSteps.getRequiredFieldsListSize(),
                "It was expected 1 required fields");

        String acquiredId = iFakerManager.getValidAcquiredId();

        midSteps.insertAcquiredId(acquiredId);

        iHelpers.sleepOneSecond();

        Assertions.assertTrue( commonSteps.validateNoRequiredFieldPresent(),
                "It was expected 0 required fields");
    }

    @Deprecated
    @DisplayName("Validate MCC field when click create MID")
    void ff9fa_2825() throws IOException, InterruptedException {
        String merchantId = iFakerManager.getValidMerchantIdWithAlphanumeric();

        String merchantNameAndLocation = iFakerManager.getValidMerchantNameAndLocation();

        String acquiredId = iFakerManager.getValidAcquiredId();

        midSteps.selectVisaRadioButton();

        midSteps.insertRequiredFieldsClickCreateAndValidateMccErrorMessage(merchantId,
                iFakerManager.getInvalidMerchantCategoryCode(), merchantNameAndLocation, acquiredId); 

        midSteps.selectMastercardRadioButton();

        midSteps.insertRequiredFieldsClickCreateAndValidateMccErrorMessage(merchantId,
                iFakerManager.getInvalidMerchantCategoryCode(),merchantNameAndLocation, acquiredId);

        midSteps.selectCbRadioButton();

        midSteps.insertRequiredFieldsClickCreateAndValidateMccErrorMessage(merchantId,
                iFakerManager.getInvalidMerchantCategoryCode(),merchantNameAndLocation, acquiredId);
    }

    @Deprecated
    @DisplayName("create VISA MID")
    void ff9fa_2828() throws IOException, InterruptedException {
        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepTwoSeconds();

        commonSteps.clickTabFleet();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");

        iHelpers.sleepOneSecond();

        String fleetName = iFakerManager.getValidFleetName();

        clientCreateFleet.createFleet(fleetName);

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        commonSteps.selectElementInList(fleetName);

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        clientCreateStoreGroup.createStoreGroup(storeGroupName, iFakerManager.getValidNote());

        Assertions.assertEquals(storeGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");

        commonSteps.selectElementInList(storeGroupName);

        String storeName = iFakerManager.getValidStoreName();

        String notes = iFakerManager.getValidNote();

        clientCreateStore.createStore(storeName, notes);

        Assertions.assertEquals(storeName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name at first line");

        commonSteps.clickFirstColumnInList();

        String merchantId = iFakerManager.getValidMerchantIdWithAlphanumeric();

        String merchantCategoryCode = iFakerManager.getValidMerchantCategoryCode();

        String merchantNameAndLocation = iFakerManager.getValidMerchantNameAndLocation();

        String acquiredId = iFakerManager.getValidAcquiredId();

        storeSteps.clickAddMidButton();

        iHelpers.sleepOneSecond();

        midSteps.selectVisaRadioButton();

        midSteps.insertMidFields(merchantId, merchantCategoryCode, merchantNameAndLocation, acquiredId);

        iHelpers.sleepOneSecond();

        midSteps.clickButtonCreateMid();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals("Merchant Id MCC Name and Location Remove",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextFirstListHeader()),
                "It was expected to validate table header");

        Assertions.assertEquals(1, commonSteps.getContentFirstList().size(), "It was expected 1");

        String getContentFirsList = commonSteps.getContentFirstList().get(0);

        Assertions.assertTrue(getContentFirsList.startsWith(merchantId) &&
                getContentFirsList.contains(merchantCategoryCode) && getContentFirsList.contains(merchantNameAndLocation)
                && !getContentFirsList.contains(acquiredId) && getContentFirsList.endsWith("VISA"));
    }
}