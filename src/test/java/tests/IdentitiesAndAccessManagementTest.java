package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import driverhelper.DriverHelper;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import properties.IReadProperties;
import properties.Properties;
import steps.Identities.IdentitiesSteps;
import steps.common.CommonSteps;
import steps.login.LoginSteps;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class IdentitiesAndAccessManagementTest {

    private static CommonSteps commonSteps;

    private static IdentitiesSteps identitiesSteps;

    private static Properties properties;

    private static IFakerManager iFakerManager;

    static DriverHelper driverHelper;

    static LoginSteps loginSteps;

    private static IHelpers iHelpers;

    private static WebDriver driver;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        WebDriverManager.chromedriver().setup();

        ChromeOptions opt = new ChromeOptions();

        opt.setHeadless(true);

//        opt.addArguments("--no-sandbox");
//        opt.addArguments("--window-size=1420,1080");
//        opt.addArguments("--headless");
//        opt.addArguments("--disable-gpu");

        driver = new ChromeDriver(opt);

        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        properties = iReadProperties.readFromYaml();

        commonSteps = new CommonSteps(driver, properties);

        identitiesSteps = new IdentitiesSteps(driver, properties);

        iFakerManager = new FakerManager();

        iHelpers = injector.getInstance(IHelpers.class);

        driverHelper = new DriverHelper(driver, properties);

        loginSteps = new LoginSteps(driver, properties);

        driverHelper.navigateToUrl(properties.getCockpitBaseUrl());

        loginSteps.login(properties.getCockpitUsername(), properties.getCockpitPassword());
    }

    @BeforeEach
    public void initialTestConditions() throws IOException, InterruptedException {
        commonSteps.clickIdentitiesLink();

        iHelpers.sleepFiveSeconds();
    }

    @AfterAll
    public static void afterAll(){
        driver.quit();
    }

    @Test
    @DisplayName("Validate Identities and Access Management Page")
    void PAY_8239_8242() throws IOException, InterruptedException {
        Assertions.assertEquals("[, Username, Client ID (Username for application), Email, Realm, Status]",
                identitiesSteps.getTextHeader(), "It was expected to validate identities header");

        Assertions.assertEquals("10",identitiesSteps.getTextRowsPerPage(),
                "It was expected to validate 10 rows per page default");

        Assertions.assertEquals(10, identitiesSteps.getTableRows().size(),
                "It was expected to validate 10 results per page");

        identitiesSteps.scrollDownInsideTable(9);

        iHelpers.sleepTwoSeconds();

        identitiesSteps.clickRowsPerPage("50");

        Assertions.assertEquals("50",identitiesSteps.getTextRowsPerPage(),
                "It was expected to validate 10 rows per page default");

        Assertions.assertEquals(50, identitiesSteps.getTableRows().size(),
                "It was expected to validate 10 results per page");

        identitiesSteps.scrollDownInsideTable(49);

        iHelpers.sleepTwoSeconds();

        identitiesSteps.clickRowsPerPage("20");

        Assertions.assertEquals("20",identitiesSteps.getTextRowsPerPage(),
                "It was expected to validate 10 rows per page default");

        Assertions.assertEquals(20, identitiesSteps.getTableRows().size(),
                "It was expected to validate 10 results per page");

        identitiesSteps.scrollDownInsideTable(19);
    }

    @Test
    @DisplayName("Validate clicking on Next button until end and validate number of results per page")
    void PAY_8294() throws IOException, InterruptedException {
        Assertions.assertEquals("10",identitiesSteps.getTextRowsPerPage(),
                "It was expected to validate 10 rows per page default");

        Assertions.assertEquals(10, identitiesSteps.getTableRows().size(),
                "It was expected to validate 10 results per page");

        identitiesSteps.scrollDownInsideTable(9);

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(identitiesSteps.clickNextButtonWhileEnabledAndValidateNumberOfResultsPerPage(10),
                "It was expected to validate results per page");

    }

    @Test
    @DisplayName("Validate Sorting functionality")
    void PAY_8266() throws IOException {
        String firstRow = identitiesSteps.getTextFirstRow();

        identitiesSteps.clickHeaderUsername();

        String firstRowAfterClickOnHeader = identitiesSteps.getTextFirstRow();

        Assertions.assertNotEquals(firstRow,firstRowAfterClickOnHeader,
                "It was expected to validate sort functionality worked");
    }

    @Test
    @DisplayName("Validate Identities Details")
    void PAY_8306() throws IOException, UnsupportedFlavorException {
        identitiesSteps.clickFirstRow();

        Assertions.assertTrue(identitiesSteps.validateDetailsData(),
                "It was expected to validate Details Data");

        identitiesSteps.clickDivId();

        Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();

        String idFromClipboard = (String) c.getData(DataFlavor.stringFlavor);

        Assertions.assertEquals(idFromClipboard.length(), 36, "It was expected to validate length");
    }

    @Test
    @DisplayName("Validate Email And Username fields are editable")
    void PAY_8309() throws IOException, InterruptedException {
        identitiesSteps.clickFirstRow();

        Assertions.assertTrue(commonSteps.validateSpanSaveAndCancelNotPresent(),
                "It was expected to validate buttons Save and cancel not present");

        identitiesSteps.clickDivEditableFieldByPosition(0);

        Assertions.assertTrue(commonSteps.validateSpanSaveAndCancelArePresent(),
                "It was expected to validate buttons Save and cancel not present");

    }

    @Test
    @DisplayName("Validate Edition of Email And Username fields")
    void PAY_8310_l8311() throws IOException, InterruptedException {
        identitiesSteps.clickFirstRow();

        Assertions.assertTrue(commonSteps.validateSpanSaveAndCancelNotPresent(),
                "It was expected to validate buttons Save and cancel not present");

        identitiesSteps.clearEditableFieldUsername();

        String newUsername = iFakerManager.getValidUsername();

        identitiesSteps.sendTextEditableFieldUsername(newUsername);

        commonSteps.clickConfirmationChangesSpanSave();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals(identitiesSteps.getTextEditableFieldByPosition(0),newUsername,
                "It was expected to validate username changed");

        String actualEmail = identitiesSteps.getTextEditableFieldByPosition(1); //0 is username 1 is email

        identitiesSteps.clearEditableFieldEmail();

        String newEmail = iFakerManager.getValidUsername();

        identitiesSteps.sendTextEditableFieldEmail(newEmail);

        commonSteps.clickConfirmationChangesSpanCancel();

        Assertions.assertEquals(identitiesSteps.getTextEditableFieldByPosition(1),actualEmail,
                "It was expected to validate email has not changed");
    }

    @Test
    @DisplayName("Validate error message when username has less than 6 chars")
    void PAY_8317() throws IOException {
        identitiesSteps.clickFirstRow();

        String actualUsername = identitiesSteps.getTextEditableFieldByPosition(0); //0 is username 1 is email

        identitiesSteps.clearEditableFieldUsername();

        commonSteps.clickConfirmationChangesSpanSave();

        Assertions.assertTrue(identitiesSteps.validateUsernameSizeErrorMessage(),
                "It was expected to validate username error message");

        commonSteps.clickButtonCloseModalDialogPopUp();

        Assertions.assertEquals(identitiesSteps.getTextEditableFieldByPosition(0),actualUsername,
                "It was expected to validate username has not changed");
    }

    @Test
    @DisplayName("Validate select and deselect checkboxes")
    void PAY_8303_8304() throws IOException {
        Assertions.assertEquals("10",identitiesSteps.getTextRowsPerPage(),
                "It was expected to validate 10 rows per page default");

        Assertions.assertEquals(10, identitiesSteps.getTableRows().size(),
                "It was expected to validate 10 results per page");

        Assertions.assertTrue(identitiesSteps.validateFirstCheckboxDataIndeterminate("false"),
                "It was expected to validate data indeterminate");

        commonSteps.clickSecondCheckbox();

        Assertions.assertEquals(1,commonSteps.numberOfCheckboxesAreSelected(),
                "It was expected to validate 1 checkbox selected");

        Assertions.assertTrue(identitiesSteps.validateFirstCheckboxDataIndeterminate("true"),
                "It was expected to validate data indeterminate");

        commonSteps.clickFirstCheckbox();

        Assertions.assertEquals(11,commonSteps.numberOfCheckboxesAreSelected(),
                "It was expected to validate 11 checkbox selected");

        Assertions.assertTrue(identitiesSteps.validateFirstCheckboxDataIndeterminate("false"),
                "It was expected to validate data indeterminate");

    }

}
