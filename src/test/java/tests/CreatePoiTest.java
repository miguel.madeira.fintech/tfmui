package tests;

import appinjector.AppInjector;
import client.ClientCreateFleet;
import client.ClientCreateStore;
import client.ClientCreateStoreGroup;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ModalCommonsComponent;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.poi.PoiSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CreatePoiTest extends NewWindow {

    //Tests from ff9fa_2835 to ff9fa_2884 related to US-2678

    private static ClientCreateFleet clientCreateFleet;

    private static ClientCreateStoreGroup clientCreateStoreGroup;

    private static ClientCreateStore clientCreateStore;

    private static FleetSteps fleetSteps;

    private static StoreGroupSteps storeGroupSteps;

    private static StoreSteps storeSteps;

    private static PoiSteps poiSteps;

    private static CommonSteps commonSteps;

    private static IFakerManager iFakerManager;

    private static Properties properties;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        properties = iReadProperties.readFromYaml();

        iFakerManager = new FakerManager();

        fleetSteps = new FleetSteps(driver, properties);

        storeSteps = new StoreSteps(driver, properties);

        poiSteps = new PoiSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        clientCreateFleet = new ClientCreateFleet(driver, properties);

        clientCreateStoreGroup = new ClientCreateStoreGroup(driver, properties);

        clientCreateStore = new ClientCreateStore(driver, properties);
    }

    @BeforeEach
    public void initTestConditions() throws InterruptedException {
        iHelpers.sleepFiveSeconds();
    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepTwoSeconds();

        commonSteps.clickTabFleet();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate poi modal component")
    void ff9fa_2835() throws IOException, InterruptedException {
        commonSteps.clickFirstColumnInList();

        iHelpers.sleepTwoSeconds();

        storeGroupSteps.createStoreGroupIfNecessary(iFakerManager.getValidStoreGroupName());

        iHelpers.sleepOneSecond();

        commonSteps.clickFirstColumnInList();

        iHelpers.sleepTwoSeconds();

        storeSteps.createStoreIfNecessary(iFakerManager.getValidStoreName(), "");

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        storeSteps.clickAddPoiButton();

        Assertions.assertTrue(poiSteps.validateCreatePoiModal(), "It was expected true");

        commonSteps.clickButtonCloseModalDialog();

        Assertions.assertTrue(storeSteps.validateButtonAddPoiDisplayedAndEnabled(),
                "It was expected the button Add poi displayed and enabled");
    }

    @Test
    @DisplayName("Validate trying to create poi store without MID Contract")
    void ff9fa_2859() throws IOException, InterruptedException {
        String fleetName = iFakerManager.getValidFleetName();

        clientCreateFleet.createFleet(fleetName);

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        commonSteps.selectElementInList(fleetName);

        iHelpers.sleepOneSecond();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        clientCreateStoreGroup.createStoreGroup(storeGroupName, iFakerManager.getValidNote());

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(storeGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");

        commonSteps.selectElementInList(storeGroupName);

        iHelpers.sleepTwoSeconds();

        String storeName = iFakerManager.getValidStoreName();

        String notes = iFakerManager.getValidNote();

        clientCreateStore.createStore(storeName, notes);

        Assertions.assertEquals(storeName, commonSteps.getTextFirstStoreList(),
                "It was expected store name at first line");

        commonSteps.clickOnStore();

        iHelpers.sleepOneSecond();

        storeSteps.clickAddPoiButton();

        iHelpers.sleepTwoSeconds();

        poiSteps.insertFieldsCreatePoiModal(notes);

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(poiSteps.validateErrorMessageNoAcceptanceServiceContract(),
                "It was expected the message no acceptance services contracts displayed");
    }

    @Test
    @DisplayName("Validate trying create poi fleet state draft")
    void ff9fa_2860_0() throws IOException, InterruptedException {
        commonSteps.selectElementInList("BEFleetmdnx57");

        iHelpers.sleepTwoSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepTwoSeconds();

        storeSteps.clickAddPoiButton();

        poiSteps.insertFieldsCreatePoiModal("");

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(poiSteps.validateErrorMessageFleetStateDraft(),
                "It was expected the message fleet state draft displayed");
    }

    @Test
    @DisplayName("Validate trying create poi no active poi profile")
    void ff9fa_2860_1() throws IOException, InterruptedException {
        commonSteps.selectElementInList("BEFleetqtyv61");

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        storeSteps.clickAddPoiButton();

        poiSteps.insertFieldsCreatePoiModal("");

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateErrorMessageNoActivePoiProfile(),
                "It was expected the message no active poi profile displayed");
    }

    @Test
    @DisplayName("Create poi with VISA MID active store")
    void ff9fa_2863() throws IOException, InterruptedException {
        commonSteps.selectElementInList("BEFleetxktl68");

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        commonSteps.selectElementInList("stqgarye");

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResultsBeforeCreation = commonSteps.getContentLastList();

        storeSteps.clickAddPoiButton();

        iHelpers.sleepOneSecond();

        poiSteps.insertFieldsCreatePoiModal("");

        iHelpers.sleepFiveSeconds();

        Assertions.assertEquals("POI ID Terminal ID Profile State Configuration status Enabled",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextPoiHeader()));

        List<String> tableContentResults = commonSteps.getContentLastList();

        Assertions.assertEquals(tableContentResults.size() - 1, tableContentResultsBeforeCreation.size());

        Assertions.assertTrue(poiSteps.validateAllPoisAreEnabled(),
                "It was expected to validate enabled column ");

        //Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults, "V","X") ,
              //  "It was expected the column enable with the possible values V or X");

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults,
                "Available to Poi","Locally Modified") ,
                "It was expected the column enable with values Available to Poi or Locally Modified");

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults, "IN USE","NEW") ,
                "It was expected the column enable with the possible values In Use or New");
    }

    @Test
    @DisplayName("Create poi with MASTERCARD MID active store")
    void ff9fa_2864() throws IOException, InterruptedException {
        commonSteps.selectElementInList(properties.getFleetWithActiveMastercardMid());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        storeSteps.clickAddPoiButton();

        List<String> tableContentResultsBeforeCreation = commonSteps.getContentLastList();

        iHelpers.sleepOneSecond();

        poiSteps.insertFieldsCreatePoiModal("");

        iHelpers.sleepFiveSeconds();

        Assertions.assertEquals("POI ID Terminal ID Profile State Configuration status Enabled",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextPoiHeader()));

        List<String> tableContentResults = commonSteps.getContentLastList();

        Assertions.assertEquals(tableContentResults.size() - 1, tableContentResultsBeforeCreation.size());

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults,
                "Available to Poi","Locally Modified") ,
                "It was expected the column enable with values Available to Poi or Locally Modified");

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults, "IN USE","NEW") ,
                "It was expected the column enable with the possible values In Use or New");
    }

    @Test
    @DisplayName("Create poi with poi profile active store")
    void ff9fa_2865() throws IOException, InterruptedException {
        commonSteps.selectElementInList(properties.getFleetForPlatforms());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResultsBeforePoiCreation = commonSteps.getContentLastList();

        storeSteps.clickAddPoiButton();

        iHelpers.sleepOneSecond();

        poiSteps.insertFieldsCreatePoiModal("");

        iHelpers.sleepFiveSeconds();

        Assertions.assertEquals("POI ID Terminal ID Profile State Configuration status Enabled",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextPoiHeader()));

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResults = commonSteps.getContentLastList();

        Assertions.assertEquals(tableContentResults.size() - 1, tableContentResultsBeforePoiCreation.size());

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults,
                "Available to Poi","Locally Modified") ,
                "It was expected the column enable with values Available to Poi or Locally Modified");

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults, "IN USE","NEW") ,
                "It was expected the column enable with the possible values In Use or New");
    }

    @Test
    @DisplayName("Create poi with poi profile active store group")
    void ff9fa_2866() throws IOException, InterruptedException {
        commonSteps.selectElementInList("BEFleetbqzz76");

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResultsBeforePoiCreation = commonSteps.getContentLastList();

        storeSteps.clickAddPoiButton();

        iHelpers.sleepTwoSeconds();

        poiSteps.insertFieldsCreatePoiModal("");

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals("POI ID Terminal ID Profile State Configuration status Enabled",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextPoiHeader()));

        List<String> tableContentResults = commonSteps.getContentLastList();

        Assertions.assertEquals(tableContentResults.size() - 1, tableContentResultsBeforePoiCreation.size());

        Assertions.assertTrue(poiSteps.validateAllPoisAreEnabled(),
                "It was expected to validate enabled column ");

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults,
                "Available to Poi","Locally Modified") ,
                "It was expected the column enable with values Available to Poi or Locally Modified");

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults, "IN USE","NEW") ,
                "It was expected the column enable with the possible values In Use or New");
    }

    @Test
    @DisplayName("Create poi with poi profile active fleet")
    void ff9fa_2867() throws IOException, InterruptedException {
        commonSteps.selectElementInList("BEFleetpwid90");

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResultsBeforePoiCreation = commonSteps.getContentLastList();

        iHelpers.sleepOneSecond();

        storeSteps.clickAddPoiButton();

        iHelpers.sleepOneSecond();

        poiSteps.insertFieldsCreatePoiModal("");

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals("POI ID Terminal ID Profile State Configuration status Enabled",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextPoiHeader()));

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResults = commonSteps.getContentLastList();

        Assertions.assertEquals(tableContentResults.size() - 1, tableContentResultsBeforePoiCreation.size());

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults,
                "Available to Poi","Locally Modified") ,
                "It was expected the column enable with values Available to Poi or Locally Modified");

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults, "IN USE","NEW") ,
                "It was expected the column enable with the possible values In Use or New");
    }

    @Test
    @DisplayName("Create poi with poi profile active store without selectors and active PP at fleet")
    void ff9fa_2884() throws IOException, InterruptedException {
        commonSteps.selectElementInList("BEFleetghbe01");

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResultsBeforePoiCreation = commonSteps.getContentLastList();

        storeSteps.clickAddPoiButton();

        iHelpers.sleepOneSecond();

        poiSteps.insertFieldsCreatePoiModal("");

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        Assertions.assertEquals("POI ID Terminal ID Profile State Configuration status Enabled",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextPoiHeader()));

        List<String> tableContentResults = commonSteps.getContentLastList();

        Assertions.assertEquals(tableContentResults.size() - 1, tableContentResultsBeforePoiCreation.size());

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults,
                "Available to Poi","Locally Modified") ,
                "It was expected the column enable with values Available to Poi or Locally Modified");

        Assertions.assertTrue(commonSteps.validateResultsInList(tableContentResults, "IN USE","NEW") ,
                "It was expected the column enable with the possible values In Use or New");
    }

    @Test
    @DisplayName("Validate Deploy button, pop up message and back end call when button Yes is selected")
    void ff9fa_3221_3223() throws IOException, InterruptedException {
        commonSteps.selectElementInList(properties.getFleetForRetailerAndDeployPoi());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList(properties.getPoiForFLeetForRetailerAndDeployPoi());

        Assertions.assertTrue(poiSteps.validateDeployButton(),
                "It was expected to validate Deploy button");

        poiSteps.clickButtonDeploy();

        Assertions.assertTrue(poiSteps.validateDivDeployChangesText("Are you sure you want to deploy the" +
                " configuration changes?"),"It was expected to validate text");

        Assertions.assertTrue(poiSteps.validateNumberOfButtonsPopUpDeployPoi(2),
                "It was expected to validate 2 buttons");

        iHelpers.sleepTwoSeconds();

        poiSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepFiveSeconds();

        List <String> listComplete = iHelpers.networkRequestsComplete(driver);

        Assertions.assertEquals(2, poiSteps.validateListContainsCallToBackEnd(listComplete,
                properties.getPoiForFLeetForRetailerAndDeployPoi()).size(),
                "It was expected to validate call to back end to deploy POI");

        List <String> listWithErrors =iHelpers.networkRequestError(driver);

        Assertions.assertEquals(0, poiSteps.validateNetworkCallsError(listWithErrors,
                properties.getPoiForFLeetForRetailerAndDeployPoi()).size(),
                "It was expected to validate call to back end to deploy POI was successfully");
    }

    @Test
    @DisplayName("Validate Deploy button, pop up message and back end call when button No is selected")
    void ff9fa_3222() throws IOException, InterruptedException {
        commonSteps.selectElementInList(properties.getFleetForRetailerAndDeployPoi());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList(properties.getPoiForFLeetForRetailerAndDeployPoi());

        Assertions.assertTrue(poiSteps.validateDeployButton(),
                "It was expected to validate Deploy button");

        poiSteps.clickButtonDeploy();

        Assertions.assertTrue(poiSteps.validateDivDeployChangesText("Are you sure you want to deploy the" +
                " configuration changes?"),"It was expected to validate text");

        Assertions.assertTrue(poiSteps.validateNumberOfButtonsPopUpDeployPoi(2),
                "It was expected to validate 2 buttons");

        poiSteps.clickConfirmationChangesSpanNo();

        iHelpers.sleepFiveSeconds();

        List<String> listComplete = iHelpers.networkRequestsComplete(driver);

        Assertions.assertEquals(0, poiSteps.validateListContainsCallToBackEnd(listComplete,
                properties.getPoiForFLeetForRetailerAndDeployPoi()).size(),
                "It was expected to validate call to back end to deploy POI");
    }

    @Test
    @DisplayName("Validate Poi is Available to Poi When Poi was locally modified and deployed")
    void NoJiraTest() throws InterruptedException, IOException {
        commonSteps.selectElementInList("BEFleetwqni57");

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList("styytzeq");

        iHelpers.sleepFiveSeconds();

        poiSteps.clickFirstPoiByConfigurationStatusOnPoiDetails("Locally Modified");

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateDivConfigurationStatus("Locally Modified"),
                "It was expected to validate status");

        poiSteps.clickButtonDeploy();

        poiSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepTenSeconds();

        Assertions.assertTrue(poiSteps.validateDivConfigurationStatus("Available to Poi"),
                "It was expected to validate status");
    }

    @Test
    @DisplayName("Validate platform dropdown options")
    void ff9fa_3380() throws IOException, InterruptedException {
        commonSteps.selectElementInList(properties.getFleetWithActiveMastercardMid());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        storeSteps.clickAddPoiButton();

        List <String> lst = Arrays.asList(poiSteps.getTextFromPlatformDropdown().get(0).split("\n"));

        Assertions.assertTrue(lst.size() >= 2,
                "It was expected to validate lst size");

        Assertions.assertTrue(lst.contains("PAX S920"),
                "It was expected to validate dropdown list contains PAX S920");

        Assertions.assertTrue(lst.contains("PAX D190"),
                "It was expected to validate dropdown list contains PAX D190");
    }

    @Test
    @DisplayName("Create POI with platform PAX S920")
    void ff9fa_3381() throws IOException, InterruptedException {
        commonSteps.selectElementInList(properties.getFleetForPlatforms());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResultsBeforeCreation = commonSteps.getContentLastList();

        storeSteps.clickAddPoiButton();

        List <String> lst = Arrays.asList(poiSteps.getTextFromPlatformDropdown().get(0).split("\n"));

        int position = lst.indexOf("PAX S920");

        poiSteps.insertFieldsCreatePoiModalSpecificPlatform("", position+1);

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResults = commonSteps.getContentLastList();

        Assertions.assertEquals(tableContentResults.size() - 1, tableContentResultsBeforeCreation.size());

        commonSteps.clickFirstPoiInList();

        Assertions.assertTrue(poiSteps.validatePlatformChosen("PAX S920"),
                "It was expected to validate platform");


    }

    @Test
    @DisplayName("Create POI with platform PAX D190")
    void ff9fa_3382() throws IOException, InterruptedException {
        commonSteps.selectElementInList(properties.getFleetForPlatforms());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResultsBeforeCreation = commonSteps.getContentLastList();

        storeSteps.clickAddPoiButton();

        List <String> lst = Arrays.asList(poiSteps.getTextFromPlatformDropdown().get(0).split("\n"));

        int position = lst.indexOf("PAX D190");

        poiSteps.insertFieldsCreatePoiModalSpecificPlatform("", position+1);

        iHelpers.sleepFiveSeconds();

        List<String> tableContentResults = commonSteps.getContentLastList();

        Assertions.assertEquals(tableContentResults.size() - 1, tableContentResultsBeforeCreation.size());

        commonSteps.clickFirstPoiInList();

        Assertions.assertTrue(poiSteps.validatePlatformChosen("PAX D190"),
                "It was expected to validate platform");
    }

}