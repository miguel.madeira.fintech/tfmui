package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import driverhelper.DriverHelper;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindowdifferentusers.NewWindowMaintainer;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.poi.PoiSteps;
import steps.poigroup.PoiGroupSteps;
import steps.poiprofile.PoiProfileSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;

public class MaintainerTest extends NewWindowMaintainer {

    private static FleetSteps fleetSteps;

    private static StoreGroupSteps storeGroupSteps;

    private static StoreSteps storeSteps;

    private static PoiSteps poiSteps;

    private static PoiGroupSteps poiGroupSteps;

    private static PoiProfileSteps poiProfileSteps;

    private static CommonSteps commonSteps;

    private static IHelpers iHelpers;

    private static IFakerManager iFakerManager;

    private static DriverHelper driverHelper;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iFakerManager = injector.getInstance(IFakerManager.class);

        iHelpers = injector.getInstance(IHelpers.class);

        Properties properties = iReadProperties.readFromYaml();

        fleetSteps = new FleetSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        storeSteps = new StoreSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        poiSteps = new PoiSteps(driver, properties);

        poiGroupSteps = new PoiGroupSteps(driver, properties);

        poiProfileSteps = new PoiProfileSteps(driver, properties);

        driverHelper = new DriverHelper(driver, properties);
    }

    @BeforeEach
    public void initialTestConditions() throws IOException, InterruptedException {
        iHelpers.sleepFiveSeconds();

        commonSteps.clickTabFleet();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();
    }

    @Test
    @DisplayName("Validate permissions for maintainer")
    void ff9fa_3177() throws IOException, InterruptedException {
        Assertions.assertTrue(fleetSteps.validateFleetListPresent(), "It was expected the fleet list present");

        Assertions.assertTrue(poiSteps.validatePoiTabPresent(), "It was expected the poiTab Present");

        Assertions.assertTrue(poiGroupSteps.validateTabPoiGroupDisplayedAndEnabled(),
                "It was expected the poiGroupTab displayed and enabled");

        Assertions.assertTrue(poiProfileSteps.validateTabPoiProfileDisplayedAndEnabled(),
                "It was expected the poiProfile displayed and enabled");

        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        Assertions.assertTrue(poiSteps.validatePoiTab(), "It was expected to validate poi tab");

        Assertions.assertTrue(poiSteps.validateMerchantPin(), "It was expected not empty");

        Assertions.assertTrue(poiSteps.validateMerchantPinDefaultPolicy(),
                "It was expected POI ID or Fixed");
    }

    @Test
    @DisplayName("Validate operation actions for maintainer")
    void ff9fa_3174() throws InterruptedException, IOException {
        Assertions.assertTrue(fleetSteps.validateFleetListPresent(),
                "It was expected the fleet list present");

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button add fleet present");

        Assertions.assertTrue(commonSteps.validateButtonDeleteFleetsDisplayed(),
                "It was expected the button remove fleet present");

        commonSteps.selectElementInList("mantainer");

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(storeGroupSteps.validateButtonAddStoreGroupDisplayedAndEnabled(),
                "It was expected the button add store group  present");

        Assertions.assertTrue(commonSteps.validateButtonDeleteStoreGroupIsDisplayed(),
                "It was expected the button remove store group  present");

        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(storeSteps.validateButtonAddStoreDisplayedAndEnabled(),
                "It was expected the button add store  present");

        Assertions.assertTrue(commonSteps.validateButtonDeleteStoreIsDisplayed(),
                "It was expected the button remove store group not present");

        commonSteps.clickOnStore();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateButtonAddPoiDisplayedAndEnabled(),
                "It was expected the button add poi displayed and enabled");

        iHelpers.sleepTwoSeconds();

        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateCheckboxInAllPoiS(),
                "It was expected the same number of elements");

        poiSteps.clickFirstPoiInList();

        iHelpers.sleepTwoSeconds();

        iHelpers.sleepFiveSeconds();

        driverHelper.scrollBy();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateMerchantPinDisplayedAndEnabled(),
                "It was expected the Merchant Pin displayed and enabled");

        commonSteps.clickTabPoiGroup();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiGroupSteps.validateTabPoiGroupDisplayedAndEnabled(),
                "It was expected the button poi group displayed and enabled");

        Assertions.assertTrue(commonSteps.validateButtonRemoveIsPresent(),
                "It was expected the button remove poi group displayed");
    }

    @Test
    @DisplayName("Validate retailer does not have permission to create POI label")
    void ff9fa_3361_3363_3587() throws IOException, InterruptedException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        poiSteps.inputSelectFirstPoiInList();

        Assertions.assertTrue(poiSteps.validateButtonAddLabelIsPresent(),
                "It was expected the button add label not present");

        Assertions.assertTrue(poiSteps.validateDeletePoiButtonIsEnabled(),
                "It was expected button enabled");

    }

    @Test
    @DisplayName("Validate pop up to create POI Label and create button")
    void ff9fa_3366() throws IOException, InterruptedException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        String actualLabels = poiSteps.getLabelsOnFirstPoiInList();

        if(actualLabels.length() == 0) {
            poiSteps.inputSelectFirstPoiInList();

            poiSteps.clickAddLabelButton();

            poiSteps.insertLabelName(iFakerManager.getValidLabelName());

            commonSteps.clickButtonCreateInModal();

            iHelpers.sleepFiveSeconds();

            String labelsAfterCreation = poiSteps.getLabelsOnFirstPoiInList();

            Assertions.assertTrue(labelsAfterCreation.split(",").length == 1 ,
                    "It was expected to validate new label was created");
        }

        else {

            poiSteps.inputSelectFirstPoiInList();

            poiSteps.clickAddLabelButton();

            poiSteps.insertLabelName(iFakerManager.getValidLabelName());

            commonSteps.clickButtonCreateInModal();

            iHelpers.sleepFiveSeconds();

            String labelsAfterCreation = poiSteps.getLabelsOnFirstPoiInList();

            Assertions.assertTrue(labelsAfterCreation.split(",").length ==
                            actualLabels.split(",").length +1,
                    "It was expected to validate new label was created");
        }
    }



    @Test
    @DisplayName("Validate pop up to create POI Label and cancel button")
    void ff9fa_3365() throws IOException, InterruptedException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        String actualLabels = poiSteps.getLabelsOnFirstPoiInList();

        poiSteps.inputSelectFirstPoiInList();

        poiSteps.clickAddLabelButton();

        poiSteps.insertLabelName(iFakerManager.getValidLabelName());

        commonSteps.clickButtonCancelInModal();

        iHelpers.sleepTwoSeconds();

        String labelsAfterCreation = poiSteps.getLabelsOnFirstPoiInList();

        Assertions.assertEquals(actualLabels, labelsAfterCreation,
                    "It was expected to validate new label was created");
    }

    @Test
    @DisplayName("Validate add label in multiple POIs when one already exists")
    void ff9fa_3391() throws IOException, InterruptedException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        String actualLabels = poiSteps.getLabelsOnFirstPoiInList();

        String firstLabel= iFakerManager.getValidLabelName();

        if(actualLabels.length() == 0) {
            poiSteps.inputSelectFirstPoiInList();

            poiSteps.clickAddLabelButton();

            poiSteps.insertLabelName(firstLabel);

            commonSteps.clickButtonCreateInModal();

            iHelpers.sleepTwoSeconds();

            String labelsAfterCreation = poiSteps.getLabelsOnFirstPoiInList();

            Assertions.assertTrue(labelsAfterCreation.split(",").length == 1 ,
                    "It was expected to validate new label was created");

            Assertions.assertTrue(labelsAfterCreation.contains(firstLabel.toUpperCase()),
                    "It was expected to validate new label was created");
        }

        else {

            poiSteps.inputSelectFirstPoiInList();

            poiSteps.clickAddLabelButton();

            poiSteps.insertLabelName(firstLabel);

            commonSteps.clickButtonCreateInModal();

            iHelpers.sleepTwoSeconds();

            String labelsAfterCreation = poiSteps.getLabelsOnFirstPoiInList();

            Assertions.assertEquals(labelsAfterCreation.split(",").length, actualLabels.split(",")
                            .length + 1, "It was expected to validate new label was created");

            Assertions.assertTrue(labelsAfterCreation.contains(firstLabel.toUpperCase()),
                    "It was expected to validate new label was created");
        }

        poiSteps.inputSelectSecondPoiInList();

        poiSteps.clickAddLabelButton();

        poiSteps.insertLabelName(firstLabel);

        commonSteps.clickButtonCreateInModal();

       Assertions.assertTrue(poiSteps.validateDivDeployChangesText("already has label "+firstLabel
                       .toUpperCase()), "It was expected to validate text");

       poiSteps.clickConfirmationChangesButtonClose();

        String labelsAfterSecondCreation = poiSteps.getLabelsOnFirstPoiInList();

        Assertions.assertEquals(labelsAfterSecondCreation.split(",").length,
                actualLabels.split(",").length + 1,
                "It was expected to validate new label was created");

        Assertions.assertTrue(poiSteps.getLabelsOnSecondPoiInList().contains(firstLabel.toUpperCase()),
                "It was expected to validate new label was created");

    }

}