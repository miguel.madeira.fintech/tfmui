package tests;

import appinjector.AppInjector;
import client.ClientCreateFleet;
import client.ClientCreateStore;
import client.ClientCreateStoreGroup;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ModalCommonsComponent;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import pages.storedetails.StoreDetailsWebElements;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.mid.MidSteps;
import steps.poi.PoiSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;
import java.text.ParseException;

public class StoreDetailsTest extends NewWindow {

    //Tests from ff9fa_2793 to ff9fa_2794 related to US-2682
    //Tests from ff9fa_2786 to ff9fa_2785_2786_2 related to US-2679

    private static ClientCreateFleet clientCreateFleet;

    private static ClientCreateStoreGroup clientCreateStoreGroup;

    private static ClientCreateStore clientCreateStore;

    private static Properties properties;

    private static FleetSteps fleetSteps;

    private static StoreGroupSteps storeGroupSteps;

    private static StoreSteps storeSteps;

    private static PoiSteps poiSteps;

    private static CommonSteps commonSteps;

    private static MidSteps midSteps;

    private static StoreDetailsWebElements storeDetailsWebElements;

    private static IFakerManager iFakerManager;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException, InterruptedException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        properties = iReadProperties.readFromYaml();

        iFakerManager = new FakerManager();

        fleetSteps = new FleetSteps(driver, properties);

        storeSteps = new StoreSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        poiSteps = new PoiSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        midSteps = new MidSteps(driver,properties);

        storeDetailsWebElements = new StoreDetailsWebElements(driver, properties);

        clientCreateFleet = new ClientCreateFleet(driver, properties);

        clientCreateStoreGroup = new ClientCreateStoreGroup(driver, properties);

        clientCreateStore = new ClientCreateStore(driver, properties);

        iHelpers.sleepFiveSeconds();
    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepTwoSeconds();

        commonSteps.clickTabFleet();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate Store Details Data")
    void ff9fa_2770_3649_3650() throws IOException, InterruptedException, ParseException {
        String fleetName = iFakerManager.getValidFleetName();

        String solutionContractName = clientCreateFleet.createFleet(fleetName);

        iHelpers.sleepTenSeconds();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        commonSteps.selectElementInList(fleetName);

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        clientCreateStoreGroup.createStoreGroup(storeGroupName, iFakerManager.getValidNote());

        Assertions.assertEquals(storeGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");

        commonSteps.selectElementInList(storeGroupName);

        String storeName = iFakerManager.getValidStoreName();

        String notes = iFakerManager.getValidNote();

        clientCreateStore.createStore(storeName, notes);

        Assertions.assertEquals(storeName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name at first line");

        commonSteps.clickOnStore();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(midSteps.validateAddMidButtonIsEnabled(),
                    "It was expected to validate create mid button is enabled");

        Assertions.assertTrue(storeSteps.validateButtonRemoveAcceptanceServiceContractIsNotPresent(),
                "It was expected to validate remove not present");

        storeSteps.clickAddMidButton();

        String merchantId = iFakerManager.getValidMerchantIdWithAlphanumeric();

        String merchantCategoryCode = iFakerManager.getValidMerchantCategoryCode();

        String merchantNameAndLocation = iFakerManager.getValidMerchantNameAndLocation();

        String acquiredId = iFakerManager.getValidAcquiredId();

        midSteps.insertMidFieldsAndValidateValues(merchantId, merchantCategoryCode, merchantNameAndLocation, acquiredId);

        midSteps.clickButtonCreateMid();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeSteps.validateButtonRemoveAcceptanceServiceContract(),
                "It was expected to validate remove present");

        Assertions.assertTrue(storeSteps.validateStoreDetails(solutionContractName, fleetName, storeGroupName,storeName,
                "None", notes, "0"), "It was expected to validate all fields");

        Assertions.assertTrue(commonSteps.validateDateIsBeforeNow(storeDetailsWebElements.divCreatedDate()),
                "It was expected to validate created date is before now");

        Assertions.assertTrue(commonSteps.validateDateIsBeforeNow(storeDetailsWebElements.divModifiedDate()),
                "It was expected to validate modified date is before now");
    }

    @Test
    @DisplayName("Validate message displayed when there no poi's in the list")
    void ff9fa_2793() throws IOException, InterruptedException{
        String fleetName = iFakerManager.getValidFleetName();

        clientCreateFleet.createFleet(fleetName);

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        commonSteps.selectElementInList(fleetName);

        iHelpers.sleepOneSecond();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        clientCreateStoreGroup.createStoreGroup(storeGroupName, iFakerManager.getValidNote());

        Assertions.assertEquals(storeGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");

        commonSteps.selectElementInList(storeGroupName);

        iHelpers.sleepOneSecond();

        String storeName = iFakerManager.getValidStoreName();

        String notes = iFakerManager.getValidNote();

        clientCreateStore.createStore(storeName, notes);

        Assertions.assertEquals(storeName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name at first line");

        commonSteps.clickOnStore();

        Assertions.assertTrue(storeSteps.validateDivNoPoiSAreDefined(),
                "It was expected the div no poi's displayed");
    }

    @Test
    @DisplayName("Validate Store Details Data - Acceptance Service empty table")
    void ff9fa_2786() throws IOException, InterruptedException {
        fleetSteps.clickButtonAddFleet();

        iHelpers.sleepOneSecond();

        String fleetName = iFakerManager.getValidFleetName();

        fleetSteps.insertFieldsCreateFleetModal(fleetName, "");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

       Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName),
               "It was expected to validate fleet present on list");

        commonSteps.selectElementInList(fleetName);

        iHelpers.sleepTwoSeconds();

        fleetSteps.clickButtonAddStoreGroup();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(storeGroupName, iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals(storeGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");

        commonSteps.selectElementInList(storeGroupName);

        storeGroupSteps.clickAddStoreButton();

        String storeName = iFakerManager.getValidStoreName();

        String notes = iFakerManager.getValidNote();

        storeSteps.insertFieldsCreateStoreModal(storeName, notes);

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(storeName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name at first line");

        commonSteps.clickOnStore();

        iHelpers.sleepTwoSeconds();

        Assertions.assertEquals("Merchant Id MCC Name and Location Remove",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextFirstListHeader()),
                "It was expected to validate table header");

        Assertions.assertEquals("No Acceptance services Contracts are defined",
                storeSteps.acceptanceServiceContractsStore()[4], "It was expected to validate table header");
    }

    @Test
    @DisplayName("Validate Store Details Data - Acceptance Service with data")
    void ff9fa_2785_2786_2() throws InterruptedException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList(properties.getFleetWithMultipleSelectors());

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.clickOnStore();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals("Merchant Id MCC Name and Location Remove",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextFirstListHeader()),
                "It was expected to validate table header");
    }

    @Test
    @DisplayName("Access poi list from store details page")
    void ff9fa_3137() throws IOException, InterruptedException{
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList(properties.getFleetWithMultipleSelectors());

        iHelpers.sleepTwoSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList("styytzeq");

        poiSteps.clickFirstPoiInList();

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(poiSteps.validateDivPoiIdInformationDisplayed(),
                "It was expected the div poi information displayed");

        Assertions.assertTrue(poiSteps.getTextDivPoiIdInformation().contains("POI:"),
                "It was expected the poiId");
    }

    @Test
    @DisplayName("Validate Store Details Data - button X on Acceptance Services Contract")
    void ff9fa_3651_3652() throws IOException, InterruptedException {
        String fleetName = iFakerManager.getValidFleetName();

        clientCreateFleet.createFleet(fleetName);

        iHelpers.sleepTenSeconds();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        commonSteps.selectElementInList(fleetName);

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        clientCreateStoreGroup.createStoreGroup(storeGroupName, iFakerManager.getValidNote());

        iHelpers.sleepOneSecond();

        commonSteps.selectElementInList(storeGroupName);

        String storeName = iFakerManager.getValidStoreName();

        String notes = iFakerManager.getValidNote();

        clientCreateStore.createStore(storeName, notes);

        Assertions.assertEquals(storeName, commonSteps.getTextFirstColumnInList(),
                "It was expected store name at first line");

        commonSteps.clickOnStore();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(midSteps.validateAddMidButtonIsEnabled(),
                "It was expected to validate create mid button is enabled");

        Assertions.assertTrue(storeSteps.validateButtonRemoveAcceptanceServiceContractIsNotPresent(),
                "It was expected to validate remove not present");

        Assertions.assertEquals("No Acceptance services Contracts are defined",
                storeSteps.acceptanceServiceContractsStore()[4], "It was expected to validate table header");

        storeSteps.clickAddMidButton();

        String merchantId = iFakerManager.getValidMerchantIdWithAlphanumeric();

        String merchantCategoryCode = iFakerManager.getValidMerchantCategoryCode();

        String merchantNameAndLocation = iFakerManager.getValidMerchantNameAndLocation();

        String acquiredId = iFakerManager.getValidAcquiredId();

        midSteps.insertMidFieldsAndValidateValues(merchantId, merchantCategoryCode, merchantNameAndLocation, acquiredId);

        midSteps.clickButtonCreateMid();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeSteps.validateButtonRemoveAcceptanceServiceContract(),
                "It was expected to validate remove present");

        Assertions.assertNotEquals("No Acceptance services Contracts are defined",
                storeSteps.acceptanceServiceContractsStore()[4], "It was expected to validate table header");

        storeSteps.clickButtonRemoveAcceptanceServiceContract();

        commonSteps.clickConfirmationChangesSpanNo();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(storeSteps.validateButtonRemoveAcceptanceServiceContract(),
                "It was expected to validate remove present");

        Assertions.assertNotEquals("No Acceptance services Contracts are defined",
                storeSteps.acceptanceServiceContractsStore()[4], "It was expected to validate table header");

        storeSteps.clickButtonRemoveAcceptanceServiceContract();

        commonSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeSteps.validateButtonRemoveAcceptanceServiceContractIsNotPresent(),
                "It was expected to validate remove not present");

        Assertions.assertEquals("No Acceptance services Contracts are defined",
                storeSteps.acceptanceServiceContractsStore()[4], "It was expected to validate table header");
    }

    @Test
    @DisplayName("Validate Store Details Data - button X try to remove AS contract store with POI")
    void ff9fa_3654() throws InterruptedException, IOException {
        commonSteps.selectElementInList("BEFleetghbe01");

        iHelpers.sleepTwoSeconds();

        commonSteps.selectElementInList("Maurice Hane");

        iHelpers.sleepTwoSeconds();

        commonSteps.selectElementInList("stlntrpa");

        Assertions.assertTrue(storeSteps.validateButtonRemoveAcceptanceServiceContract(),
                "It was expected to validate remove not present");

        Assertions.assertNotEquals("No Acceptance services Contracts are defined",
                storeSteps.acceptanceServiceContractsStore()[4], "It was expected to validate table header");

        storeSteps.clickButtonRemoveAcceptanceServiceContract();

        commonSteps.clickConfirmationChangesSpanYes();

        Assertions.assertTrue(storeSteps.validateAcceptanceServiceContractRemoveError(),
                "It was expected to validate pop up error");

        commonSteps.clickButtonCloseModalDialogPopUp();
    }
}