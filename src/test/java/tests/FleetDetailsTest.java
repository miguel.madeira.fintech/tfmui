package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ModalCommonsComponent;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import pages.fleetdetails.FleetDetailsWebElements;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.poi.PoiSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;

public class FleetDetailsTest extends NewWindow {

    //Tests from ff9fa_2789 to ff9fa_2790 related to US-2681

    private static FleetSteps fleetSteps;

    private static StoreGroupSteps storeGroupSteps;

    private static CommonSteps commonSteps;

    private static PoiSteps poiSteps;

    private static StoreSteps storeSteps;

    private static FleetDetailsWebElements fleetDetailsWebElements;

    private static IFakerManager iFakerManager;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException, InterruptedException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        Properties properties = iReadProperties.readFromYaml();

        iFakerManager = new FakerManager();

        fleetDetailsWebElements = new FleetDetailsWebElements(driver, properties);

        fleetSteps = new FleetSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        poiSteps = new PoiSteps(driver, properties);

        storeSteps = new StoreSteps(driver,properties);

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepTwoSeconds();
    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        iHelpers.sleepTwoSeconds();

        commonSteps.clickTabFleet();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate search element")
    void ff9fa_2765() throws IOException, InterruptedException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        commonSteps.validateSearchComponent("Search by Store Group, Profile");
    }

    @Test
    @DisplayName("Validate action buttons next the search component")
    void ff9fa_2766() throws IOException, InterruptedException {
        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        storeGroupSteps.createStoreGroupIfNecessary(iFakerManager.getValidStoreGroupName());

        Assertions.assertTrue(commonSteps.validateButtonDisplayedAndEnabledWithExpectedText(
                fleetDetailsWebElements.buttonAddStoreGroup(), "ADD STORE GROUP"),
                "It was expected the button Add store group displayed and enabled");

    }

    @Test
    @DisplayName("Navigation through breadcrumb")
    void FF9FA_3450() throws IOException, InterruptedException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList("BEFleetyiaw15");

        iHelpers.sleepFiveSeconds();

        commonSteps.clickFirstColumnInList();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickOnStore();

        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList("01033082966");

        iHelpers.sleepFiveSeconds();

        Assertions.assertEquals("POI: 01033082966", poiSteps.getTextDivPoiIdInformation(),
                "It was expected the same poi id");

        commonSteps.clickBreadCrumbElement("stfjdxgp");

        iHelpers.sleepFiveSeconds();

        Assertions.assertEquals("Merchant Id MCC Name and Location Remove",
                commonSteps.replaceNewLineForSpace(commonSteps.getTextFirstListHeader()),
                "It was expected to validate table header");

        commonSteps.clickBreadCrumbElement("Bret Bartell");

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeSteps.validateButtonAddStoreDisplayedAndEnabled(),
                "It was expected the button add store  present");

        Assertions.assertEquals("Select all Store Profile Nr of POIs", commonSteps.replaceNewLineForSpace(
                commonSteps.getTextFirstListHeader()),"It was expected to validate header");

        commonSteps.clickBreadCrumbElement("BEFleetyiaw15");

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeSteps.validateButtonAddStoreGroupDisplayedAndEnabled(),
                "It was expected to validate Add store button");

        Assertions.assertEquals("Select all Store Group Profile Nr of Stores Nr of POIs", commonSteps.replaceNewLineForSpace(
                commonSteps.getTextFirstListHeader()),"It was expected to validate header");
    }

    @Test
    @DisplayName("Validate Nr of store groups is updated on fleet details label")
    void NoJiraTest() throws IOException, InterruptedException {
        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        int numberOfStoreGroups = Integer.parseInt(fleetSteps.getTextFromNrOfStoreGroups()[1]);

        iHelpers.sleepOneSecond();

        fleetSteps.clickButtonAddStoreGroup();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        iHelpers.sleepOneSecond();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(storeGroupName, iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        int numberOfStoreGroupsAfterAddingNew = Integer.parseInt(fleetSteps.getTextFromNrOfStoreGroups()[1]);

        Assertions.assertEquals(numberOfStoreGroups + 1,numberOfStoreGroupsAfterAddingNew,
                "It was expected to validate Nr of store groups updated");

        //TODO BUG FF9FA_3657
    }

    @Test
    @DisplayName("Delete Store group successfully")
    void ff9fa_3694_3695_3696_3697_3698() throws IOException, InterruptedException {
        String fleetName = iFakerManager.getValidFleetName();

        iHelpers.sleepTwoSeconds();

        fleetSteps.clickButtonAddFleet();

        fleetSteps.insertFieldsCreateFleetModal(fleetName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        commonSteps.selectElementInList(fleetName);

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(commonSteps.validateButtonDeleteStoreGroupIsDisplayed(),
                "It was expected to validate button delete store group displayed");

        storeGroupSteps.createStoreGroupIfNecessary(iFakerManager.getValidStoreGroupName());

        iHelpers.sleepTwoSeconds();

        commonSteps.clickFirstCheckbox();

        Assertions.assertTrue(commonSteps.validateButtonDeleteStoreGroupIsDisplayedAndEnabled(),
                "It was expected to validate button delete store group displayed and enabled");

        fleetSteps.clickButtonAddStoreGroup();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(iFakerManager.getValidStoreGroupName(),
                iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(fleetSteps.validateNrOfStoreGroups(2),
                "It was expected to validate 2 SGs");

        storeGroupSteps.clickDeleteStoreGroupButton();

        Assertions.assertEquals("Are you sure? Removing store group can not be undone", commonSteps.getTextFromPopUp(),
                "It was expected to validate button delete store group displayed and enabled");

        commonSteps.clickConfirmationChangesSpanYes();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(fleetSteps.validateNrOfStoreGroups(1),
                "It was expected to validate 1 SG");

        fleetSteps.clickButtonAddStoreGroup();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(iFakerManager.getValidStoreGroupName(),
                iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        commonSteps.clickOnDivSelectAll();

        storeGroupSteps.clickDeleteStoreGroupButton();

        Assertions.assertEquals("Are you sure? Removing store groups can not be undone", commonSteps.getTextFromPopUp(),
                "It was expected to validate button delete store group displayed and enabled");

        commonSteps.clickConfirmationChangesSpanNo();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(fleetSteps.validateNrOfStoreGroups(2),
                "It was expected to validate 2 SGs");
    }
}