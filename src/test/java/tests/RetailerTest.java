package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import helpers.IHelpers;
import newwindowdifferentusers.NewWindowRetailer;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.poi.PoiSteps;
import steps.poigroup.PoiGroupSteps;
import steps.poiprofile.PoiProfileSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;

public class RetailerTest extends NewWindowRetailer {

    private static FleetSteps fleetSteps;

    private static StoreGroupSteps storeGroupSteps;

    private static StoreSteps storeSteps;

    private static PoiSteps poiSteps;

    private static PoiGroupSteps poiGroupSteps;

    private static PoiProfileSteps poiProfileSteps;

    private static CommonSteps commonSteps;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        Properties properties = iReadProperties.readFromYaml();

        fleetSteps = new FleetSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        poiSteps = new PoiSteps(driver, properties);

        poiGroupSteps = new PoiGroupSteps(driver, properties);

        poiProfileSteps = new PoiProfileSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        storeSteps = new StoreSteps(driver, properties);
    }

    @BeforeEach
    public void initialTestConditions() throws IOException, InterruptedException {
        iHelpers.sleepFiveSeconds();

        commonSteps.clickTabFleet();

        iHelpers.sleepFiveSeconds();
    }

    @Test
    @DisplayName("Validate permissions for retailer")
    void ff9fa_3178() throws IOException, InterruptedException {
        Assertions.assertTrue(fleetSteps.validateFleetListPresentForRetailer(), "It was expected the fleet list present");

        Assertions.assertTrue(poiSteps.validatePoiTabPresent(), "It was expected the poiTab Present");

        Assertions.assertTrue(poiGroupSteps.validateTabPoiGroupNotPresent(),
                "It was expected the poiGroupTab not Present");

        Assertions.assertTrue(poiProfileSteps.validateTabPoiProfileNotPresent(),
                "It was expected the poiProfileTab not Present");

        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateInputSelectPoiNotPresent(),
                "It was expected any checkbox found");

        commonSteps.selectElementInList();

        Assertions.assertTrue(poiSteps.validatePoiTab(), "It was expected to validate poi tab");

        Assertions.assertTrue(poiSteps.validateMerchantPin(), "It was expected not empty");

        Assertions.assertTrue(poiSteps.validateMerchantPinDefaultPolicy(),
                "It was expected POI ID or Fixed");
    }

    @Test
    @DisplayName("Validate operation actions for retailer")
    void ff9fa_3172() throws InterruptedException, IOException {
        Assertions.assertTrue(fleetSteps.validateFleetListPresentForRetailer(), "It was expected the fleet list present");

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetNotPresent(),
                "It was expected the button add fleet not present");

        Assertions.assertTrue(commonSteps.validateButtonDeleteFleetNotPresent(),
                "It was expected the button remove fleet not present");

        iHelpers.sleepTwoSeconds();

        commonSteps.selectElementInList("BEFleetvazb22");

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(storeGroupSteps.validateButtonAddStoreGroupNotPresent(),
                "It was expected the button add store group not present");

        Assertions.assertTrue(commonSteps.validateButtonDeleteFleetNotPresent(),
                "It was expected the button remove store group not present");

        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(storeSteps.validateButtonAddStoreNotPresent(),
                "It was expected the button add store not present");

        Assertions.assertTrue(commonSteps.validateButtonDeleteFleetNotPresent(),
                "It was expected the button remove store group not present");

        commonSteps.selectElementInList();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateButtonAddPoiNotPresent(),
                "It was expected the button add poi not present");

        Assertions.assertTrue(poiSteps.validateButtonAddMidNotPresent(),
                "It was expected the button add mid not present");

        Assertions.assertTrue(storeSteps.validateButtonRemoveAcceptanceServiceContractIsNotPresent(),
                "It was expected to validate remove not present");

        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateInputSelectPoiNotPresent(),
                "It was expected any checkbox found");

        poiSteps.clickFirstPoiInList();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(poiSteps.validateMerchantPinEditableContentNotPresent(),
                "It was expected the Merchant Pin Editable Content not present");
    }

    @Test
    @DisplayName("Validate retailer does not have permission to create POI label")
    void ff9fa_3361_3587() throws IOException, InterruptedException {
        commonSteps.clickTabPoi();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(poiSteps.validateButtonAddLabelNotPresent(),
                "It was expected the button add label not present");

        Assertions.assertTrue(poiSteps.validateButtonDeletePoiNotPresent(),
                "It was expected the button delete poi not present");

    }
}