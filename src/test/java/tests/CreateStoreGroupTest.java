package tests;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.ListComponent;
import components.ModalCommonsComponent;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.IHelpers;
import newwindow.NewWindow;
import org.junit.jupiter.api.*;
import properties.IReadProperties;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;

public class CreateStoreGroupTest extends NewWindow {

    //Tests from ff9fa_2653 to ff9fa_2660 related to US-2523
    //Tests from ff9fa_2735 to ff9fa_2740 related to US-2522

    private static FleetSteps fleetSteps;

    private static StoreGroupSteps storeGroupSteps;

    private static ListComponent listComponent;

    private static CommonSteps commonSteps;

    private static IFakerManager iFakerManager;

    private static IHelpers iHelpers;

    @BeforeAll
    public static void initialTestClassConditions() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        iHelpers = injector.getInstance(IHelpers.class);

        Properties properties = iReadProperties.readFromYaml();

        iFakerManager = new FakerManager();

        fleetSteps = new FleetSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);
    }

    @BeforeEach
    public void initialTestConditions() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        fleetSteps.clickButtonAddStoreGroup();
    }

    @AfterEach
    public void finalTestConditions() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        commonSteps.closeModalIfNecessary(ModalCommonsComponent.MODAL_DIALOG);

        commonSteps.clickTabFleet();

        Assertions.assertTrue(fleetSteps.validateButtonAddFleetDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate create store group modal component")
    void ff9fa_2653() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(storeGroupSteps.validateCreateStoreGroupModal(), "It was expected true");

        commonSteps.clickButtonCloseModalDialog();

        Assertions.assertTrue(storeGroupSteps.validateButtonAddStoreGroupDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Click button cancel after informing the fields in modal component")
    void ff9fa_2654() throws IOException {
        storeGroupSteps.insertFieldsCreateStoreGroupModal(iFakerManager.getValidStoreGroupName(),
                iFakerManager.getValidNote());

        commonSteps.clickButtonCancelInModal();

        Assertions.assertTrue(storeGroupSteps.validateButtonAddStoreGroupDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate click on 'X' button after informing the fields in modal component")
    void ff9fa_2655() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(iFakerManager.getValidStoreGroupName(),
                iFakerManager.getValidNote());

        commonSteps.clickButtonCloseModalDialog();

        Assertions.assertTrue(storeGroupSteps.validateButtonAddStoreGroupDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Validate trying to create a new fleet with empty store group name")
    void ff9fa_2656() throws IOException{
        storeGroupSteps.insertFieldsCreateStoreGroupModal("",
                iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        Assertions.assertEquals("Invalid input: Required Field", commonSteps.getTextRequiredFieldMessage(),
                "It was expected the same message");

        Assertions.assertTrue(commonSteps.validateRequiredField(), "It was expected displayed and enabled");

        commonSteps.clickButtonCloseModalDialog();

        Assertions.assertTrue(storeGroupSteps.validateButtonAddStoreGroupDisplayedAndEnabled(),
                "It was expected the button addFleet displayed and enabled");
    }

    @Test
    @DisplayName("Trying to create new store group with duplicated store group name")
    void ff9fa_2657() throws IOException, InterruptedException {
        String fleetName = iFakerManager.getValidFleetName();

        commonSteps.clickButtonCloseModalDialog();

        commonSteps.clickTabFleet();

        iHelpers.sleepFiveSeconds();

        fleetSteps.clickButtonAddFleet();

        fleetSteps.insertFieldsCreateFleetModal(fleetName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        String storeGroupName = iFakerManager.getValidSolutionContractName();

        commonSteps.selectElementInList();

        iHelpers.sleepOneSecond();

        storeGroupSteps.addStoreGroupInsertFieldsAndCreate(storeGroupName);

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(storeGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");

        storeGroupSteps.addStoreGroupInsertFieldsAndCreate(storeGroupName);

        Assertions.assertTrue(storeGroupSteps.validateErrorMessageStoreGroupNameAlreadyExists(),
                "It was expected the error message store group name already exists");
    }

    @Test
    @DisplayName("Validate trying to create a new store group with only one character on store group name")
    void ff9fa_2658() throws IOException, InterruptedException {
        String invalidStoreGroupName = iFakerManager.getValidStoreGroupNameWithOneCharacter();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(invalidStoreGroupName, iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        Assertions.assertTrue(storeGroupSteps.validateErrorMessageMinLength(),
                "It was expected the error message store group name already exists");
    }

    @Test
    @DisplayName("Validate create a new store group successfully")
    void ff9fa_2659() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(storeGroupName, iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(storeGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");
    }

    @Test
    @DisplayName("Validate create a new store group successfully with empty notes")
    void ff9fa_2660() throws IOException, InterruptedException {
        String storeGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(storeGroupName, "");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals(storeGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");
    }

    @Test
    @DisplayName("Validate message when no store group in parent fleet")
    void ff9fa_2735() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        String fleetName = iFakerManager.getValidFleetName();

        commonSteps.clickButtonCloseModalDialog();

        commonSteps.clickTabFleet();

        iHelpers.sleepOneSecond();

        fleetSteps.clickButtonAddFleet();

        fleetSteps.insertFieldsCreateFleetModal(fleetName,iFakerManager.getValidNote());

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        Assertions.assertTrue(fleetSteps.validateFleetCreatedInFleetList(fleetName));

        commonSteps.selectElementInList(fleetName);

        Assertions.assertTrue(fleetSteps.validateDisplayedAndEnabledDivNoStoreGroup(),
                "It was expected No Store Group are defined present");
    }

    @Test
    @DisplayName("Validate store group list table")
    void ff9fa_2736() throws IOException, InterruptedException {
        commonSteps.clickButtonCloseModalDialog();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals("Select all Store Group Profile Nr of Stores Nr of POIs", commonSteps.replaceNewLineForSpace(
                commonSteps.getTextFirstListHeader()),"It was expected to validate header");
    }

    @Test
    @DisplayName("Validate sorting functionality")
    void ff9fa_2737() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(storeGroupName, "");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        fleetSteps.clickButtonAddStoreGroup();

        String secondStoreGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(secondStoreGroupName, "");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepFiveSeconds();

        Assertions.assertEquals(secondStoreGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the same store group name");

        Assertions.assertTrue(commonSteps.getListContentSize() > 1,
                "It was expected the list size grater than 1");

        commonSteps.clickDivStoreGroup();

        iHelpers.sleepFiveSeconds();

        Assertions.assertNotEquals(secondStoreGroupName, commonSteps.getTextFirstColumnInList(),
                "It was expected the first column changing on sort");
    }

    @Test
    @DisplayName("Validate clicking store group name in list")
    void ff9fa_2738() throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        String storeGroupName = iFakerManager.getValidStoreGroupName();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(storeGroupName, "");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        commonSteps.clickFirstColumnInList();

        iHelpers.sleepOneSecond();

        Assertions.assertEquals("Select all Store Profile Nr of POIs", commonSteps.replaceNewLineForSpace(
                commonSteps.getTextFirstListHeader()),"It was expected to validate header");
    }
}