package properties;

import appinjector.AppInjector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.inject.Guice;
import com.google.inject.Injector;
import helpers.IHelpers;

import java.io.File;
import java.io.IOException;

public class ReadProperties implements IReadProperties{

    private IHelpers iHelpers;

    public ReadProperties(){
        Injector injector = Guice.createInjector(new AppInjector());

        iHelpers = injector.getInstance(IHelpers.class);
    }

    public Properties readFromYaml() throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        mapper.findAndRegisterModules();

        return mapper.readValue(new File(getPropertiesFullPath()), Properties.class);
    }

    private String getPropertiesFullPath() throws IOException {
        return iHelpers.getCanonicalPath().concat("/Properties.yaml");

    }
}
