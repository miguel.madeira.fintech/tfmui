package properties;

import lombok.Getter;

public class Properties {
    @Getter private String chromePath;
    @Getter private String firefoxPath;
    @Getter private String screenshotsPath;
    @Getter private String cockpitBaseUrl;
    @Getter private int webDriverWaitInSeconds;
    @Getter private String cockpitUsername;
    @Getter private String cockpitPassword;
    @Getter private String terminalFleetManagementUrl;
    @Getter private String screenshotsFolder;
    @Getter private String fleetWithDetailsOnStore;
    @Getter private String fleetWithMultipleSelectors;
    @Getter private String fleetWithActiveMastercardMid;
    @Getter private String poiActivePropertyFleetLevel;
    @Getter private String poiActivePropertyPoiLevel;
    @Getter private String cockpitBaseUrlRetailer;
    @Getter private String cockpitUsernameRetailer;
    @Getter private String cockpitPasswordOtherUsers;
    @Getter private String cockpitBaseUrlMaintainer;
    @Getter private String cockpitUsernameMaintainer;
    @Getter private String cockpitBaseUrlVendor;
    @Getter private String cockpitUsernameVendor;
    @Getter private String fleetForRetailerAndDeployPoi;
    @Getter private String poiForFLeetForRetailerAndDeployPoi;
    @Getter private String fleetForPlatforms;

}