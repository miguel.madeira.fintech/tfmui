package properties;

import java.io.IOException;

public interface IReadProperties {

    Properties readFromYaml() throws IOException;
}
