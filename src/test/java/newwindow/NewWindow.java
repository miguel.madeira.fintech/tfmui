package newwindow;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import driverfactory.DriverManagerFactory;
import driverhelper.DriverHelper;
import drivermanager.DriverManager;
import drivertype.DriverType;
import helpers.IHelpers;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import properties.IReadProperties;
import properties.Properties;
import steps.login.LoginSteps;

import java.io.File;
import java.io.IOException;

public class NewWindow {

    public static WebDriver driver;

    static Properties properties;

    static IHelpers iHelpers;

    static DriverManager driverManager;

    static DriverHelper driverHelper;

    static LoginSteps loginSteps;

    @BeforeAll
    public static void beforeAll() throws IOException, InterruptedException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        properties = iReadProperties.readFromYaml();

        iHelpers = injector.getInstance(IHelpers.class);

        WebDriverManager.chromedriver().setup();

        ChromeOptions opt = new ChromeOptions();

        opt.setHeadless(true);

        driver = new ChromeDriver(opt);

        driverHelper = new DriverHelper(driver, properties);

        driverHelper.navigateToUrl(properties.getCockpitBaseUrl());

        loginSteps = new LoginSteps(driver, properties);

        loginSteps.login(properties.getCockpitUsername(), properties.getCockpitPassword());

        iHelpers.sleepTwoSeconds();

        driverHelper.navigateToTerminalFleetManagement();
    }

    @AfterAll
    public static void afterAll(){
        driverManager.quitDriver();
    }
}