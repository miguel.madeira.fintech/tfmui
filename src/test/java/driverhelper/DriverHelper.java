package driverhelper;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import helpers.IHelpers;
import org.junit.jupiter.api.Assumptions;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import properties.Properties;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;


import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class DriverHelper {

    private IHelpers iHelpers;

    private Properties properties;

    private WebDriver driver;

    private Actions actions;

    private WebDriverWait webDriverWait;

    private Random randomGenerator;

    public DriverHelper(WebDriver driver, Properties properties){
        Injector injector = Guice.createInjector(new AppInjector());

        iHelpers = injector.getInstance(IHelpers.class);

        this.driver = driver;

        this.actions = new Actions(driver);

        this.webDriverWait = new WebDriverWait(driver, properties.getWebDriverWaitInSeconds());

        this.properties = properties;

        this.randomGenerator = new Random();
    }

    private WebDriver getDriver(){
        return driver;
    }

    private Actions getDriverActions(){
        return actions;
    }

    private WebDriverWait getDriverWait(){
        return webDriverWait;
    }

    public void navigateToUrl(String url){
        getDriver().navigate().to(url);
    }

    public boolean invisibilityElementLocated(By locator) {
        return getDriverWait().until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitElementToBePresent(By locator) {
        getDriverWait().until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitElementToBeClickable(By locator) {
        getDriverWait().until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void waitUrlToBe(String url){
        getDriverWait().until(ExpectedConditions.urlToBe(url));
    }

    public void waitTextToBePresent(WebElement element, String text){
        getDriverWait().until(ExpectedConditions.textToBePresentInElement(element, text));
    }

    public void actionClick(WebElement element) {
        getDriverActions().click(element).perform();
    }

    public void actionMoveToElementAndClick(WebElement element) {
        getDriverActions().moveToElement(element).perform();

        element.click();
    }

    public void actionClickAndHold(WebElement element) {
        getDriverActions().clickAndHold(element).build().perform();
    }

    public void actionClickRelease(WebElement element) {
        getDriverActions().clickAndHold(element).release().perform();
    }

    public void actionMoveToElement(WebElement element) {
        getDriverActions().moveToElement(element).perform();
    }

    public void refreshPage(){
        getDriver().navigate().refresh();
    }

    public void openNewTab(){
        ((JavascriptExecutor)getDriver()).executeScript("window.open()");
    }

    public void switchTab(int tab){
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tab));
    }

    public void switchDefaultContent(){
        getDriver().switchTo().defaultContent();
    }

    public void scrollBy() {
        JavascriptExecutor jse = (JavascriptExecutor)getDriver();

        jse.executeScript("window.scrollBy(0,250)");
    }

    public String getDriverCurrentUrl(){
        return getDriver().getCurrentUrl();
    }

    public void deleteAllCookies(){
        getDriver().manage().deleteAllCookies();
    }

    public boolean validateElementDisplayed(WebElement element) {
        return element.isDisplayed();
    }

    public boolean validateElementDisplayedAndDisabled(WebElement element) {
        return element.isDisplayed() && !element.isEnabled();
    }

    public boolean validateElementDisplayedAndEnabled(WebElement element) {
        return element.isDisplayed() && element.isEnabled();
    }

    public WebElement createWebElement(By locator) throws IOException {
        WebElement webElement = null;

        try {
            webElement = driver.findElement(locator);

            webElement.isEnabled();
        }
        catch (Exception e){
            takeScreenShotWhenFailed(driver, locator);
        }
        return webElement;
    }

    public List<WebElement> createListWebElement(By locator){
        return driver.findElements(locator);
    }

    public List<WebElement> createChildListWebElement(WebElement element, By childLocator){
        return element.findElements(childLocator);
    }

    private void takeScreenShotWhenFailed(WebDriver driver, By locator) throws IOException {
        Screenshot screenshot = new AShot().coordsProvider(new WebDriverCoordsProvider())
                .takeScreenshot(driver);

        ImageIO.write(screenshot.getImage(), "PNG", new File(iHelpers.getCanonicalPath()
                .concat(properties.getScreenshotsFolder()).concat(properties.getScreenshotsPath()
                        .concat(driver.getTitle()).concat(".png"))));
    }

    public void navigateToTerminalFleetManagement(){
        driver.navigate().to(properties.getTerminalFleetManagementUrl());
    }

    public void insertText(WebElement element, String text){
        element.sendKeys(text);
    }

    public String webElementGetText(WebElement element){
        return element.getText();
    }

    public void selectRandomOptionInSelect(Select select){
        Assumptions.assumeTrue(select.getOptions().size() > 0, "It was expected a non empty select");

        select.selectByIndex(randomGenerator.nextInt(20));
    }

    public boolean validateDataAttribute(WebElement webElement, String attribute, String attributeValue){
        return webElement.getAttribute(attribute).equals(attributeValue);
    }

    public List<String> getTextFromListWebElements(List<WebElement> list){
        return list.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public boolean validateSelectGetOptionsSize(Select select, int expectedSize){
        return select.getOptions().size() == expectedSize;
    }

    public void selectRandomOptionInSelectWithLimit(Select select){
        select.selectByIndex(randomGenerator.nextInt(select.getOptions().size()));
    }

    public String getWebElementContentByAttribute(WebElement element, String attribute){
        return element.getAttribute(attribute);
    }

    public void sendEscapeKey(WebElement element){
        element.sendKeys(Keys.ESCAPE);
    }

    public void sendEnterKey(WebElement element){
        element.sendKeys(Keys.ENTER);
    }

    public void scrollUp() {
        JavascriptExecutor jse = (JavascriptExecutor)getDriver();

        jse.executeScript("window.scrollBy(0,-1750)");
    }

    public void scrollEndPage() {
        JavascriptExecutor jse = (JavascriptExecutor)getDriver();

        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public void selectOptionByPosition(Select select, int position){
        select.selectByIndex(position);
    }
}