package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class IdentitiesComponent {

    public static final By HEADER_IDENTITIES = By.cssSelector("th[class$='MuiTableCell-stickyHeader']");

    public static final By DIV_ROWS_PER_PAGE = By.cssSelector("div[class$='MuiInputBase-input']");

    public static final By TABLE_ROWS = By.cssSelector("tr[data-automation-id^='identity-row-']");

    public static final By TABLE_USERS = By.cssSelector("table[data-automation-id='identity-table-pagination']");

    public static final By BUTTON_NEXT = By.cssSelector("button[title='Next page']");

    public static final By TABLE_FIRST_ROW = By.cssSelector("tr[data-automation-id='identity-row-0']");

    public static final By TABLE_HEADER_USERNAME = By.cssSelector("th[data-automation-id='identity-header-userName']");

    public static final By DIV_ID = By.cssSelector("div[class$='ioYvfy']");

    public static final By DIV_DETAILS_DATA = By.cssSelector("div[data-automation-id='identity-details-grid']");

    public static final By DIV_EDITABLE_FIELDS = By.cssSelector("div[data-automation-id='editable-detail-text']");

    public static final By DIV_EDITABLE_FIELDS_TEXT_AREA = By.cssSelector("textarea[class*='jss']");

    public static final By USERNAME_SIZE_ERROR_MESSAGE = By.id("alert-dialog-description");

    private DriverHelper driverHelper;

    public IdentitiesComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public List<WebElement> headerIdentities() {
        return driverHelper.createListWebElement(HEADER_IDENTITIES);
    }

    public  WebElement divRowsPerPage() throws IOException {
        return driverHelper.createWebElement(DIV_ROWS_PER_PAGE);
    }

    public List<WebElement> tableRows() {
        return driverHelper.createListWebElement(TABLE_ROWS);
    }

    public WebElement divTable() throws IOException {
        return driverHelper.createWebElement(TABLE_USERS);
    }

    public WebElement buttonNext() throws IOException {
        return driverHelper.createWebElement(BUTTON_NEXT);
    }

    public WebElement tableFirstRow() throws IOException {
        return driverHelper.createWebElement(TABLE_FIRST_ROW);
    }

    public WebElement tableHeaderUsername() throws IOException {
        return driverHelper.createWebElement(TABLE_HEADER_USERNAME);
    }

    public WebElement divId() throws IOException {
        return driverHelper.createWebElement(DIV_ID);
    }

    public WebElement divDetailsData() throws IOException {
        return driverHelper.createWebElement(DIV_DETAILS_DATA);
    }

    public List<WebElement> divEditableFields() {
        return driverHelper.createListWebElement(DIV_EDITABLE_FIELDS);
    }

    public WebElement usernameSizeErrorMessage() throws IOException {
        return driverHelper.createWebElement(USERNAME_SIZE_ERROR_MESSAGE);
    }

    public List<WebElement> divEditableFieldsTextArea() {
        return driverHelper.createListWebElement(DIV_EDITABLE_FIELDS_TEXT_AREA);
    }
}
