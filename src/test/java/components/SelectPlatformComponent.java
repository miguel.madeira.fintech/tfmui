package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class SelectPlatformComponent {

    public static final By SELECT_PLATFORM = By.cssSelector("select[class$='MuiOutlinedInput-input']");

    private DriverHelper driverHelper;

    public SelectPlatformComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    private WebElement selectPlatform() throws IOException {
        return driverHelper.createWebElement(SELECT_PLATFORM);
    }

    public List<WebElement> selectPlatformList() throws IOException {
        return driverHelper.createListWebElement(SELECT_PLATFORM);
    }

    public Select selectPlatformOptions() throws IOException {
        return new Select(selectPlatform());
    }
}