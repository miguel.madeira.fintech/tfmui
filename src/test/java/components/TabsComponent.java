package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class TabsComponent {

    public static final By FLEET_TAB = By.cssSelector("a[href='#/tfm/fleet']");

    public static final By POI_TAB = By.cssSelector("a[href='#/tfm/poi']");

    public static final By IDENTITIES_TAB = By.cssSelector("a[href='#/iam/identities']");

    public static final By POI_PROFILE_TAB = By.cssSelector("a[href='#/tfm/poiProfile']");

    public static final By POI_GROUP_TAB = By.cssSelector("a[href='#/tfm/poiGroup']");

    private DriverHelper driverHelper;

    public TabsComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement fleetTab() throws IOException {
        return driverHelper.createWebElement(FLEET_TAB);
    }

    public WebElement poiTab() throws IOException {
        return driverHelper.createWebElement(POI_TAB);
    }

    public WebElement poiProfileTab() throws IOException {
        return driverHelper.createWebElement(POI_PROFILE_TAB);
    }

    public WebElement poiGroupTab() throws IOException {
        return driverHelper.createWebElement(POI_GROUP_TAB);
    }

    public WebElement identitiesTab() throws IOException {
        return driverHelper.createWebElement(IDENTITIES_TAB);
    }
}