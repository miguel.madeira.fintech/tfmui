package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class ModalCreateFleetComponent {

    public static final By MODAL_CREATE_FLEET_INPUT_FLEET_NAME = By.cssSelector("input[name='name']");

    public static final By MODAL_CREATE_FLEET_ACCEPTANCE_SERVICES_CONTENT = By.cssSelector("div[class='MuiFormGroup-root']");

    private DriverHelper driverHelper;

    public ModalCreateFleetComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement modalCreateFleetInputFleetName() throws IOException {
        return driverHelper.createWebElement(MODAL_CREATE_FLEET_INPUT_FLEET_NAME);
    }

    public WebElement modalCreateFleetAcceptanceServicesContent() throws IOException {
        return driverHelper.createWebElement(MODAL_CREATE_FLEET_ACCEPTANCE_SERVICES_CONTENT);
    }
}