package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class ListComponent {

    public static final By LIST = By.cssSelector("div[data-automation-id$='-scroll']");

    public static final By LIST_CONTENT = By.cssSelector("div[data-automation-id*='-row-']");

    public static final By LIST_HEADER = By.cssSelector("div[data-automation-id$='-list-header']");

    public static final By POI_LIST_HEADER = By.cssSelector("div[data-automation-id='poi-list-header']");

    public static final By LIST_HEADER_COLUMNS = By.cssSelector("div[class='sc-fzqNJr hXQgjp']");

    public static final By COLUMNS = By.cssSelector("div[data-automation-id*='-cell-']");

    public static final By NAME_COLUMN = By.cssSelector("div[data-automation-id$='-cell-1']");

    public static final By DIV_FIRST_POI = By.cssSelector("div[data-automation-id='poi-cell-0']");

    public static final By INPUT_CHECKBOX = By.cssSelector("input[type='checkbox']");

    public static final By SPAN_SELECT_ALL = By.xpath("//span[text()='Select all']");

    private DriverHelper driverHelper;

    public ListComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public List<WebElement> list(){
        return driverHelper.createListWebElement(LIST);
    }

    public List<WebElement> listContent(){
        return driverHelper.createListWebElement(LIST_CONTENT);
    }

    public List<WebElement> listContentChildElements(WebElement element){
        return driverHelper.createChildListWebElement(element, LIST_CONTENT);
    }

    public List<WebElement> listHeader(){
        return driverHelper.createListWebElement(LIST_HEADER);
    }

    public List<WebElement> listHeaderColumns(){
        return driverHelper.createListWebElement(LIST_HEADER_COLUMNS);
    }

    public List<WebElement> columns(){
        return driverHelper.createListWebElement(COLUMNS);
    }

    public WebElement divFirstPoi() throws IOException {
        return driverHelper.createWebElement(DIV_FIRST_POI);
    }

    public List<WebElement> inputCheckboxes(){
        return driverHelper.createListWebElement(INPUT_CHECKBOX);
    }

    public WebElement poiListHeader() throws IOException {
        return driverHelper.createWebElement(POI_LIST_HEADER);
    }

    public WebElement spanSelectAll() throws IOException {
        return driverHelper.createWebElement(SPAN_SELECT_ALL);
    }

    public WebElement nameColumn() throws IOException {
        return driverHelper.createWebElement(NAME_COLUMN);
    }


}