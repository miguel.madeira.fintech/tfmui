package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class SearchComponent {

    public static final By LABEL_SEARCH_STORE_FLEET_DETAILS = By.xpath("//label[contains(text(), 'Search by Store Group, Profile')]");

    public static final By LABEL_SEARCH_STORE_GROUP_DETAILS = By.xpath("//label[contains(text(), 'Search by Store, Profile')]");

    public static final By INPUT_SEARCH = By.id("searchTerm");

    public static final By BUTTON_SEARCH = By.cssSelector("button[data-automation-id$='-search-btn']");

    private DriverHelper driverHelper;

    public SearchComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement labelSearchOnFleetDetails() throws IOException {
        return driverHelper.createWebElement(LABEL_SEARCH_STORE_FLEET_DETAILS);
    }

    public WebElement labelSearchOnStoreGroupDetails() throws IOException {
        return driverHelper.createWebElement(LABEL_SEARCH_STORE_GROUP_DETAILS);
    }

    public WebElement inputSearch() throws IOException {
        return driverHelper.createWebElement(INPUT_SEARCH);
    }

    public WebElement buttonSearch() throws IOException {
        return driverHelper.createWebElement(BUTTON_SEARCH);
    }
}