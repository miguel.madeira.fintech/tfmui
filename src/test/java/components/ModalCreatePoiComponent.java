package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class ModalCreatePoiComponent {

    public static final By ERROR_MESSAGE_NO_SERVICE_CONTRACTS =
            By.xpath("//div[text()='Store must have at least one valid acceptance service contract']");

    public static final By ERROR_MESSAGE_FLEET_STATE_DRAFT =
            By.xpath("//div[text()='In order to deploy the POI on Atheos, fleet state cannot be DRAFT']");

    public static final By ERROR_MESSAGE_NO_ACTIVE_POI_PROFILE =
            By.xpath("//div[contains(text(), 'Unable to retrieve an active poi profile for entity with id')]");

    private DriverHelper driverHelper;

    public ModalCreatePoiComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement errorMessageNoServiceContracts() throws IOException {
        return driverHelper.createWebElement(ERROR_MESSAGE_NO_SERVICE_CONTRACTS);
    }

    public WebElement errorMessageFleetStateDraft() throws IOException {
        return driverHelper.createWebElement(ERROR_MESSAGE_FLEET_STATE_DRAFT);
    }

    public WebElement errorMessageNoActivePoiProfile() throws IOException {
        return driverHelper.createWebElement(ERROR_MESSAGE_NO_ACTIVE_POI_PROFILE);
    }
}