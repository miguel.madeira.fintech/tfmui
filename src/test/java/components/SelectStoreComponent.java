package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class SelectStoreComponent {

    private static final By SELECT_STORE = By.cssSelector("select[class$='Mui-disabled']");

    private DriverHelper driverHelper;

    public SelectStoreComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    private List<WebElement> selectStore() throws IOException {
        return driverHelper.createListWebElement(SELECT_STORE);
    }

    public Select selectStoreOptions() throws IOException {
        return new Select(selectStore().get(1));
    }
}