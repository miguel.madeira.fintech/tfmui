package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class PoiFilterComponent {

    public static final By FILTER_FLEET = By.cssSelector("div[data-automation-id='fleet-multi-select']");

    public static final By FILTER_STORE_GROUP = By.cssSelector("div[data-automation-id='store-group-multi-select']");

    public static final By FILTER_STORE = By.cssSelector("div[data-automation-id='store-multi-select']");

    public static final By FILTER_PLATFORM = By.cssSelector("div[data-automation-id='platform-multi-select']");

    public static final By FILTER_CONFIGURATION_STATUS =
            By.cssSelector("div[data-automation-id='config-status-multi-select']");

    public static final By FILTER_LIST_BOX = By.cssSelector("ul[role='listbox']");

    public static final By FILTER_LIST_BOX_LI = By.tagName("li");

    public static final By DIV_PLATFORM = By.cssSelector("div[data-automation-id='img-undefined-text']");

    public static final By IMG_PAX_D190 =
            By.cssSelector("img[src='https://staging-payment.dlns.io/ui/poistatic/media/PAXD190.9b4649bf.png']");

    public static final By IMG_PAX_S920 =
            By.cssSelector("img[src='https://staging-payment.dlns.io/ui/poistatic/media/PAXS920.e5ad0348.jpg']");

    public static final By BUTTON_ADD_LABEL = By.cssSelector("button[data-automation-id='create-poi-label']");

    public static final By DIV_FIRST_POI_IN_LIST = By.cssSelector("div[data-automation-id='poi-row-0']");

    public static final By DIV_SECOND_POI_IN_LIST = By.cssSelector("div[data-automation-id='poi-row-1']");

    public static final By INPUT_POI_LABEL = By.cssSelector("input[class$='MuiOutlinedInput-input']");

    public static final By DIV_LABEL = By.cssSelector("div[data-automation-id='Editable-lables']");

    public static final By BUTTON_LABEL_SAVE = By.cssSelector("button[class$='jMkHfA']");

    public static final By BUTTON_LABEL_CANCEL = By.cssSelector("button[class$='hjUgRs']");

    public static final By PROPERTIES_LABEL = By.id("tab-context");

    public static final By DIV_LABEL_CLASS = By.cssSelector("div[class$='fnbSrb']");

    private DriverHelper driverHelper;

    public PoiFilterComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement filterFleet() throws IOException {
        return driverHelper.createWebElement(FILTER_FLEET);
    }

    public WebElement filterStoreGroup() throws IOException {
        return driverHelper.createWebElement(FILTER_STORE_GROUP);
    }

    public WebElement filterStore() throws IOException {
        return driverHelper.createWebElement(FILTER_STORE);
    }

    public WebElement filterPlatform() throws IOException {
        return driverHelper.createWebElement(FILTER_PLATFORM);
    }

    public WebElement filterConfigurationStatus() throws IOException {
        return driverHelper.createWebElement(FILTER_CONFIGURATION_STATUS);
    }

    public List<WebElement> filterListBoxItems() throws IOException {
        return driverHelper.createWebElement(FILTER_LIST_BOX).findElements(FILTER_LIST_BOX_LI);
    }

    public WebElement divPlatform() throws IOException {
        return driverHelper.createWebElement(DIV_PLATFORM);
    }

    public WebElement imgPaxD190() throws IOException {
        return driverHelper.createWebElement(IMG_PAX_D190);
    }

    public WebElement imgPaxS920() throws IOException {
        return driverHelper.createWebElement(IMG_PAX_S920);
    }

    public WebElement buttonAddLabel() throws IOException {
        return driverHelper.createWebElement(BUTTON_ADD_LABEL);
    }

    public WebElement divFirstPoiInList() throws IOException {
        return driverHelper.createWebElement(DIV_FIRST_POI_IN_LIST);
    }

    public WebElement divSecondPoiInList() throws IOException {
        return driverHelper.createWebElement(DIV_SECOND_POI_IN_LIST);
    }

    public WebElement inputPoiLabel() throws IOException {
        return driverHelper.createWebElement(INPUT_POI_LABEL);
    }

    public WebElement divLabel() throws IOException {
        return driverHelper.createWebElement(DIV_LABEL);
    }

    public List<WebElement> divLabelList() {
        return driverHelper.createListWebElement(DIV_LABEL);
    }

    public WebElement buttonLabelSave() throws IOException {
        return driverHelper.createWebElement(BUTTON_LABEL_SAVE);
    }

    public WebElement buttonLabelCancel() throws IOException {
        return driverHelper.createWebElement(BUTTON_LABEL_CANCEL);
    }

    public WebElement propertiesLabel() throws IOException {
        return driverHelper.createWebElement(PROPERTIES_LABEL);
    }

    public WebElement divLabelClass() throws IOException {
        return driverHelper.createWebElement(DIV_LABEL_CLASS);
    }
}