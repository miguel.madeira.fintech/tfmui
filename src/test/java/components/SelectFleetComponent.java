package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class SelectFleetComponent {

    private static final By SELECT_FLEET = By.cssSelector("select[class$='Mui-disabled']");

    private DriverHelper driverHelper;

    public SelectFleetComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    private List<WebElement> selectFleet() throws IOException {
        return driverHelper.createListWebElement(SELECT_FLEET);
    }

    public Select selectFleetOptions() throws IOException {
        return new Select(selectFleet().get(0));
    }
}