package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import properties.Properties;

import java.io.IOException;

public class SelectRetailerComponent {

    private static final By SELECT_RETAILER = By.cssSelector("select[class$='MuiOutlinedInput-input']");

    private DriverHelper driverHelper;

    public SelectRetailerComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    private WebElement selectRetailer() throws IOException {
        return driverHelper.createWebElement(SELECT_RETAILER);
    }

    public Select selectRetailerOptions() throws IOException {
        return new Select(selectRetailer());
    }
}