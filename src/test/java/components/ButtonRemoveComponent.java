package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class ButtonRemoveComponent {

    public static final By BUTTON_DELETE_FLEETS = By.xpath("//span[text()='Delete Fleets']");

    public static final By BUTTON_DELETE_STORE = By.xpath("//span[text()='DELETE STORE']");

    public static final By BUTTON_DELETE_STORE_GROUP = By.xpath("//span[text()='DELETE STORE GROUP']");

    public static final By BUTTON_REMOVE = By.xpath("//span[text()='REMOVE']");

    private DriverHelper driverHelper;

    public ButtonRemoveComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement buttonDeleteFleets() throws IOException {
        return driverHelper.createWebElement(BUTTON_DELETE_FLEETS);
    }

    public WebElement buttonDeleteStore() throws IOException {
        return driverHelper.createWebElement(BUTTON_DELETE_STORE);
    }

    public WebElement buttonDeleteStoreGroup() throws IOException {
        return driverHelper.createWebElement(BUTTON_DELETE_STORE_GROUP);
    }

    public  WebElement buttonRemove() throws IOException {
        return driverHelper.createWebElement(BUTTON_REMOVE);
    }
}