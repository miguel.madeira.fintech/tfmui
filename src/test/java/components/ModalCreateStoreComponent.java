package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class ModalCreateStoreComponent {

    public static final By MODAL_CREATE_STORE_INPUT_STORE_NAME = By.cssSelector("input[name='name']");

    public static final By INVALID_INPUT_STORE_ALREADY_EXISTS_MESSAGE =
            By.xpath("//p[text()='Invalid input: This store already exists']");

    public static final By INVALID_INPUT_FIELD_MUST_BE_ALPHANUMERIC =
            By.xpath("//p[text()='Invalid input: Field must be alphanumeric']");

    public static final By INVALID_INPUT_MAX_LENGTH = By.xpath("//p[text()='Invalid input: Max length is 14 chars']");

    private DriverHelper driverHelper;

    public ModalCreateStoreComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement modalCreateStoreInputStoreName() throws IOException {
        return driverHelper.createWebElement(MODAL_CREATE_STORE_INPUT_STORE_NAME);
    }

    public WebElement invalidInputStoreAlreadyExistsMessage() throws IOException {
        return driverHelper.createWebElement(INVALID_INPUT_STORE_ALREADY_EXISTS_MESSAGE);
    }

    public WebElement invalidInputFieldMustBeAlphanumericMessage() throws IOException {
        return driverHelper.createWebElement(INVALID_INPUT_FIELD_MUST_BE_ALPHANUMERIC);
    }

    public WebElement invalidInputMaxLengthMessage() throws IOException {
        return driverHelper.createWebElement(INVALID_INPUT_MAX_LENGTH);
    }
}