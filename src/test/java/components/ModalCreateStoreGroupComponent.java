package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class ModalCreateStoreGroupComponent {

    public static final By MODAL_CREATE_STORE_GROUP_INPUT_STORE_GROUP_NAME = By.cssSelector("input[name='name']");

    public static final By MODAL_CREATE_STORE_GROUP_INPUT_NOTES = By.cssSelector("textarea[name='notes']");

    public static final By MODAL_FEEDBACK_ERROR_MESSAGE_STORE_GROUP_ALREADY_EXISTS =
            By.xpath("//p[contains(text(), 'Invalid input: This store group already exists')]");

    public static final By MODAL_FEEDBACK_ERROR_MESSAGE_MIN_LENGTH =
            By.xpath("//p[contains(text(), 'Invalid input: Min length is 2 chars')]");

    private DriverHelper driverHelper;

    public ModalCreateStoreGroupComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement modalCreateStoreGroupInputStoreGroupName() throws IOException {
        return driverHelper.createWebElement(MODAL_CREATE_STORE_GROUP_INPUT_STORE_GROUP_NAME);
    }

    public WebElement modalCreateStoreGroupInputNotes() throws IOException {
        return driverHelper.createWebElement(MODAL_CREATE_STORE_GROUP_INPUT_NOTES);
    }

    public WebElement modalFeedbackErrorMessageStoreGroupAlreadyExists() throws IOException {
        return driverHelper.createWebElement(MODAL_FEEDBACK_ERROR_MESSAGE_STORE_GROUP_ALREADY_EXISTS);
    }

    public WebElement modalFeedbackErrorMessageMinLength() throws IOException {
        return driverHelper.createWebElement(MODAL_FEEDBACK_ERROR_MESSAGE_MIN_LENGTH);
    }
}