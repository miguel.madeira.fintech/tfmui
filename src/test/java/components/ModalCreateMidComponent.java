package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class ModalCreateMidComponent {

    public static final By DIV_ACCEPTANCE_SERVICES_CONTENT = By.cssSelector("div[role='radiogroup']");

    public static final By LABEL_ACCEPTANCE_SERVICES = By.xpath("//label[text()='Acceptance Services']");

    public static final By INPUT_RADIO_VISA = By.cssSelector("input[value='VISA']");

    public static final By INPUT_RADIO_MASTERCARD = By.cssSelector("input[value='MASTERCARD']");

    public static final By INPUT_RADIO_CB = By.cssSelector("input[value='CB']");

    public static final By BUTTON_CREATE_MID = By.cssSelector("button[type='submit']");

    public static final By INPUT_MERCHANT_ID = By.cssSelector("input[name='merchantId']");

    public static final By INPUT_MERCHANT_CATEGORY_CODE = By.cssSelector("input[name='merchantCategoryCode']");

    public static final By INPUT_MERCHANT_NAME_AND_LOCATION = By.cssSelector("input[name='merchantNameAndLocation']");

    public static final By INPUT_ACQUIRED_ID = By.cssSelector("input[name='acquirerId']");

    public static final By ERROR_MESSAGE_ONLY_DIGITS_ARE_ALLOWED =
            By.xpath("//p[text()='Invalid input: Only digits are allowed']");

    private DriverHelper driverHelper;

    public ModalCreateMidComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement divAcceptanceServicesContent() throws IOException {
        return driverHelper.createWebElement(DIV_ACCEPTANCE_SERVICES_CONTENT);
    }

    public WebElement labelAcceptanceServices() throws IOException {
        return driverHelper.createWebElement(LABEL_ACCEPTANCE_SERVICES);
    }

    public WebElement inputRadioVisa() throws IOException {
        return driverHelper.createWebElement(INPUT_RADIO_VISA);
    }

    public WebElement inputRadioMastercard() throws IOException {
        return driverHelper.createWebElement(INPUT_RADIO_MASTERCARD);
    }

    public WebElement inputRadioCb() throws IOException {
        return driverHelper.createWebElement(INPUT_RADIO_CB);
    }

    public WebElement buttonCreateMid() throws IOException {
        return driverHelper.createWebElement(BUTTON_CREATE_MID);
    }

    public WebElement inputMerchantId() throws IOException {
        return driverHelper.createWebElement(INPUT_MERCHANT_ID);
    }

    public WebElement inputMerchantCategoryCode() throws IOException {
        return driverHelper.createWebElement(INPUT_MERCHANT_CATEGORY_CODE);
    }

    public WebElement inputMerchantNameAndLocation() throws IOException {
        return driverHelper.createWebElement(INPUT_MERCHANT_NAME_AND_LOCATION);
    }

    public WebElement inputAcquiredId() throws IOException {
        return driverHelper.createWebElement(INPUT_ACQUIRED_ID);
    }

    public WebElement errorMessageOnlyDigitsAreAllowed() throws IOException {
        return driverHelper.createWebElement(ERROR_MESSAGE_ONLY_DIGITS_ARE_ALLOWED);
    }
}