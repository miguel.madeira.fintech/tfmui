package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class ModalCreatePoiGroup {

    public static final By INPUT_POI_GROUP_NAME = By.cssSelector("input[name='name']");

    private DriverHelper driverHelper;

    public ModalCreatePoiGroup(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement inputPoiGroupName() throws IOException {
        return driverHelper.createWebElement(INPUT_POI_GROUP_NAME);
    }
}