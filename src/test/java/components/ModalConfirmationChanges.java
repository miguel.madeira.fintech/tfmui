package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class ModalConfirmationChanges {

    public static final By CONFIRMATION_CHANGES_TITLE = By.xpath("//p[text()='Save Changes, are you sure?']");

    public static final By CONFIRMATION_CHANGES_SPAN_YES = By.xpath("//span[text()='YES']");

    public static final By CONFIRMATION_CHANGES_SPAN_NO = By.xpath("//span[text()='NO']");

    public static final By CONFIRMATION_CHANGES_SPAN_SAVE = By.cssSelector("button[data-automation-id*='save']");

    public static final By CONFIRMATION_CHANGES_SPAN_CANCEL =  By.cssSelector("button[data-automation-id*='cancel']");

    public static final By CONFIRMATION_CHANGES_BUTTON_CLOSE =
            By.cssSelector("button[data-automation-id$='popup-close']");

    public static final By CONFIRMATION_CHANGES_PROPERTY_VALUE_MUST_NOT_BE_BLANK =
            By.xpath("//p[text()='propertyValue must not be blank']");

    public static final By REMOVE_ACCEPTANCE_SERVICE_CONTRACT_ERROR_MESSAGE =
            By.xpath("//p[text()='Unable to remove acceptance service contract, please remove all associated POIs before']");

    private DriverHelper driverHelper;

    public ModalConfirmationChanges(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement confirmationChangesTitle() throws IOException {
        return driverHelper.createWebElement(CONFIRMATION_CHANGES_TITLE);
    }

    public WebElement confirmationChangesSpanYes() throws IOException {
        return driverHelper.createWebElement(CONFIRMATION_CHANGES_SPAN_YES);
    }

    public WebElement confirmationChangesSpanNo() throws IOException {
        return driverHelper.createWebElement(CONFIRMATION_CHANGES_SPAN_NO);
    }

    public WebElement confirmationChangesSpanSave() throws IOException {
        return driverHelper.createWebElement(CONFIRMATION_CHANGES_SPAN_SAVE);
    }

    public WebElement confirmationChangesSpanCancel() throws IOException {
        return driverHelper.createWebElement(CONFIRMATION_CHANGES_SPAN_CANCEL);
    }

    public WebElement confirmationChangesButtonClose() throws IOException {
        return driverHelper.createWebElement(CONFIRMATION_CHANGES_BUTTON_CLOSE);
    }

    public WebElement confirmationChangesPropertyValueMustNotBeBlank() throws IOException {
        return driverHelper.createWebElement(CONFIRMATION_CHANGES_PROPERTY_VALUE_MUST_NOT_BE_BLANK);
    }

    public WebElement removeAcceptanceServiceContractErrorMessage() throws IOException {
        return driverHelper.createWebElement(REMOVE_ACCEPTANCE_SERVICE_CONTRACT_ERROR_MESSAGE);
    }
}