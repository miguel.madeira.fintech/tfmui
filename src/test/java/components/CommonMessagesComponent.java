package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;

public class CommonMessagesComponent {

    public static final By REQUIRED_FIELD_MESSAGE = By.xpath("//p[text()='Invalid input: Required Field']");

    public static final By REQUIRED_FIELD = By.cssSelector("input[aria-invalid='true']");

    public static final By REQUIRED_FIELD_LABEL = By.xpath("//label[contains(@class, 'Mui-error')]");

    public static final By MODAL_FEEDBACK_ERROR_MESSAGE_MIN_LENGTH3 =
            By.xpath("//p[contains(text(), 'Invalid input: Min length is 3 chars')]");

    private DriverHelper driverHelper;

    public CommonMessagesComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement requiredField() throws IOException {
        return driverHelper.createWebElement(REQUIRED_FIELD);
    }

    public WebElement requiredFieldMessage() throws IOException {
        return driverHelper.createWebElement(REQUIRED_FIELD_MESSAGE);
    }

    public List<WebElement> requiredFieldMessageList(){
        return driverHelper.createListWebElement(REQUIRED_FIELD_MESSAGE);
    }

    public WebElement requiredFieldLabel() throws IOException {
        return driverHelper.createWebElement(REQUIRED_FIELD_LABEL);
    }

    public WebElement modalFeedbackErrorMessageMinLength3() throws IOException {
        return driverHelper.createWebElement(MODAL_FEEDBACK_ERROR_MESSAGE_MIN_LENGTH3);
    }
}