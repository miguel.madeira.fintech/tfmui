package components;

import driverhelper.DriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;

public class ModalCommonsComponent {

    public static final By MODAL_DIALOG = By.cssSelector("div[role='dialog']");

    public static final By MODAL_DIALOG_LABEL = By.id("alert-dialog-title");

    public static final By DIV_POP_UP_TEXT = By.id("alert-dialog-description");

    public static final By MODAL_DIALOG_INPUT_NOTES = By.cssSelector("textarea[name='notes']");

    public static final By MODAL_DIALOG_BUTTON_CREATE =
            By.cssSelector("button[data-automation-id='undefined-modal-ok']");

    public static final By MODAL_DIALOG_BUTTON_CANCEL =
            By.cssSelector("button[data-automation-id='undefined-modal-cancel']");

    public static final By MODAL_DIALOG_BUTTON_CLOSE =
            By.cssSelector("button[data-automation-id='undefined-modal-close']");

    public static final By MODAL_DIALOG_BUTTON_CLOSE_POI_GROUP =
            By.cssSelector("button[data-automation-id='createPoiGroupDialog-modal-close']");

    public static final By MODAL_DIALOG_BUTTON_CLOSE_ACCEPTANCE_SERVICE_ERROR =
            By.cssSelector("button[data-automation-id='PopUp-popup-close']");


    private DriverHelper driverHelper;

    public ModalCommonsComponent(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);
    }

    public WebElement modalDialogLabel() throws IOException {
        return driverHelper.createWebElement(MODAL_DIALOG_LABEL);
    }

    public WebElement modalDialogInputNotes() throws IOException {
        return driverHelper.createWebElement(MODAL_DIALOG_INPUT_NOTES);
    }

    public WebElement modalDialogButtonCreate() throws IOException {
        return driverHelper.createWebElement(MODAL_DIALOG_BUTTON_CREATE);
    }

    public WebElement modalDialogButtonCancel() throws IOException {
        return driverHelper.createWebElement(MODAL_DIALOG_BUTTON_CANCEL);
    }

    public WebElement modalDialogButtonClose() throws IOException {
        return driverHelper.createWebElement(MODAL_DIALOG_BUTTON_CLOSE);
    }

    public WebElement modalDialogButtonClosePoiGroup() throws IOException {
        return driverHelper.createWebElement(MODAL_DIALOG_BUTTON_CLOSE_POI_GROUP);
    }

    public WebElement modalDialogButtonCloseAcceptanceServiceError() throws IOException {
        return driverHelper.createWebElement(MODAL_DIALOG_BUTTON_CLOSE_ACCEPTANCE_SERVICE_ERROR);
    }

    public WebElement divPopUpText() throws IOException {
        return driverHelper.createWebElement(DIV_POP_UP_TEXT);
    }
}