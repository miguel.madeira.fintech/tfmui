package helpers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Helpers implements IHelpers{

    public String getCanonicalPath() throws IOException {
        return new File(".").getCanonicalPath();
    }

    public void sleepOneSecond() throws InterruptedException {
        Thread.sleep(2000);
    }

    public void sleepTwoSeconds() throws InterruptedException {
        Thread.sleep(2000);
    }

    public void sleepFiveSeconds() throws InterruptedException {
        Thread.sleep(5000);
    }

    public void sleepTenSeconds() throws InterruptedException {
        Thread.sleep(10000);
    }

    public void deleteFile(File directoryPath){
        for(File file: Objects.requireNonNull(directoryPath.listFiles()))
            if (!file.isDirectory())
                file.delete();
    }

    public List<String> networkRequests(WebDriver driver){
        String scriptToExecute = "var performance = window.performance || window.mozPerformance || window.msPerformance" +
                " || window.webkitPerformance || {}; var network = performance.getEntries() || {}; return network;";

        return Arrays.asList(((JavascriptExecutor)driver).executeScript(scriptToExecute).toString().split("name")); //sapce before name
    }

    public String generateRandomMerchantPin(){
        Random random = new Random();

        return String.format("%04d", random.nextInt(10000));
    }

    public List<String> networkRequestsComplete(WebDriver driver) {
        List<LogEntry> entries = driver.manage().logs().get(LogType.PERFORMANCE).getAll();

        List<String> lst = new ArrayList<>();

        for (int i=0; i < entries.size(); i++) {
            lst.add(entries.get(i).getMessage());
        }

        return lst;
    }

    public List<String> networkRequestError(WebDriver driver) {
        List<LogEntry> les = driver.manage().logs().get(LogType.BROWSER).getAll();

        List<String> lst = new ArrayList<>();

        for (int i=0; i < les.size(); i++) {
            lst.add(les.get(i).getMessage());
        }

        return lst;
    }
}