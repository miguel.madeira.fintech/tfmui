package helpers;

import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.List;

public interface IHelpers {

    String getCanonicalPath() throws IOException;

    void sleepOneSecond() throws InterruptedException;

    void sleepTwoSeconds() throws InterruptedException;

    void sleepFiveSeconds() throws InterruptedException;

    void sleepTenSeconds() throws InterruptedException;

    List<String> networkRequests(WebDriver driver);

    String generateRandomMerchantPin();

    List<String> networkRequestsComplete(WebDriver driver);

    List<String> networkRequestError(WebDriver driver);

}