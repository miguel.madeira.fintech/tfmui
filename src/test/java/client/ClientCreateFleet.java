package client;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import helpers.IHelpers;
import org.openqa.selenium.WebDriver;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;

import java.io.IOException;

public class ClientCreateFleet {

    private IHelpers iHelpers;

    private FleetSteps fleetSteps;

    private CommonSteps commonSteps;

    public ClientCreateFleet(WebDriver driver, Properties properties){
        Injector injector = Guice.createInjector(new AppInjector());

        iHelpers = injector.getInstance(IHelpers.class);

        fleetSteps = new FleetSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);
    }

    public String createFleet(String fleetName) throws IOException, InterruptedException {
        fleetSteps.clickButtonAddFleet();

        iHelpers.sleepFiveSeconds();

        String solutionContractName = fleetSteps.insertFieldsCreateFleetModal(fleetName, "");

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();

        return solutionContractName;
    }
}
