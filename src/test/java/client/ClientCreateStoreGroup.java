package client;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import helpers.IHelpers;
import org.openqa.selenium.WebDriver;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;

public class ClientCreateStoreGroup {

    private IHelpers iHelpers;

    private FleetSteps fleetSteps;

    private StoreGroupSteps storeGroupSteps;

    private CommonSteps commonSteps;

    public ClientCreateStoreGroup(WebDriver driver, Properties properties){
        Injector injector = Guice.createInjector(new AppInjector());

        iHelpers = injector.getInstance(IHelpers.class);

        fleetSteps = new FleetSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);
    }

    public void createStoreGroup(String StoreGroupName, String notes) throws IOException, InterruptedException {
        iHelpers.sleepFiveSeconds();

        fleetSteps.clickButtonAddStoreGroup();

        storeGroupSteps.insertFieldsCreateStoreGroupModal(StoreGroupName, notes);

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepTwoSeconds();
    }
}
