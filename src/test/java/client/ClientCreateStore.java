package client;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import helpers.IHelpers;
import org.openqa.selenium.WebDriver;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;
import steps.store.StoreSteps;
import steps.storegroup.StoreGroupSteps;

import java.io.IOException;

public class ClientCreateStore {

    private IHelpers iHelpers;

    private StoreSteps storeSteps;

    private StoreGroupSteps storeGroupSteps;

    private CommonSteps commonSteps;

    public ClientCreateStore(WebDriver driver, Properties properties){
        Injector injector = Guice.createInjector(new AppInjector());

        iHelpers = injector.getInstance(IHelpers.class);

        storeSteps = new StoreSteps(driver, properties);

        storeGroupSteps = new StoreGroupSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);
    }

    public void createStore(String storeName, String notes) throws IOException, InterruptedException {
        iHelpers.sleepTwoSeconds();

        storeGroupSteps.clickAddStoreButton();

        storeSteps.insertFieldsCreateStoreModal(storeName, notes);

        commonSteps.clickButtonCreateInModal();

        iHelpers.sleepOneSecond();

        iHelpers.sleepTwoSeconds();
    }
}
