package driverfactory;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import drivermanager.ChromeDriverManager;
import drivermanager.DriverManager;
import drivermanager.FirefoxDriverManager;
import drivertype.DriverType;
import helpers.IHelpers;
import properties.IReadProperties;
import properties.Properties;

import java.io.IOException;

public class DriverManagerFactory {

    Properties properties;

    IHelpers iHelpers;

    public DriverManagerFactory() throws IOException {
        Injector injector = Guice.createInjector(new AppInjector());

        IReadProperties iReadProperties = injector.getInstance(IReadProperties.class);

        properties = iReadProperties.readFromYaml();

        iHelpers = injector.getInstance(IHelpers.class);
    }

   // public DriverManager getManager(DriverType type) throws IOException {

    //    DriverManager driverManager;

    //    if (type == DriverType.FIREFOX) {
   //         driverManager = new FirefoxDriverManager(iHelpers.getCanonicalPath().concat(properties.getFirefoxPath()));
   //     } else {
     //       driverManager = new ChromeDriverManager(iHelpers.getCanonicalPath().concat(properties.getChromePath()));
     //   }
     //   return driverManager;
   // }


}