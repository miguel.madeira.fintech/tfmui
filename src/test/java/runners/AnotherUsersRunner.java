package runners;


import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import tests.*;

@RunWith(JUnitPlatform.class)
@SelectClasses({VendorTest.class, RetailerTest.class, MaintainerTest.class})

public class AnotherUsersRunner { }
