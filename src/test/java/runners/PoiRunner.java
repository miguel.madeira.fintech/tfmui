package runners;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import tests.*;

@RunWith(JUnitPlatform.class)
@SelectClasses({PoiTest.class, CreatePoiTest.class, PoiGroupTest.class})
public class PoiRunner { }
