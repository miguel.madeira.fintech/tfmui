package runners;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import tests.IdentitiesAndAccessManagementTest;

@RunWith(JUnitPlatform.class)
@SelectClasses({IdentitiesAndAccessManagementTest.class})

public class IdentitiesAndAccessManagementRunner { }