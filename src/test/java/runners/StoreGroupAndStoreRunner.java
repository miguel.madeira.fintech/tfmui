package runners;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import tests.*;

@RunWith(JUnitPlatform.class)
@SelectClasses({StoreDetailsTest.class, StoreGroupDetailsTest.class, CreateStoreGroupTest.class,
                CreateStoreTest.class})

public class StoreGroupAndStoreRunner { }
