package runners;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import tests.*;

@RunWith(JUnitPlatform.class)
@SelectClasses({VendorTest.class, RetailerTest.class, MaintainerTest.class, CreateFleetTest.class, CreateMidTest.class,
        CreatePoiTest.class, CreateStoreGroupTest.class, CreateStoreTest.class, FleetDetailsTest.class,
        PoiDetailsTest.class, PoiGroupTest.class, PoiTest.class, StoreDetailsTest.class, StoreGroupDetailsTest.class})
public class AllRunner { }
