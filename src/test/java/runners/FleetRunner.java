package runners;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import tests.CreateFleetTest;
import tests.FleetDetailsTest;

@RunWith(JUnitPlatform.class)
@SelectClasses({CreateFleetTest.class, FleetDetailsTest.class})
public class FleetRunner { }
