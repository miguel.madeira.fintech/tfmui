package appinjector;

import com.google.inject.AbstractModule;
import fakermanager.FakerManager;
import fakermanager.IFakerManager;
import helpers.Helpers;
import helpers.IHelpers;
import properties.IReadProperties;
import properties.ReadProperties;

public class AppInjector extends AbstractModule {

    @Override
    protected void configure() {
        bind(IReadProperties.class).to(ReadProperties.class);
        bind(IHelpers.class).to(Helpers.class);
        bind(IFakerManager.class).to(FakerManager.class);
    }
}

