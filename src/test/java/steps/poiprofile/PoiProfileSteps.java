package steps.poiprofile;

import components.*;
import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import properties.Properties;

import java.io.IOException;

public class PoiProfileSteps {

    private TabsComponent tabsComponent;

    private DriverHelper driverHelper;

    public PoiProfileSteps(WebDriver driver, Properties properties){
        tabsComponent = new TabsComponent(driver, properties);

        driverHelper = new DriverHelper(driver, properties);
    }

    public boolean validateTabPoiProfileNotPresent(){
        return driverHelper.invisibilityElementLocated(TabsComponent.POI_PROFILE_TAB);
    }

    public boolean validateTabPoiProfileDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBePresent(TabsComponent.POI_PROFILE_TAB);

        return driverHelper.validateElementDisplayedAndEnabled(tabsComponent.poiProfileTab());
    }
}
