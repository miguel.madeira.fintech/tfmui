package steps.poigroup;

import components.*;
import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.poi.PoiLocators;
import pages.poigroup.PoiGroupLocators;
import pages.poigroup.PoiGroupWebElements;
import properties.Properties;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class PoiGroupSteps {

    private DriverHelper driverHelper;

    private PoiGroupWebElements poiGroupWebElements;

    private ModalCreatePoiGroup modalCreatePoiGroup;

    private ModalCommonsComponent modalCommonsComponent;

    private ListComponent listComponent;

    private CommonMessagesComponent commonMessagesComponent;

    private TabsComponent tabsComponent;

    public PoiGroupSteps(WebDriver driver, Properties properties){
        driverHelper = new DriverHelper(driver, properties);

        poiGroupWebElements = new PoiGroupWebElements(driver, properties);

        modalCreatePoiGroup = new ModalCreatePoiGroup(driver, properties);

        modalCommonsComponent = new ModalCommonsComponent(driver, properties);

        listComponent = new ListComponent(driver, properties);

        commonMessagesComponent = new CommonMessagesComponent(driver, properties);

        tabsComponent = new TabsComponent(driver, properties);
    }

    public boolean validateCreatePoiGroupModal() throws IOException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        return  driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogButtonCreate()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonClosePoiGroup()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonCancel()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonCreate()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCreatePoiGroup.inputPoiGroupName()) &&
                driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogLabel()) &&
                driverHelper.webElementGetText(modalCommonsComponent.modalDialogLabel())
                        .equals("Create Poi Group");
    }

    public boolean validateNoPoiGroupsAreDefinedMessage() throws IOException {
        driverHelper.waitElementToBePresent(PoiGroupLocators.NO_POI_GROUPS_ARE_DEFINED_MESSAGE);

        return driverHelper.validateElementDisplayed(poiGroupWebElements.noPoiGroupsAreDefinedMessage());
    }

    public void clickButtonAddPoi() throws IOException {
        driverHelper.actionClick(poiGroupWebElements.buttonAddPoiGroup());
    }

    public void createPoiGroup(String poiGroupName) throws IOException {
        insertPoiGroupName(poiGroupName);

        driverHelper.actionClick(modalCommonsComponent.modalDialogButtonCreate());
    }

    public void insertPoiGroupName(String poiGroupName) throws IOException {
        driverHelper.waitElementToBeClickable(ModalCreatePoiGroup.INPUT_POI_GROUP_NAME);

        driverHelper.insertText(modalCreatePoiGroup.inputPoiGroupName(), poiGroupName);
    }

    public String getPoiGroupListHeaderText() throws IOException {
        return driverHelper.webElementGetText(poiGroupWebElements.divPoiGroupListHeader());
    }

    public boolean validatePoiGroupCreatedInPoiGroupList(String poiGroupName){
        driverHelper.waitElementToBePresent(ListComponent.LIST);

        return listComponent.listContent().stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("List not found")).getText().startsWith(poiGroupName);
    }

    public boolean validateInputPoiGroupNameIsRequired() throws IOException {
        return driverHelper.validateElementDisplayed(commonMessagesComponent.requiredFieldMessage());
    }

    public boolean validatePoiGroupAlreadyExistsErrorMessage() throws IOException {
        return driverHelper.validateElementDisplayed(poiGroupWebElements.poiGroupAlreadyExistsErrorMessage());
    }

    public boolean validatePoiGroupCreatedInPoiGroupListIsInclusionBased(){
        driverHelper.waitElementToBePresent(ListComponent.LIST);

        return listComponent.listContent().stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("List not found")).getText().contains("Inclusion-based");
    }

    public boolean validateTabPoiGroupNotPresent(){
        return driverHelper.invisibilityElementLocated(TabsComponent.POI_GROUP_TAB);
    }

    public boolean validateTabPoiGroupDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBePresent(TabsComponent.POI_GROUP_TAB);

        return driverHelper.validateElementDisplayedAndEnabled(tabsComponent.poiGroupTab());
    }

    public boolean validateButtonPoiGroupNotPresent(){
        return driverHelper.invisibilityElementLocated(PoiGroupLocators.BUTTON_ADD_POI_GROUP);
    }

    public boolean validateButtonRemoveNotEnabled() throws IOException {
        return driverHelper.validateElementDisplayedAndDisabled(poiGroupWebElements.buttonRemovePoiGroup());
    }

    public boolean validateButtonRemoveDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBePresent(PoiGroupLocators.BUTTON_REMOVE_POI_GROUP);

        return driverHelper.validateElementDisplayedAndEnabled(poiGroupWebElements.buttonRemovePoiGroup());
    }

    public void clickButtonRemovePoiGroup() throws IOException {
        driverHelper.waitElementToBePresent(PoiGroupLocators.BUTTON_REMOVE_POI_GROUP);

        driverHelper.actionClick(poiGroupWebElements.buttonRemovePoiGroup());
    }

    public boolean validateDivPopUpDeletePoiGroup(String text) throws IOException {
        driverHelper.waitElementToBePresent(PoiGroupLocators.DIV_POP_UP_DELETE_POI_GROUP);

        return driverHelper.webElementGetText(poiGroupWebElements.divPopUpDeletePoiGroup())
                .contains(text);
    }

    public List<String> getPoiGroupNames() throws IOException {
        return  driverHelper.getTextFromListWebElements(new ArrayList<>(poiGroupWebElements.divPoiGroupNameList()));
    }

    public void clickSecondCheckbox() {
        driverHelper.waitElementToBePresent(listComponent.INPUT_CHECKBOX);

        listComponent.inputCheckboxes().get(1).click();
    }
}