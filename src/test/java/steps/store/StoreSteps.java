package steps.store;

import components.ListComponent;
import components.ModalCommonsComponent;
import components.ModalConfirmationChanges;
import components.ModalCreateStoreComponent;
import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.fleetdetails.FleetDetailsLocators;
import pages.storedetails.StoreDetailsLocators;
import pages.storedetails.StoreDetailsWebElements;
import pages.storegroupdetails.StoreGroupDetailsLocators;
import pages.storegroupdetails.StoreGroupDetailsWebElements;
import properties.Properties;
import steps.common.CommonSteps;


import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class StoreSteps {

    private StoreGroupDetailsWebElements storeGroupDetailsWebElements;

    private StoreDetailsWebElements storeDetailsWebElements;

    private ModalCommonsComponent modalCommonsComponent;

    private ModalCreateStoreComponent modalCreateStoreComponent;

    private ModalConfirmationChanges modalConfirmationChanges;

    private ListComponent listComponent;

    private CommonSteps commonSteps;

    private DriverHelper driverHelper;

    public StoreSteps(WebDriver driver, Properties properties){
        storeGroupDetailsWebElements = new StoreGroupDetailsWebElements(driver, properties);

        storeDetailsWebElements = new StoreDetailsWebElements(driver, properties);

        modalCreateStoreComponent = new ModalCreateStoreComponent(driver, properties);

        modalCommonsComponent = new ModalCommonsComponent(driver, properties);

        modalConfirmationChanges = new ModalConfirmationChanges(driver,properties);

        listComponent = new ListComponent(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        driverHelper = new DriverHelper(driver, properties);
    }

    public boolean validateButtonAddStoreGroupDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBeClickable(StoreGroupDetailsLocators.BUTTON_ADD_STORE_GROUP);

        return driverHelper.validateElementDisplayedAndEnabled(storeGroupDetailsWebElements.buttonAddStoreGroup());
    }

    public boolean validateCreateStoreModal() throws IOException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        return  driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogButtonCreate()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonCancel()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonClose()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCreateStoreComponent.modalCreateStoreInputStoreName()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogInputNotes()) &&
                driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogLabel()) &&
                driverHelper.webElementGetText(modalCommonsComponent.modalDialogLabel())
                        .equals("Create Store");
    }

    public void insertFieldsCreateStoreModal(String storeName, String notes) throws IOException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        driverHelper.insertText(modalCreateStoreComponent.modalCreateStoreInputStoreName(), storeName);

        driverHelper.insertText(modalCommonsComponent.modalDialogInputNotes(), notes);
    }

    public boolean validateInvalidInputMessagesFieldMustBeAlphanumeric() throws IOException {
        driverHelper.waitElementToBePresent(ModalCreateStoreComponent.INVALID_INPUT_FIELD_MUST_BE_ALPHANUMERIC);

        return driverHelper.validateElementDisplayed(
                modalCreateStoreComponent.invalidInputFieldMustBeAlphanumericMessage());
    }

    public boolean validateInvalidInputMessageStoreNameAlreadyExists() throws IOException {
        driverHelper.waitElementToBePresent(ModalCreateStoreComponent.INVALID_INPUT_STORE_ALREADY_EXISTS_MESSAGE);

        return driverHelper.validateElementDisplayed(modalCreateStoreComponent.invalidInputStoreAlreadyExistsMessage());
    }

    public boolean validateInvalidInputMessageMaxLength() throws IOException {
        driverHelper.waitElementToBePresent(ModalCreateStoreComponent.INVALID_INPUT_MAX_LENGTH);

        return driverHelper.validateElementDisplayed(modalCreateStoreComponent.invalidInputMaxLengthMessage());
    }

    public boolean validateStoreDetails(String solutionContractName, String fleetName, String storeGroupName,
         String storeName, String profileName, String notes, String numberOfPoiS) throws IOException {
        driverHelper.waitElementToBePresent(StoreDetailsLocators.DIV_CREATED_DATE);

        return driverHelper.webElementGetText(storeDetailsWebElements.divStoreName()).contains("Store") &&
                driverHelper.webElementGetText(storeDetailsWebElements.divStoreName()).contains(storeName) &&
                driverHelper.webElementGetText(storeDetailsWebElements.divStoreGroupName()).contains("Store Group") &&
                driverHelper.webElementGetText(storeDetailsWebElements.divStoreGroupName()).contains(storeGroupName) &&
                driverHelper.webElementGetText(storeDetailsWebElements.divFleet()).contains("Fleet") &&
                driverHelper.webElementGetText(storeDetailsWebElements.divFleet()).contains(fleetName) &&
                driverHelper.webElementGetText(storeDetailsWebElements.divPoiProfile()).contains("Profile") &&
                driverHelper.webElementGetText(storeDetailsWebElements.divPoiProfile()).contains(profileName) &&
                driverHelper.webElementGetText(storeDetailsWebElements.divRetailer()).contains("Retailer") &&
                driverHelper.webElementGetText(storeDetailsWebElements.divRetailer()).contains(solutionContractName) &&
                driverHelper.webElementGetText(storeDetailsWebElements.divNumberOfPoiS()).contains("Nr of POIs") &&
                driverHelper.webElementGetText(storeDetailsWebElements.divNumberOfPoiS()).contains(numberOfPoiS) &&
                driverHelper.webElementGetText(storeDetailsWebElements.divNotes()).contains("Notes") &&
                driverHelper.webElementGetText(storeDetailsWebElements.divNotes()).contains(notes) &&
                driverHelper.webElementGetText(storeDetailsWebElements.divCreatedDate()).contains("Created") &&
                driverHelper.webElementGetText(storeDetailsWebElements.divModifiedDate()).contains("Modified");
    }

    public boolean validateDivNoPoiSAreDefined() throws IOException {
        driverHelper.waitElementToBePresent(StoreDetailsLocators.DIV_NO_POI_S_ARE_DEFINED);

        return driverHelper.validateElementDisplayed(storeDetailsWebElements.divNoPoiSAreDefined());
    }

    public void createStoreIfNecessary(String storeName, String notes) throws IOException {
        if(listComponent.columns().size() == 0){
            driverHelper.actionClick(storeGroupDetailsWebElements.buttonAddStore());

            insertFieldsCreateStoreModal(storeName, notes);

            commonSteps.clickButtonCreateInModal();
        }
    }

    public void clickAddPoiButton() throws IOException {
        driverHelper.waitElementToBeClickable(StoreDetailsLocators.BUTTON_ADD_POI);

        driverHelper.actionClick(storeDetailsWebElements.buttonAddPoi());
    }

    public void clickAddMidButton() throws IOException {
        driverHelper.waitElementToBeClickable(StoreDetailsLocators.BUTTON_ADD_MID);

        driverHelper.actionClick(storeDetailsWebElements.buttonAddMid());
    }

    public boolean validateButtonAddPoiDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBeClickable(StoreDetailsLocators.BUTTON_ADD_POI);

        return driverHelper.validateElementDisplayedAndEnabled(storeDetailsWebElements.buttonAddPoi());
    }
    public List<String> acceptanceServiceContractsStoreList() throws IOException { // NOT NEEDED NOW
        return driverHelper.createListWebElement(StoreDetailsLocators.DIV_STORE_ACCEPTANCE_SERVICE_CONTRACTS)
                .stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public String[] acceptanceServiceContractsStore() throws IOException {
        return driverHelper.webElementGetText(storeDetailsWebElements.divStoreAcceptanceServiceContracts())
                .split("\n");
    }

    public boolean validateEditButtonDisplayedAndDisabled() throws IOException {
        return driverHelper.validateElementDisplayedAndDisabled(storeDetailsWebElements.buttonEditStore());
    }

    public boolean validateEditButtonText() throws IOException {
        return commonSteps.validateWebElementText(storeDetailsWebElements.buttonEditStore(), "EDIT STORE");
    }

    public boolean validateButtonAddStoreNotPresent(){
        return driverHelper.invisibilityElementLocated(StoreGroupDetailsLocators.BUTTON_ADD_STORE);
    }

    public boolean validateButtonAddStoreDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBePresent(StoreGroupDetailsLocators.BUTTON_ADD_STORE);

        return driverHelper.validateElementDisplayedAndEnabled(storeGroupDetailsWebElements.buttonAddStore());
    }

    public boolean validateButtonRemoveAcceptanceServiceContract() throws IOException {
        driverHelper.waitElementToBePresent(StoreDetailsLocators.BUTTON_REMOVE_ACCEPTANCE_SERVICE_CONTRACT);

        return driverHelper.validateElementDisplayed(storeDetailsWebElements.buttonRemoveAcceptanceServiceContract());
    }

    public boolean validateButtonRemoveAcceptanceServiceContractIsNotPresent(){
        return driverHelper.invisibilityElementLocated(StoreDetailsLocators.BUTTON_REMOVE_ACCEPTANCE_SERVICE_CONTRACT);
    }

    public void clickButtonRemoveAcceptanceServiceContract() throws IOException {
        driverHelper.waitElementToBeClickable(StoreDetailsLocators.BUTTON_REMOVE_ACCEPTANCE_SERVICE_CONTRACT);

        driverHelper.actionClick(storeDetailsWebElements.buttonRemoveAcceptanceServiceContract());
    }

    public boolean validatePopUpRemoveMid() throws IOException {
        driverHelper.waitElementToBePresent(StoreDetailsLocators.POP_UP_REMOVE_MID);

        return driverHelper.validateElementDisplayed(storeDetailsWebElements.popUpRemoveMid());
    }

    public boolean validateAcceptanceServiceContractRemoveError() throws IOException {
        driverHelper.waitElementToBePresent(modalConfirmationChanges.REMOVE_ACCEPTANCE_SERVICE_CONTRACT_ERROR_MESSAGE);

        return driverHelper.validateElementDisplayed(modalConfirmationChanges.removeAcceptanceServiceContractErrorMessage());
    }
}