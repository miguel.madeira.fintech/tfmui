package steps.poi;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.*;
import driverhelper.DriverHelper;
import helpers.IHelpers;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.poi.PoiLocators;
import pages.poi.PoiWebElements;
import pages.poigroup.PoiGroupLocators;
import pages.poigroup.PoiGroupWebElements;
import pages.storedetails.StoreDetailsLocators;
import pages.storedetails.StoreDetailsWebElements;
import properties.Properties;
import steps.common.CommonSteps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PoiSteps {

    private ModalCommonsComponent modalCommonsComponent;

    private ModalCreatePoiComponent modalCreatePoiComponent;

    private SelectFleetComponent selectFleetComponent;

    private SelectStoreComponent selectStoreComponent;

    private SelectPlatformComponent selectPlatformComponent;

    private ListComponent listComponent;

    private CommonSteps commonSteps;

    private ModalConfirmationChanges modalConfirmationChanges;

    private TabsComponent tabsComponent;

    private PoiWebElements poiWebElements;

    private StoreDetailsWebElements storeDetailsWebElements;

    private PoiGroupWebElements poiGroupWebElements;

    private PoiFilterComponent poiFilterComponent;

    private DriverHelper driverHelper;

    private static  IHelpers iHelpers;


    public PoiSteps(WebDriver driver, Properties properties) {
        modalCommonsComponent = new ModalCommonsComponent(driver, properties);

        modalCreatePoiComponent = new ModalCreatePoiComponent(driver, properties);

        selectFleetComponent = new SelectFleetComponent(driver, properties);

        selectStoreComponent = new SelectStoreComponent(driver, properties);

        selectPlatformComponent = new SelectPlatformComponent(driver, properties);

        modalConfirmationChanges = new ModalConfirmationChanges(driver, properties);

        listComponent = new ListComponent(driver, properties);

        poiWebElements = new PoiWebElements(driver, properties);

        tabsComponent = new TabsComponent(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        storeDetailsWebElements = new StoreDetailsWebElements(driver, properties);

        poiGroupWebElements = new PoiGroupWebElements(driver, properties);

        poiFilterComponent = new PoiFilterComponent(driver, properties);

        driverHelper = new DriverHelper(driver, properties);

        Injector injector = Guice.createInjector(new AppInjector());

        iHelpers = injector.getInstance(IHelpers.class);

    }

    public boolean validateCreatePoiModal() throws IOException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        return driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogButtonCreate()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonCancel()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonClose()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonCreate()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogInputNotes()) &&
                driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogLabel()) &&
                driverHelper.webElementGetText(modalCommonsComponent.modalDialogLabel())
                        .equals("Create Poi") &&
                driverHelper.validateSelectGetOptionsSize(selectFleetComponent.selectFleetOptions(), 1) &&
                driverHelper.validateSelectGetOptionsSize(selectStoreComponent.selectStoreOptions(), 1) &&
                driverHelper.validateElementDisplayedAndEnabled(selectPlatformComponent.selectPlatformOptions()
                        .getWrappedElement());
    }

    public void insertFieldsCreatePoiModal(String notes) throws IOException, InterruptedException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        iHelpers.sleepTwoSeconds();

        driverHelper.selectRandomOptionInSelectWithLimit(selectPlatformComponent.selectPlatformOptions());

        iHelpers.sleepTwoSeconds();

        driverHelper.insertText(modalCommonsComponent.modalDialogInputNotes(), notes);

        driverHelper.actionClick(modalCommonsComponent.modalDialogButtonCreate());
    }

    public boolean validateErrorMessageNoAcceptanceServiceContract() throws IOException {
        driverHelper.waitElementToBePresent(ModalCreatePoiComponent.ERROR_MESSAGE_NO_SERVICE_CONTRACTS);

        return driverHelper.validateElementDisplayed(modalCreatePoiComponent.errorMessageNoServiceContracts());
    }

    public boolean validateErrorMessageFleetStateDraft() throws IOException {
        driverHelper.waitElementToBePresent(ModalCreatePoiComponent.ERROR_MESSAGE_FLEET_STATE_DRAFT);

        return driverHelper.validateElementDisplayed(modalCreatePoiComponent.errorMessageFleetStateDraft());
    }

    public boolean validateErrorMessageNoActivePoiProfile() throws IOException {
        driverHelper.waitElementToBePresent(ModalCreatePoiComponent.ERROR_MESSAGE_NO_ACTIVE_POI_PROFILE);

        return driverHelper.validateElementDisplayed(modalCreatePoiComponent.errorMessageNoActivePoiProfile());
    }

    public boolean validatePoiTab() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_TABS_CONTENT);

        return commonSteps.replaceNewLineForSpace(driverHelper.webElementGetText(poiWebElements.divTabsContent()))
                .equals("Details & properties Parameters Components & Capabilities");
    }

    public boolean validateDetailsAndPropertiesTab() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_COMPONENT_CONTENT);

        return driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Details") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Profile") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Last Contact") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Note") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Store") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Terminal ID") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Created") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Store Group") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Applications") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Modified") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Properties") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Merchant PIN") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Merchant PIN default policy");
    }

    public boolean validateParametersTab() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_COMPONENT_CONTENT);

        return driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Parameters");
    }

    public boolean validateComponentsAndCapabilitiesTab() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_COMPONENT_CONTENT);

        return driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Components") &&
                driverHelper.webElementGetText(poiWebElements.divComponentContent()).contains("Capabilities");
    }

    public void clickButtonParameters() throws IOException {
        driverHelper.waitElementToBeClickable(PoiLocators.BUTTON_PARAMETERS);

        driverHelper.actionClick(poiWebElements.buttonParameters());
    }

    public void clickButtonComponentsAndCapabilities() throws IOException {
        driverHelper.waitElementToBeClickable(PoiLocators.BUTTON_COMPONENTS_AND_CAPABILITIES);

        driverHelper.actionClick(poiWebElements.buttonComponentsAndCapabilities());
    }

    public String getFirstPoiInList() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_FIRST_POI_IN_LIST);

        return driverHelper.webElementGetText(poiWebElements.divFirstPoiInList());
    }

    public boolean validatePoiLabelIsPresent(String poiId) throws IOException {
        return driverHelper.validateElementDisplayed(driverHelper.createWebElement(
                By.xpath("//label[text()='POI: " + poiId + "']")));
    }

    public String getTextPoiLine(String poiId) {
        return listComponent.listContent().stream().filter(x -> x.getText().startsWith(poiId)).findFirst().orElseThrow(
                () -> new NoSuchElementException("Poi Id not found")).getText();
    }

    public boolean validateMerchantPinDefaultPolicy() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_MERCHANT_PIN_DEFAULT_POLICY);

        final String mPinDefaultPolicy = driverHelper.webElementGetText(poiWebElements.divMerchantPinDefaultPolicy());
        return mPinDefaultPolicy.endsWith("POI ID") || mPinDefaultPolicy.endsWith("Fixed");
    }

    public boolean validateMerchantPinDefaultPolicyNotPresent() {
        return driverHelper.invisibilityElementLocated(PoiLocators.DIV_MERCHANT_PIN_DEFAULT_POLICY);
    }

    public boolean validateMerchantPin() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_MERCHANT_PIN);

        return StringUtils.isNotEmpty(driverHelper.webElementGetText(poiWebElements.divMerchantPin()));
    }

    public boolean validateMerchantPinNotPresent() {
        return driverHelper.invisibilityElementLocated(PoiLocators.DIV_MERCHANT_PIN);
    }

    public boolean validateMerchantPinDisplayedAndEnabled() throws IOException {
        return driverHelper.validateElementDisplayedAndEnabled(poiWebElements.divMerchantPin());
    }

    public boolean validateMerchantPinDefaultPolicyWithExpectedValue(String expectedValue) throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_MERCHANT_PIN_DEFAULT_POLICY);

        return driverHelper.webElementGetText(poiWebElements.divMerchantPinDefaultPolicy()).endsWith(expectedValue);
    }

    public boolean validateMerchantPinCountDigits() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_MERCHANT_PIN);

        int count = 0, num = Integer.parseInt(driverHelper.webElementGetText(poiWebElements.divMerchantPin())
                .split("\n")[1]);
        ;

        while (num != 0) {
            num /= 10;
            ++count;
        }

        return count == 4;
    }

    public void clickFirstPoiInList() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_FIRST_POI_IN_LIST);

        driverHelper.actionClick(poiWebElements.divFirstPoiInList());
    }

    public boolean validateDivPoiIdInformationDisplayed() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_POI_ID_INFORMATION);

        return driverHelper.validateElementDisplayed(poiWebElements.divPoiIdInformation());
    }

    public String getTextDivPoiIdInformation() throws IOException {
        return driverHelper.webElementGetText(poiWebElements.divPoiIdInformation());
    }

    public String getTextDivPoiState() throws IOException {
        return driverHelper.webElementGetText(poiWebElements.divPoiState());
    }

    public String getTextDivConfigStatus() throws IOException {
        return driverHelper.webElementGetText(poiWebElements.divPoiConfigStatus());
    }

    public String getTextDivPoiEnable() throws IOException {
        return driverHelper.webElementGetText(poiWebElements.divPoiEnable());
    }

    public String getTextDivPoiDelete() throws IOException {
        return driverHelper.webElementGetText(poiWebElements.divPoiDelete());
    }

    public String getTextPoiDetailsColumns(String row) throws IOException {
        switch (row) {
            case "R1":
                return driverHelper.getWebElementContentByAttribute(poiWebElements.divPoiDetailsR1(), "textContent");
            case "R2":
                return driverHelper.getWebElementContentByAttribute(poiWebElements.divPoiDetailsR2(), "textContent");
            case "R3":
                return driverHelper.getWebElementContentByAttribute(poiWebElements.divPoiDetailsR3(), "textContent");
            case "R4":
                return driverHelper.getWebElementContentByAttribute(poiWebElements.divPoiDetailsR4(), "textContent");
        }
        return "";
    }

    public void clickMerchantPinEditableContent() {
        driverHelper.actionClick(poiWebElements.divMerchantPinEditableContent().get(1));
    }

    public boolean validateDivMerchantPinEditableContentSpanSave() throws IOException {
        driverHelper.waitElementToBeClickable(PoiLocators.DIV_MERCHANT_PIN_EDITABLE_CONTENT_SPAN_SAVE);

        return driverHelper.validateElementDisplayed(poiWebElements.divMerchantPinEditableContentSpanSave());
    }

    public boolean validateDivMerchantPinEditableContentSpanCancel() throws IOException {
        driverHelper.waitElementToBeClickable(PoiLocators.DIV_MERCHANT_PIN_EDITABLE_CONTENT_SPAN_CANCEL);

        return driverHelper.validateElementDisplayed(poiWebElements.divMerchantPinEditableContentSpanCancel());
    }

    public void clearDivMerchantPinEditableContentText() {
        clickMerchantPinEditableContent();

        if (poiWebElements.divsEditableContentText().get(2).isDisplayed()) {
            poiWebElements.divsEditableContentText().get(2).sendKeys(Keys.CONTROL + "a");
            poiWebElements.divsEditableContentText().get(2).sendKeys(Keys.DELETE);
        }
    }

    public String getTextMerchantPinEditableContent() {
        return driverHelper.webElementGetText(poiWebElements.divMerchantPinEditableContent().get(2));
    }

    public void sendEscapeKeyToDivMerchantPinEditableContentText() {
        clickMerchantPinEditableContent();

        driverHelper.sendEscapeKey(poiWebElements.divsEditableContentText().get(2));
    }

    public void sendEnterKeyToDivMerchantPinEditableContentText() {
        driverHelper.sendEnterKey(poiWebElements.divsEditableContentText().get(2));
    }

    public void clickEditableContentSpanCancel() throws IOException {
        driverHelper.actionClick(poiWebElements.divMerchantPinEditableContentSpanCancel());
    }

    public void clickEditableContentSpanSave() throws IOException {
        driverHelper.actionClick(poiWebElements.divMerchantPinEditableContentSpanSave());
    }

    public void clickDivMerchantPinDefaultPolicy() throws IOException {
        driverHelper.actionClick(poiWebElements.divMerchantPinDefaultPolicy());
    }

    public boolean validateModalConfirmationChangesDisplayed() throws IOException {
        driverHelper.waitElementToBePresent(ModalConfirmationChanges.CONFIRMATION_CHANGES_TITLE);

        return driverHelper.validateElementDisplayed(modalConfirmationChanges.confirmationChangesTitle());
    }

    public void clickConfirmationChangesButtonClose() throws IOException {
        driverHelper.actionClick(modalConfirmationChanges.confirmationChangesButtonClose());
    }

    public void clickConfirmationChangesSpanYes() throws IOException {
        driverHelper.actionClick(modalConfirmationChanges.confirmationChangesSpanYes());
    }

    public void clickConfirmationChangesSpanNo() throws IOException {
        driverHelper.actionClick(modalConfirmationChanges.confirmationChangesSpanNo());
    }

    public void insertTextToDivMerchantPinEditableContentText(String textToInsert) {
        driverHelper.insertText(poiWebElements.divsEditableContentText().get(2), textToInsert);
    }

    public boolean validateModalConfirmationChangesPropertyValueMustNotBeBlankDisplayed() throws IOException {
        driverHelper.waitElementToBePresent(
                ModalConfirmationChanges.CONFIRMATION_CHANGES_PROPERTY_VALUE_MUST_NOT_BE_BLANK);

        return driverHelper.validateElementDisplayed(
                modalConfirmationChanges.confirmationChangesPropertyValueMustNotBeBlank());
    }

    public boolean validatePoiTabPresent() throws IOException {
        driverHelper.waitElementToBePresent(TabsComponent.POI_TAB);

        return driverHelper.validateElementDisplayedAndEnabled(tabsComponent.poiTab());
    }

    public boolean validateButtonAddPoiNotPresent() {
        return driverHelper.invisibilityElementLocated(StoreDetailsLocators.BUTTON_ADD_POI);
    }

    public boolean validateButtonAddMidNotPresent() {
        return driverHelper.invisibilityElementLocated(StoreDetailsLocators.BUTTON_ADD_MID);
    }

    public boolean validateButtonAddMidDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBePresent(StoreDetailsLocators.BUTTON_ADD_MID);

        return driverHelper.validateElementDisplayedAndEnabled(storeDetailsWebElements.buttonAddMid());
    }

    public boolean validateMerchantPinEditableContentNotPresent() {
        return poiWebElements.divMerchantPinEditableContent().size() <= 1;
    }

    public boolean validateButtonAddPoiDisplayedAndEnabled() throws IOException {
        return driverHelper.validateElementDisplayedAndEnabled(storeDetailsWebElements.buttonAddPoi());
    }

    public boolean validateButtonDeployDisplayedAndDisabled() throws IOException {
        return driverHelper.validateElementDisplayedAndDisabled(poiWebElements.buttonDeployDisabled());
    }

    public boolean validateButtonDeployDisplayedAndEnabled() throws IOException {
        return driverHelper.validateElementDisplayedAndEnabled(poiWebElements.buttonDeploy());
    }

    public boolean validateCheckboxInAllPoiS() {
        return poiWebElements.inputSelectPoiList().size() == listComponent.listContent().size();
    }

    public boolean validateAnyOneCheckboxIsSelected() {
        return poiWebElements.inputSelectPoiList().stream().noneMatch(WebElement::isSelected);
    }

    public boolean validateFirstCheckboxIsSelected() {
        return poiWebElements.inputSelectPoiList().get(0).isSelected();
    }

    public void inputSelectFirstPoiInList() {
        driverHelper.waitElementToBePresent(PoiLocators.INPUT_SELECT_POI);

        poiWebElements.inputSelectPoiList().stream().findFirst()
                .orElseThrow(() -> new NoSuchElementException("Any select available in list")).click();
    }

    public void inputSelectAllPoiSInList() {
        driverHelper.waitElementToBePresent(PoiLocators.INPUT_SELECT_POI);

        poiWebElements.inputSelectPoiList().stream().filter(x -> !x.isSelected()).forEach(WebElement::click);
    }

    public void inputUnSelectAllPoiSInList() {
        driverHelper.waitElementToBePresent(PoiLocators.INPUT_SELECT_POI);

        poiWebElements.inputSelectPoiList().stream().filter(WebElement::isSelected).forEach(WebElement::click);
    }

    public boolean validateCheckboxesAreSelected() {
        return poiWebElements.inputSelectPoiList().stream().filter(WebElement::isSelected).count() > 1;
    }

    public void clickButtonDeploy() throws IOException {
        driverHelper.actionClick(poiWebElements.buttonDeploy());
    }

    public boolean validateInputSelectPoiNotPresent() {
        return driverHelper.invisibilityElementLocated(PoiLocators.INPUT_SELECT_POI);
    }

    public boolean validateDeployButton() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.BUTTON_DEPLOY);

        return driverHelper.validateElementDisplayedAndEnabled(poiWebElements.buttonDeploy());
    }

    public boolean validateDivDeployChangesText(String expectedValue) throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_DEPLOY_POP_UP_TEXT);

        return driverHelper.webElementGetText(poiWebElements.divDeployPopUpText()).contains(expectedValue);
    }

    public boolean validateNumberOfButtonsPopUpDeployPoi(int expectedValue) {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_DEPLOY_POP_UP_TEXT);

        return poiWebElements.buttonsDeployPopUp().size() == expectedValue;
    }

    public List<String> validateListContainsCallToBackEnd(List<String> lst, String poiId) {
        return lst.stream().filter(x -> x.contains("https://staging-payment.dlns.io/poiRepresentations/" + poiId + "?toDeploy=true"))
                .collect(Collectors.toList());
    }

    public void clickFirstPoiCheckboxByConfigurationStatus(String configurationStatus) {
        driverHelper.waitElementToBePresent(PoiLocators.INPUT_SELECT_POI);

        List<String> lst = poiWebElements.divPoiConfigurationStatusTable().stream().map(WebElement::getText)
                .collect(Collectors.toList());

        driverHelper.actionClick(poiWebElements.inputSelectPoiList().get(lst.indexOf(configurationStatus)));
    }

    public String clickFirstPoiCheckboxByConfigurationStatusWithIntStream(String configurationStatus) {
        List<String> lst = poiWebElements.divPoiConfigurationStatusTable().stream().map(WebElement::getText)
                .collect(Collectors.toList());

        int index = IntStream.range(0, lst.size())
                .filter(i -> configurationStatus.equals(lst.get(i))).findFirst().orElse(-1);

        String poiId = poiWebElements.divPoiPoiIdTable().get(index).getText();

        driverHelper.actionClick(poiWebElements.inputSelectPoiList().get(index));

        return poiId;
    }

    public void clickFirstPoiByPoiId(String poiId) {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_POI_POI_ID_TABLE);

        driverHelper.actionClick(poiWebElements.divPoiConfigurationStatusTable().get(IntStream.range(0,
                poiWebElements.divPoiPoiIdTable().size()).filter(i -> poiId.equals(poiWebElements.divPoiPoiIdTable()
                .get(i).getText())).findFirst().orElse(-1)));
    }

    public List<String> validateNetworkCallsError(List<String> lst, String poiId) {
        return lst.stream().filter(x -> x.contains("https://staging-payment.dlns.io/poiRepresentations/" + poiId + "?toDeploy=true"))
                .collect(Collectors.toList());
    }

    public void clickFirstPoiByConfigurationStatus(String configurationStatus) {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_FIRST_POI_IN_LIST);

        List<String> lst = poiWebElements.divPoiConfigurationStatusTable().stream().map(WebElement::getText)
                .collect(Collectors.toList());

        driverHelper.actionClick(listComponent.listContent().get(lst.indexOf(configurationStatus)));
    }

    public void clickFirstPoiByConfigurationStatusOnPoiDetails(String configurationStatus) {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_ALL_LABELS);

        List<String> lst = poiWebElements.divAllLabels().stream().map(WebElement::getText)
                .collect(Collectors.toList());

        driverHelper.actionClick(poiWebElements.divAllLabels().get(lst.indexOf(configurationStatus)));
    }

    public boolean validateDivConfigurationStatus(String expectedValue) throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_POI_CONFIG_STATUS);

        return driverHelper.webElementGetText(poiWebElements.divPoiConfigStatus()).equals(expectedValue);
    }

    public boolean validateFilterPanelOnPoiTab() throws IOException {
        return driverHelper.validateElementDisplayedAndEnabled(poiFilterComponent.filterFleet()) &&
                driverHelper.validateElementDisplayedAndEnabled(poiFilterComponent.filterStoreGroup()) &&
                driverHelper.validateElementDisplayedAndEnabled(poiFilterComponent.filterStore()) &&
                driverHelper.validateElementDisplayedAndEnabled(poiFilterComponent.filterPlatform()) &&
                driverHelper.validateElementDisplayedAndEnabled(poiFilterComponent.filterConfigurationStatus());
    }

    public void clickFilterPanelFleet() throws IOException {
        driverHelper.actionClick(poiFilterComponent.filterFleet());
    }

    public void clickFilterPanelStoreGroup() throws IOException {
        driverHelper.actionClick(poiFilterComponent.filterStoreGroup());
    }

    public void clickFilterPanelStore() throws IOException {
        driverHelper.actionClick(poiFilterComponent.filterStore());
    }

    public void clickFilterPanelPlatform() throws IOException {
        driverHelper.actionClick(poiFilterComponent.filterPlatform());
    }

    public void clickFilterPanelConfigurationStatus() throws IOException {
        driverHelper.actionClick(poiFilterComponent.filterConfigurationStatus());
    }

    public boolean validateFilterPanelFilterListBoxItemsHasCheckBoxes() throws IOException {
        return poiFilterComponent.filterListBoxItems().stream()
                .allMatch(x -> x.findElement(By.cssSelector("input[type='checkbox']")).isEnabled());
    }

    public void selectFilterPanelFilterListBoxSpecificItem(String item) throws IOException {
        poiFilterComponent.filterListBoxItems().stream().filter(x -> x.getText().contains(item))
                .map(x -> x.findElement(By.cssSelector("input[type='checkbox']"))).findFirst()
                .orElseThrow(() -> new NoSuchElementException("Any item found")).click();
    }

    public List<String> getTextFromPlatformDropdown() throws IOException {
        driverHelper.waitElementToBeClickable(SelectPlatformComponent.SELECT_PLATFORM);

       return  driverHelper.getTextFromListWebElements(selectPlatformComponent.selectPlatformList()
                .stream().collect(Collectors.toList()));
    }

    public void insertFieldsCreatePoiModalSpecificPlatform(String notes, int position) throws IOException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        driverHelper.selectOptionByPosition(selectPlatformComponent.selectPlatformOptions(), position);

        driverHelper.insertText(modalCommonsComponent.modalDialogInputNotes(), notes);

        driverHelper.actionClick(modalCommonsComponent.modalDialogButtonCreate());
    }

    public boolean validatePlatformChosen(String text) throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.DIV_PLATFORM);

        return driverHelper.validateElementDisplayed(poiFilterComponent.divPlatform()) &&
                driverHelper.webElementGetText(poiFilterComponent.divPlatform()).equals(text);
    }

    public void clickPoiInList(String poiId) throws IOException {
        driverHelper.actionClick(driverHelper.createWebElement(By.xpath("//div[contains(text(), '"+poiId+"')]")));
    }


    public boolean validateImgPaxD190() throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.IMG_PAX_D190);

        return driverHelper.validateElementDisplayed(poiFilterComponent.imgPaxD190());
    }

    public boolean validateImgPaxS920() throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.IMG_PAX_S920);

        return driverHelper.validateElementDisplayed(poiFilterComponent.imgPaxS920());
    }

    public boolean validateButtonAddLabelNotPresent(){
        return driverHelper.invisibilityElementLocated(PoiFilterComponent.BUTTON_ADD_LABEL);
    }

    public boolean validateButtonDeletePoiNotPresent(){
        return driverHelper.invisibilityElementLocated(PoiLocators.BUTTON_POI_DELETE);
    }

    public boolean validateButtonAddLabelIsPresent() throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.BUTTON_ADD_LABEL);

        return driverHelper.validateElementDisplayedAndEnabled(poiFilterComponent.buttonAddLabel());
    }

    public String getLabelsOnFirstPoiInList() throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.DIV_FIRST_POI_IN_LIST);

        return poiFilterComponent.divFirstPoiInList().findElement(By.cssSelector("div[data-automation-id='poi-cell-4']"))
                .getText();
    }

    public String getLabelsOnSecondPoiInList() throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.DIV_SECOND_POI_IN_LIST);

        return poiFilterComponent.divSecondPoiInList().findElement(By.cssSelector("div[data-automation-id='poi-cell-4']"))
                .getText();
    }

    public void clickAddLabelButton() throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.BUTTON_ADD_LABEL);

        driverHelper.actionClick(poiFilterComponent.buttonAddLabel());
    }

    public void insertLabelName(String labelName) throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.INPUT_POI_LABEL);

        driverHelper.insertText(poiFilterComponent.inputPoiLabel(), labelName);
    }

    public void inputSelectSecondPoiInList() {
        driverHelper.waitElementToBePresent(PoiLocators.INPUT_SELECT_POI);

        poiWebElements.inputSelectPoiList().get(1).click();
    }

    public void clickDivLabel() throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.DIV_LABEL);

        driverHelper.actionClick(poiFilterComponent.divLabel());
    }

    public void clickOnLabel(String label) throws IOException, InterruptedException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.DIV_LABEL);

        poiFilterComponent.divLabel().findElement(By.cssSelector("input[value='"+label+"']")).click();

        iHelpers.sleepOneSecond();

        poiFilterComponent.buttonLabelSave().click();

        iHelpers.sleepOneSecond();

        clickConfirmationChangesSpanYes();
    }

    public String[] getLabels() {
        driverHelper.waitElementToBePresent(PoiFilterComponent.DIV_LABEL);

       return driverHelper.createListWebElement(PoiFilterComponent.DIV_LABEL).get(0).getText().split("\n");
    }

    public void clickOnLabelCancel(String label) throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.DIV_LABEL);

        clickDivLabel();

        poiFilterComponent.divLabel().findElement(By.cssSelector("input[value='"+label+"']")).click();

        poiFilterComponent.buttonLabelCancel().click();
    }

    public boolean validateTextInvalidInputRequiredField() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.TEXT_INVALID_INPUT);

        return driverHelper.validateElementDisplayed(poiWebElements.textInvalidInputRequiredField());
    }

    public boolean validateTextInvalidInputOnlyDigitsAreAllowed() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.TEXT_INVALID_INPUT_ONLY_DIGITS_ARE_ALLOWED);

        return driverHelper.validateElementDisplayed(poiWebElements.textInvalidInputOnlyDigitsAreAllowed());
    }

    public void clickPropertiesLabel() throws IOException {
        driverHelper.waitElementToBePresent(PoiFilterComponent.DIV_LABEL);

        driverHelper.actionClick(poiFilterComponent.propertiesLabel());
    }

    public boolean getTextFromLabelDropdown(String text) throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_POI_LABEL);

        return driverHelper.webElementGetText(poiWebElements.divPoiLabel()).equals(text);
    }

    public void clickLabelDropdown() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_POI_LABEL);

        driverHelper.actionClick(poiWebElements.divPoiLabel());
    }

    public List<String> getTextFromLabelDropdown() {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_POI_LABEL_DROPDOWN);

        return  driverHelper.getTextFromListWebElements(new ArrayList<>(poiWebElements.divPoiLabelDropdown()));
    }

    public void clickOnSpecificLabel(String label) throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_POI_LABEL_DROPDOWN);

        poiFilterComponent.divLabelClass().findElement(By.xpath("//span[text()='"+label+"']")).click();

        commonSteps.clickTabPoi();

    }

    public List<String> getTextFromDivAllLabels() {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_ALL_LABELS);

        return  driverHelper.getTextFromListWebElements(new ArrayList<>(poiWebElements.divAllLabels()));
    }

    public boolean validateDeletePoiButtonIsDisabled() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.BUTTON_POI_DELETE);

        String elementClass = poiWebElements.buttonDeletePoi().getAttribute("class");

        return elementClass.contains("disabled");
    }

    public boolean validateDeletePoiButtonIsEnabled() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.BUTTON_POI_DELETE);

        return driverHelper.validateElementDisplayedAndEnabled(poiWebElements.buttonDeletePoi());
    }

    public void clickOnSelectAll() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.SPAN_SELECT_ALL);

        driverHelper.actionClick(poiWebElements.spanSelectAll());

    }

    public void clickOnDeletePoi() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.BUTTON_POI_DELETE);

        driverHelper.actionClick(poiWebElements.buttonDeletePoi());

    }

    public boolean validateDivPopUpDeletePoi(String text) throws IOException {
        driverHelper.waitElementToBePresent(PoiGroupLocators.DIV_POP_UP_DELETE_POI_GROUP);

        return driverHelper.webElementGetText(poiGroupWebElements.divPopUpDeletePoiGroup())
                .contains(text);
    }

    public List<String> getPoiIdList() {
        return  poiWebElements.divPoiPoiIdTable().stream().map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public boolean validateEnabledOrDisabledIcons() {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_POI_POI_ID_TABLE_ENABLED);

        return poiWebElements.divPoiEnabledList().stream().allMatch(x -> x.findElement(
                By.cssSelector("svg[data-automation-id='cross']")).isDisplayed() ||
                x.findElement(By.cssSelector("svg[data-automation-id='check']")).isDisplayed());
    }

    public boolean validateAllPoisAreEnabled() {
        driverHelper.waitElementToBePresent(PoiLocators.DIV_POI_POI_ID_TABLE_ENABLED);

        return poiWebElements.divPoiEnabledList().stream().allMatch(x ->
                x.findElement(By.cssSelector("svg[data-automation-id='check']")).isDisplayed());
    }

    public void clickDivNotes() throws IOException {
        driverHelper.actionClick(poiWebElements.divNotes());
    }

    public void sendEscapeKeyToDivNotesEditableContentText() {
        driverHelper.sendEscapeKey(poiWebElements.divsEditableContentText().get(0));
    }

    public boolean validateDivNotesEditableContentSpanSave() throws IOException {
        return driverHelper.validateElementDisplayed(poiWebElements.divMerchantPinEditableContentSpanSave());
    }

    public boolean validateDivNotesEditableContentSpanCancel() throws IOException {
        return driverHelper.validateElementDisplayed(poiWebElements.divMerchantPinEditableContentSpanCancel());
    }

    public boolean validateDivNotesEditableContentSpansAreNotDisplayed() {
        return driverHelper.invisibilityElementLocated(PoiLocators.DIV_MERCHANT_PIN_EDITABLE_CONTENT_SPAN_CANCEL) &&
                driverHelper.invisibilityElementLocated(PoiLocators.DIV_MERCHANT_PIN_EDITABLE_CONTENT_SPAN_SAVE) ;
    }

    public void clearDivNotesEditableContentText() throws IOException {
        clickDivNotes();

        if (poiWebElements.divsEditableContentText().get(0).isDisplayed()) {
            poiWebElements.divsEditableContentText().get(0).sendKeys(Keys.CONTROL + "a");

            poiWebElements.divsEditableContentText().get(0).sendKeys(Keys.DELETE);
        }
    }

    public String getTextDivNotesEditableContent() {
        return driverHelper.webElementGetText(poiWebElements.divMerchantPinEditableContent().get(0));
    }

    public void sendNotesToDivNotesEditableText(String notes) {
        poiWebElements.divsEditableContentText().get(0).sendKeys(notes);
    }

    public boolean validateTextInvalidInputNotesSize() throws IOException {
        driverHelper.waitElementToBePresent(PoiLocators.TEXT_INVALID_INPUT_SIZE);

        return driverHelper.validateElementDisplayed(poiWebElements.textInvalidInpuSize());
    }
}