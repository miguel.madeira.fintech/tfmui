package steps.fleet;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.*;
import driverhelper.DriverHelper;
import helpers.IHelpers;
import org.openqa.selenium.WebDriver;
import pages.fleet.FleetPageLocators;
import pages.fleet.FleetPageWebElements;
import pages.fleetdetails.FleetDetailsLocators;
import pages.fleetdetails.FleetDetailsWebElements;
import properties.Properties;

import java.io.IOException;
import java.util.NoSuchElementException;

public class FleetSteps {

    private FleetPageWebElements fleetPageWebElements;

    private FleetDetailsWebElements fleetDetailsWebElements;

    private SelectRetailerComponent selectRetailerComponent;

    private ModalCreateFleetComponent modalCreateFleetComponent;

    private ModalCommonsComponent modalCommonsComponent;

    private ButtonRemoveComponent buttonRemove;

    private ListComponent listComponent;

    private DriverHelper driverHelper;

    private IHelpers iHelpers;


    public FleetSteps(WebDriver driver, Properties properties){
        Injector injector = Guice.createInjector(new AppInjector());

        iHelpers = injector.getInstance(IHelpers.class);

        fleetPageWebElements = new FleetPageWebElements(driver, properties);

        fleetDetailsWebElements = new FleetDetailsWebElements(driver, properties);

        selectRetailerComponent = new SelectRetailerComponent(driver, properties);

        modalCreateFleetComponent = new ModalCreateFleetComponent(driver, properties);

        modalCommonsComponent = new ModalCommonsComponent(driver, properties);

        buttonRemove = new ButtonRemoveComponent(driver, properties);

        listComponent = new ListComponent(driver, properties);

        driverHelper = new DriverHelper(driver, properties);
    }

    public void clickButtonAddFleet() throws IOException {
        driverHelper.actionClick(fleetPageWebElements.buttonAddFleet());
    }

    public boolean validateButtonAddFleetDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBeClickable(FleetPageLocators.BUTTON_ADD_FLEET);

        return driverHelper.validateElementDisplayedAndEnabled(fleetPageWebElements.buttonAddFleet());
    }

    public boolean validateCreateFleetModal() throws IOException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        return  driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogButtonCreate()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCreateFleetComponent.modalCreateFleetAcceptanceServicesContent()) &&
                driverHelper.webElementGetText(modalCreateFleetComponent.modalCreateFleetAcceptanceServicesContent())
                .contains("VISA") && driverHelper.webElementGetText(modalCreateFleetComponent.modalCreateFleetAcceptanceServicesContent())
                .contains("MASTERCARD") && driverHelper.webElementGetText(modalCreateFleetComponent.modalCreateFleetAcceptanceServicesContent())
                .contains("CB") &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonCancel()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        fleetPageWebElements.buttonCloseFleet()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonCreate()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCreateFleetComponent.modalCreateFleetInputFleetName()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogInputNotes()) &&
                driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogLabel()) &&
                driverHelper.webElementGetText(modalCommonsComponent.modalDialogLabel())
                        .equals("Create Fleet")&&
                driverHelper.validateElementDisplayedAndEnabled(selectRetailerComponent.selectRetailerOptions()
                        .getWrappedElement());
    }

    public String insertFieldsCreateFleetModal(String fleetName, String notes) throws IOException, InterruptedException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        driverHelper.selectRandomOptionInSelect(selectRetailerComponent.selectRetailerOptions());

        driverHelper.waitElementToBeClickable(ModalCreateFleetComponent.MODAL_CREATE_FLEET_INPUT_FLEET_NAME);

        driverHelper.insertText(modalCreateFleetComponent.modalCreateFleetInputFleetName(), fleetName);

        driverHelper.insertText(modalCommonsComponent.modalDialogInputNotes(), notes);

        return driverHelper.webElementGetText(selectRetailerComponent.selectRetailerOptions().getFirstSelectedOption());
    }

    public boolean validateFleetCreatedInFleetList(String fleetName){
        driverHelper.waitElementToBePresent(ListComponent.LIST);

        return listComponent.listContent().stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("List not found")).getText().startsWith(fleetName);
    }

    public void clickButtonAddStoreGroup() throws IOException {
        driverHelper.actionClick(fleetDetailsWebElements.buttonAddStoreGroup());
    }

    public boolean validateDisplayedAndEnabledDivNoStoreGroup() throws IOException {
        driverHelper.waitElementToBePresent(FleetDetailsLocators.DIV_NO_STORE_GROUP);

        return driverHelper.validateElementDisplayedAndEnabled(fleetDetailsWebElements.divNoStoreGroup());
    }

    public void clickButtonCloseModalDialogFleet() throws IOException {
        driverHelper.actionClick(fleetPageWebElements.buttonCloseFleet());
    }

    public boolean validateFleetListPresent(){
        driverHelper.waitElementToBePresent(ListComponent.LIST);

        return listComponent.listHeader().stream().findFirst().orElseThrow(()-> new NoSuchElementException(
                "The list header is not present")).getText().equals("Select all\n" +
                "Fleet\n" +
                "State\n" +
                "Retailer\n" +
                "Profile\n" +
                "Nr of POIs");
    }

    public boolean validateFleetListPresentForRetailer(){
        driverHelper.waitElementToBePresent(ListComponent.LIST);

        return listComponent.listHeader().stream().findFirst().orElseThrow(()-> new NoSuchElementException(
                "The list header is not present")).getText().equals(
                "Fleet\n" +
                "State\n" +
                "Retailer\n" +
                "Profile\n" +
                "Nr of POIs");
    }

    public boolean validateButtonAddFleetNotPresent(){
        return driverHelper.invisibilityElementLocated(FleetPageLocators.BUTTON_ADD_FLEET);
    }

    public String[] getTextFromNrOfStoreGroups() throws IOException {
        driverHelper.waitElementToBePresent(FleetDetailsLocators.DIV_NR_OF_STORE_GROUPS);

        return driverHelper.webElementGetText(fleetDetailsWebElements.divNrStoreGroup()).split("\n");
    }

    public boolean validateNrOfStoreGroups(int expectedNumberOfStoreGroups) {
        driverHelper.waitElementToBePresent(FleetDetailsLocators.CELL_STORE_GROUPS);

        return fleetDetailsWebElements.cellStoreGroupRow().size() == expectedNumberOfStoreGroups;
    }

    public void clickButtonDeleteFleet() throws IOException {
        driverHelper.waitElementToBePresent(ButtonRemoveComponent.BUTTON_DELETE_FLEETS);

        driverHelper.actionClick(buttonRemove.buttonDeleteFleets());
    }
}