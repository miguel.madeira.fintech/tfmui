package steps.storegroup;

import components.ButtonRemoveComponent;
import components.ListComponent;
import components.ModalCommonsComponent;
import components.ModalCreateStoreGroupComponent;
import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import pages.fleet.FleetPageLocators;
import pages.fleetdetails.FleetDetailsLocators;
import pages.fleetdetails.FleetDetailsWebElements;
import pages.storegroupdetails.StoreGroupDetailsLocators;
import pages.storegroupdetails.StoreGroupDetailsWebElements;
import properties.Properties;
import steps.common.CommonSteps;
import steps.fleet.FleetSteps;

import java.io.IOException;

public class StoreGroupSteps {

    private FleetSteps fleetSteps;

    private CommonSteps commonSteps;

    private ButtonRemoveComponent buttonRemove;

    private FleetDetailsWebElements fleetDetailsWebElements;

    private StoreGroupDetailsWebElements storeGroupDetailsWebElements;

    private ModalCommonsComponent modalCommons;

    private ModalCreateStoreGroupComponent modalCreateStoreGroupComponent;

    private ListComponent listComponent;

    private DriverHelper driverHelper;

    public StoreGroupSteps(WebDriver driver, Properties properties){
        fleetSteps = new FleetSteps(driver, properties);

        commonSteps = new CommonSteps(driver, properties);

        buttonRemove = new ButtonRemoveComponent(driver,properties);

        fleetDetailsWebElements = new FleetDetailsWebElements(driver, properties);

        storeGroupDetailsWebElements = new StoreGroupDetailsWebElements(driver, properties);

        modalCreateStoreGroupComponent = new ModalCreateStoreGroupComponent(driver, properties);

        modalCommons = new ModalCommonsComponent(driver, properties);

        listComponent = new ListComponent(driver, properties);

        driverHelper = new DriverHelper(driver, properties);
    }

    public boolean validateButtonAddStoreGroupDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBeClickable(FleetDetailsLocators.BUTTON_ADD_STORE_GROUP);

        return driverHelper.validateElementDisplayedAndEnabled(fleetDetailsWebElements.buttonAddStoreGroup());
    }

    public boolean validateCreateStoreGroupModal() throws IOException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        return  driverHelper.validateElementDisplayedAndEnabled(modalCommons.modalDialogButtonCreate()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommons.modalDialogButtonCancel()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommons.modalDialogButtonClose()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommons.modalDialogButtonCreate()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCreateStoreGroupComponent.modalCreateStoreGroupInputStoreGroupName()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCreateStoreGroupComponent.modalCreateStoreGroupInputNotes()) &&
                driverHelper.validateElementDisplayedAndEnabled(modalCommons.modalDialogLabel()) &&
                driverHelper.webElementGetText(modalCommons.modalDialogLabel())
                        .equals("Create Store Group");
    }

    public void insertFieldsCreateStoreGroupModal(String storeGroupName, String notes) throws IOException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CREATE);

        driverHelper.waitElementToBeClickable(
                ModalCreateStoreGroupComponent.MODAL_CREATE_STORE_GROUP_INPUT_STORE_GROUP_NAME);

        driverHelper.insertText(modalCreateStoreGroupComponent.modalCreateStoreGroupInputStoreGroupName(), storeGroupName);

        driverHelper.insertText(modalCreateStoreGroupComponent.modalCreateStoreGroupInputNotes(), notes);
    }

    public void addStoreGroupInsertFieldsAndCreate(String storeGroupName) throws IOException {
        fleetSteps.clickButtonAddStoreGroup();

        insertFieldsCreateStoreGroupModal(storeGroupName, "");

        commonSteps.clickButtonCreateInModal();
    }

    public boolean validateStoreGroupDetails(String solutionContractName, String fleetName, String storeGroupName,
           String profileName, String notes, String numberOfPois, String numberOfStores) throws IOException {

        driverHelper.waitElementToBePresent(StoreGroupDetailsLocators.DIV_CREATED_DATE);

        return driverHelper.webElementGetText(storeGroupDetailsWebElements.divStoreGroupName()).contains("Store Group") &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divStoreGroupName()).contains(storeGroupName) &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divFleet()).contains("Fleet") &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divFleet()).contains(fleetName) &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divPoiProfile()).contains("Profile") &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divPoiProfile()).contains(profileName) &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divRetailer()).contains("Retailer") &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divRetailer()).contains(solutionContractName) &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divNumberOfPois()).contains("Nr of POIs") &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divNumberOfPois()).contains(numberOfPois) &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divNumberOfStores()).contains("Nr of Stores") &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divNumberOfStores()).contains(numberOfStores) &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divNotes()).contains("Notes") &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divNotes()).contains(notes) &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divCreatedDate()).contains("Created") &&
            driverHelper.webElementGetText(storeGroupDetailsWebElements.divModifiedDate()).contains("Modified");
    }

    public boolean validateThereAreNoStoresAvailable() throws IOException {
        driverHelper.waitElementToBePresent(StoreGroupDetailsLocators.DIV_NO_STORES_ARE_DEFINED);

        return driverHelper.validateElementDisplayedAndEnabled(storeGroupDetailsWebElements.divNoStoresAreDefined());
    }

    public void createStoreGroupIfNecessary(String storeGroupName) throws IOException {
        if(listComponent.columns().size() == 0){
            driverHelper.actionClick(fleetDetailsWebElements.buttonAddStoreGroup());

            insertFieldsCreateStoreGroupModal(storeGroupName, "");

            commonSteps.clickButtonCreateInModal();
        }
    }

    public void clickAddStoreButton() throws IOException {
        driverHelper.waitElementToBeClickable(StoreGroupDetailsLocators.BUTTON_ADD_STORE);

        driverHelper.actionClick(storeGroupDetailsWebElements.buttonAddStore());
    }

    public boolean validateErrorMessageStoreGroupNameAlreadyExists() throws IOException {
        driverHelper.waitElementToBePresent(
                ModalCreateStoreGroupComponent.MODAL_FEEDBACK_ERROR_MESSAGE_STORE_GROUP_ALREADY_EXISTS);

        return driverHelper.validateElementDisplayed(
                modalCreateStoreGroupComponent.modalFeedbackErrorMessageStoreGroupAlreadyExists());
    }

    public boolean validateErrorMessageMinLength() throws IOException {
        driverHelper.waitElementToBePresent(
                ModalCreateStoreGroupComponent.MODAL_FEEDBACK_ERROR_MESSAGE_MIN_LENGTH);

        return driverHelper.validateElementDisplayed(
                modalCreateStoreGroupComponent.modalFeedbackErrorMessageMinLength());
    }

    public boolean validateButtonAddStoreGroupNotPresent(){
        return driverHelper.invisibilityElementLocated(FleetDetailsLocators.BUTTON_ADD_STORE_GROUP);
    }

    public String[] getTextFromNrOfStores() throws IOException {
        driverHelper.waitElementToBePresent(StoreGroupDetailsLocators.DIV_NR_OF_STORES);

        return driverHelper.webElementGetText(storeGroupDetailsWebElements.divNrStores()).split("\n");
    }

    public void clickDeleteStoreGroupButton() throws IOException {
        driverHelper.waitElementToBePresent(buttonRemove.BUTTON_DELETE_STORE_GROUP);

        driverHelper.actionClick(buttonRemove.buttonDeleteStoreGroup());
    }

    public boolean validateNrOfStores(int expectedNumberOfStores) {
        driverHelper.waitElementToBePresent(StoreGroupDetailsLocators.CELL_NUMBER_OF_STORES);

        return storeGroupDetailsWebElements.cellNrOfStores().size() == expectedNumberOfStores;
    }

    public void clickButtonDeleteStore() throws IOException {
        driverHelper.waitElementToBePresent(buttonRemove.BUTTON_DELETE_STORE);

         driverHelper.actionClick(buttonRemove.buttonDeleteStore());
    }

    public int getNumberOfStores(){
        driverHelper.waitElementToBePresent(StoreGroupDetailsLocators.CELL_NUMBER_OF_STORES);

        return storeGroupDetailsWebElements.cellNrOfStores().size();
    }
}