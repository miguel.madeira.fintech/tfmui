package steps.mid;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.*;
import driverhelper.DriverHelper;
import helpers.IHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.openqa.selenium.WebDriver;
import pages.storedetails.StoreDetailsWebElements;
import properties.Properties;
import steps.common.CommonSteps;

import java.io.IOException;

public class MidSteps {
    private ModalCommonsComponent modalCommonsComponent;

    private ModalCreateMidComponent modalCreateMidComponent;

    private StoreDetailsWebElements storeDetailsWebElements;

    private DriverHelper driverHelper;

    private CommonSteps commonSteps;

    private IHelpers iHelpers;

    public MidSteps(WebDriver driver, Properties properties){
        Injector injector = Guice.createInjector(new AppInjector());

        iHelpers = injector.getInstance(IHelpers.class);

        modalCommonsComponent = new ModalCommonsComponent(driver, properties);

        modalCreateMidComponent = new ModalCreateMidComponent(driver, properties);

        storeDetailsWebElements = new StoreDetailsWebElements(driver,properties);

        driverHelper = new DriverHelper(driver, properties);

        commonSteps = new CommonSteps(driver, properties);
    }

    public boolean validateCreateMidModal() throws IOException {
        driverHelper.waitElementToBeClickable(ModalCommonsComponent.MODAL_DIALOG);

        return driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonCancel()) &&
                driverHelper.validateElementDisplayedAndEnabled(
                        modalCommonsComponent.modalDialogButtonClose()) &&
                driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogLabel()) &&
                driverHelper.webElementGetText(modalCommonsComponent.modalDialogLabel()).equals("Create Merchant ID") &&
                        driverHelper.validateElementDisplayedAndEnabled(modalCreateMidComponent.inputMerchantId()) &&
                        driverHelper.validateElementDisplayedAndEnabled(modalCreateMidComponent.inputMerchantNameAndLocation()) &&
                        driverHelper.validateElementDisplayedAndEnabled(modalCreateMidComponent.inputAcquiredId()) &&
                driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogButtonCreate());
    }

    public void selectVisaRadioButton() throws IOException {
        driverHelper.actionClick(modalCreateMidComponent.inputRadioVisa());
    }

    public void selectMastercardRadioButton() throws IOException {
        driverHelper.actionClick(modalCreateMidComponent.inputRadioMastercard());
    }

    public void selectCbRadioButton() throws IOException {
        driverHelper.actionClick(modalCreateMidComponent.inputRadioCb());
    }

    public boolean validateCreateMidModalAfterAcceptanceServiceSelection() throws IOException {
       return driverHelper.validateDataAttribute(modalCreateMidComponent.inputMerchantId(),
                "maxlength", "15") &&
                driverHelper.validateDataAttribute(modalCreateMidComponent.inputMerchantCategoryCode(),
                        "maxlength", "4") &&
                driverHelper.validateDataAttribute(modalCreateMidComponent.inputMerchantNameAndLocation(),
                        "maxlength", "70") &&
                driverHelper.validateDataAttribute(modalCreateMidComponent.inputAcquiredId(),
                        "maxlength", "11") &&
               driverHelper.validateElementDisplayedAndEnabled(modalCommonsComponent.modalDialogButtonCreate());
    }

    public void insertMidFields(String merchantId, String categoryCode, String nameAndLocation, String acquiredId)
            throws IOException {

        driverHelper.insertText(modalCreateMidComponent.inputMerchantId(), merchantId);

        driverHelper.insertText(modalCreateMidComponent.inputMerchantCategoryCode(), categoryCode);

        driverHelper.insertText(modalCreateMidComponent.inputMerchantNameAndLocation(), nameAndLocation);

        driverHelper.insertText(modalCreateMidComponent.inputAcquiredId(), acquiredId);
    }

    public void insertMerchantIdField(String merchantId)
            throws IOException {
        driverHelper.insertText(modalCreateMidComponent.inputMerchantId(), merchantId);
    }

    public void insertMerchantCategoryCode(String categoryCode)
            throws IOException {
        driverHelper.insertText(modalCreateMidComponent.inputMerchantCategoryCode(), categoryCode);
    }

    public void insertNameAndLocation(String nameAndLocation)
            throws IOException {
        driverHelper.insertText(modalCreateMidComponent.inputMerchantNameAndLocation(), nameAndLocation);
    }

    public void insertAcquiredId(String acquiredId)
            throws IOException {
        driverHelper.insertText(modalCreateMidComponent.inputAcquiredId(), acquiredId);
    }

    public boolean validateTextMidFields(String merchantId, String categoryCode, String nameAndLocation,
           String acquiredId) throws IOException {

        return driverHelper.validateDataAttribute(modalCreateMidComponent.inputMerchantId(),"value", merchantId) &&
               driverHelper.validateDataAttribute(modalCreateMidComponent.inputMerchantCategoryCode(), "value", categoryCode) &&
               driverHelper.validateDataAttribute(modalCreateMidComponent.inputMerchantNameAndLocation(), "value", nameAndLocation) &&
               driverHelper.validateDataAttribute(modalCreateMidComponent.inputAcquiredId(),"value", acquiredId);

    }

    public void clickButtonCreateMid() throws IOException {
        driverHelper.actionClick(modalCommonsComponent.modalDialogButtonCreate());
    }

    public void insertRequiredFieldsClickCreateAndValidateMccErrorMessage(String merchantId, String categoryCode,
            String merchantNameAndLocation, String acquiredId) throws IOException, InterruptedException {
        insertMidFields(merchantId, categoryCode, merchantNameAndLocation,
                acquiredId);

        iHelpers.sleepOneSecond();

        clickButtonCreateMid();

        iHelpers.sleepOneSecond();

        Assumptions.assumeTrue(validateErrorMessageOnlyDigitsAreAllowed(),
                "It was expected the message present");
    }

    private boolean validateErrorMessageOnlyDigitsAreAllowed() throws IOException {
        return driverHelper.validateElementDisplayed(modalCreateMidComponent.errorMessageOnlyDigitsAreAllowed());
    }

    public void insertMidFieldsAndValidateValues(String merchantId, String merchantCategoryCode,
           String merchantNameAndLocation, String acquiredId) throws IOException, InterruptedException {
        insertMidFields(merchantId, merchantCategoryCode, merchantNameAndLocation, acquiredId);

        iHelpers.sleepOneSecond();

        Assumptions.assumeTrue(validateTextMidFields(merchantId, merchantCategoryCode, merchantNameAndLocation,
                acquiredId), "It was expected the same text in fields");
    }

    public void validateEmptyRequiredFields() throws InterruptedException, IOException {
        iHelpers.sleepOneSecond();

        Assertions.assertTrue(validateTextMidFields("","", "", ""),
                "It was expected the same text in fields");
    }

    public boolean validateAddMidButtonIsEnabled() throws IOException {
        return driverHelper.validateElementDisplayedAndEnabled(storeDetailsWebElements.buttonAddMid());
    }

    public boolean validateAddMidButtonIsDisabled() {
        String elementClass = storeDetailsWebElements.buttonAddMidInList().get(1).getAttribute("class");

        if(elementClass.contains("disabled")) {
            return true;
        }
        return false;
    }
}