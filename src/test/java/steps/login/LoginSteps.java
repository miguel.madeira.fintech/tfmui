package steps.login;

import driverhelper.DriverHelper;
import org.openqa.selenium.WebDriver;
import pages.login.LoginPageLocators;
import pages.login.LoginPageWebElements;
import properties.Properties;

import java.io.IOException;

public class LoginSteps {

    private LoginPageWebElements loginPageWebElements;

    private DriverHelper driverHelper;

    public LoginSteps(WebDriver driver, Properties properties){
        loginPageWebElements = new LoginPageWebElements(driver, properties);

        driverHelper = new DriverHelper(driver, properties);
    }

    public void login(String username, String password) throws IOException {
        driverHelper.waitElementToBeClickable(LoginPageLocators.INPUT_USERNAME);

        driverHelper.insertText(loginPageWebElements.inputUserName(), username);

        driverHelper.insertText(loginPageWebElements.inputPassword(), password);

        driverHelper.actionClick(loginPageWebElements.buttonLogin());
    }
}