package steps.Identities;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.IdentitiesComponent;
import components.ListComponent;
import driverhelper.DriverHelper;
import helpers.IHelpers;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class IdentitiesSteps {

    private IdentitiesComponent identitiesComponent;

    private ListComponent listComponent;

    private DriverHelper driverHelper;

    private IHelpers iHelpers;

    public IdentitiesSteps(WebDriver driver, Properties properties) {
        Injector injector = Guice.createInjector(new AppInjector());

        identitiesComponent = new IdentitiesComponent(driver,properties);

        listComponent = new ListComponent(driver,properties);

        driverHelper = new DriverHelper(driver, properties);

        iHelpers = injector.getInstance(IHelpers.class);
    }

    public String getTextHeader() {
        driverHelper.waitElementToBePresent(IdentitiesComponent.HEADER_IDENTITIES);

        return identitiesComponent.headerIdentities().stream().map(WebElement::getText).collect(Collectors.toList()).toString();
    }

    public String getTextRowsPerPage() throws IOException {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_ROWS_PER_PAGE);

        return driverHelper.webElementGetText(identitiesComponent.divRowsPerPage());
    }

    public List<String> getTableRows() {
        driverHelper.waitElementToBePresent(IdentitiesComponent.TABLE_ROWS);

        return identitiesComponent.tableRows().stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public void scrollDownInsideTable(int position) throws IOException {
        driverHelper.waitElementToBePresent(IdentitiesComponent.TABLE_USERS);

        driverHelper.actionMoveToElement(driverHelper.createWebElement(
                By.cssSelector("tr[data-automation-id='identity-row-"+position+"']")));
    }

    public void clickRowsPerPage(String rows) throws IOException {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_ROWS_PER_PAGE);

         driverHelper.actionClick(identitiesComponent.divRowsPerPage());

         driverHelper.actionMoveToElementAndClick(driverHelper.createWebElement(
                 By.cssSelector("li[data-value='"+rows+"']")));
    }

    public boolean clickNextButtonWhileEnabledAndValidateNumberOfResultsPerPage(int numberOfResultsPerPage) throws IOException {
        while(identitiesComponent.buttonNext().isEnabled()){
            if(getTableRows().size() != numberOfResultsPerPage){
                return false;
            }
            driverHelper.actionClick(identitiesComponent.buttonNext());
        }
        return true;
    }

    public String getTextFirstRow() throws IOException {
        driverHelper.waitElementToBePresent(IdentitiesComponent.TABLE_FIRST_ROW);

        return driverHelper.webElementGetText(identitiesComponent.tableFirstRow());
    }

    public void clickHeaderUsername() throws IOException {
        driverHelper.waitElementToBePresent(IdentitiesComponent.TABLE_HEADER_USERNAME);

        driverHelper.actionClick(identitiesComponent.tableHeaderUsername());
    }

    public void clickFirstRow() throws IOException {
        driverHelper.waitElementToBePresent(IdentitiesComponent.TABLE_FIRST_ROW);

        driverHelper.actionClick(identitiesComponent.tableFirstRow());
    }

    public void clickDivId() throws IOException {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_ID);

        driverHelper.actionClick(identitiesComponent.divId());
    }

    public String getTextDetailsData() throws IOException {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_DETAILS_DATA);

        return driverHelper.webElementGetText(identitiesComponent.divDetailsData());
    }

    public boolean validateDetailsData() throws IOException {
        String detailsData = getTextDetailsData();

        return detailsData.contains("Id") && detailsData.contains("Username") && detailsData.contains("Email")
                && detailsData.contains("Realm") && detailsData.contains("Status") && detailsData.contains("Creation Date")
                && detailsData.contains("Update Date") && detailsData.contains("Last Update Reason");
    }

    public void clickDivEditableFieldByPosition(int position) {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_EDITABLE_FIELDS);

        driverHelper.actionClick(identitiesComponent.divEditableFields().get(position));
    }

    public void sendTextEditableFieldUsername(String text) {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_EDITABLE_FIELDS);

        clickDivEditableFieldByPosition(0);

        driverHelper.insertText(identitiesComponent.divEditableFieldsTextArea().get(0),text);
    }

    public void sendTextEditableFieldEmail(String text) {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_EDITABLE_FIELDS);

        clickDivEditableFieldByPosition(1);

        driverHelper.insertText(identitiesComponent.divEditableFieldsTextArea().get(2),text);
    }

    public String getTextEditableFieldByPosition(int position) {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_EDITABLE_FIELDS);

        return driverHelper.webElementGetText(identitiesComponent.divEditableFields().get(position));
    }

    public boolean validateUsernameSizeErrorMessage() throws IOException {
        driverHelper.waitElementToBePresent(IdentitiesComponent.USERNAME_SIZE_ERROR_MESSAGE);

        return driverHelper.validateElementDisplayed(identitiesComponent.usernameSizeErrorMessage()) &&
                driverHelper.webElementGetText(identitiesComponent.usernameSizeErrorMessage()).equals("Field 'userName' " +
                        "size must be between 3 and 50 in object 'writeIdentityRequest'");
    }

    public void clearEditableFieldUsername() {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_EDITABLE_FIELDS_TEXT_AREA);

        clickDivEditableFieldByPosition(0);

        identitiesComponent.divEditableFieldsTextArea().get(0).sendKeys(Keys.CONTROL + "a");

        identitiesComponent.divEditableFieldsTextArea().get(0).sendKeys(Keys.DELETE);
    }

    public void clearEditableFieldEmail() {
        driverHelper.waitElementToBePresent(IdentitiesComponent.DIV_EDITABLE_FIELDS_TEXT_AREA);

        clickDivEditableFieldByPosition(1);

        identitiesComponent.divEditableFieldsTextArea().get(2).sendKeys(Keys.CONTROL + "a");

        identitiesComponent.divEditableFieldsTextArea().get(2).sendKeys(Keys.DELETE);
    }

    public boolean validateFirstCheckboxDataIndeterminate(String dataIndeterminate) {
        return driverHelper.validateDataAttribute(listComponent.inputCheckboxes().get(0),
                "data-indeterminate", dataIndeterminate);
    }
}
