package steps.common;

import appinjector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import components.*;
import driverhelper.DriverHelper;
import helpers.IHelpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import properties.Properties;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

public class CommonSteps {

    private CommonMessagesComponent messagesComponent;

    private ModalCommonsComponent modalCommons;

    private TabsComponent tabsComponent;

    private ListComponent listComponent;

    private SearchComponent searchComponent;

    private ButtonRemoveComponent buttonRemove;

    private ModalConfirmationChanges modalConfirmationChanges;

    private DriverHelper driverHelper;

    private static IHelpers iHelpers;

    public CommonSteps(WebDriver driver, Properties properties){
        Injector injector = Guice.createInjector(new AppInjector());

        iHelpers = injector.getInstance(IHelpers.class);

        messagesComponent = new CommonMessagesComponent(driver, properties);

        modalCommons = new ModalCommonsComponent(driver, properties);

        tabsComponent = new TabsComponent(driver, properties);

        listComponent = new ListComponent(driver, properties);

        searchComponent = new SearchComponent(driver, properties);

        buttonRemove = new ButtonRemoveComponent(driver, properties);

        modalConfirmationChanges = new ModalConfirmationChanges(driver, properties);

        driverHelper = new DriverHelper(driver, properties);
    }

    public String getTextRequiredFieldMessage() throws IOException {
        driverHelper.waitElementToBePresent(CommonMessagesComponent.REQUIRED_FIELD_MESSAGE);

        return driverHelper.webElementGetText(messagesComponent.requiredFieldMessage());
    }

    public boolean validateRequiredField() throws IOException {
        driverHelper.waitElementToBePresent(CommonMessagesComponent.REQUIRED_FIELD);

        return driverHelper.validateElementDisplayedAndEnabled(messagesComponent.requiredField());
    }

    public void closeModalIfNecessary(By locator) throws IOException {
        if(driverHelper.createListWebElement(locator).size() > 0){
            driverHelper.actionClick(driverHelper.createWebElement(By.cssSelector("button[aria-label='close']")));
        }
    }

    public String getTextModalFeedbackErrorMessageMinLength3() throws IOException {
        driverHelper.waitElementToBePresent(CommonMessagesComponent.MODAL_FEEDBACK_ERROR_MESSAGE_MIN_LENGTH3);

        return driverHelper.webElementGetText(messagesComponent.modalFeedbackErrorMessageMinLength3());
    }

    public void clickButtonCloseModalDialog() throws IOException {
        driverHelper.actionClick(modalCommons.modalDialogButtonClose());
    }

    public void clickButtonCloseModalDialogPopUp() throws IOException {
        driverHelper.waitElementToBePresent(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CLOSE_ACCEPTANCE_SERVICE_ERROR);

        driverHelper.actionClick(modalCommons.modalDialogButtonCloseAcceptanceServiceError());
    }

    public void clickTabFleet() throws IOException {
        driverHelper.waitElementToBeClickable(TabsComponent.FLEET_TAB);

        driverHelper.actionClick(tabsComponent.fleetTab());
    }

    public void clickTabPoi() throws IOException {
        driverHelper.waitElementToBeClickable(TabsComponent.POI_TAB);

        driverHelper.actionClick(tabsComponent.poiTab());
    }

    public void clickIdentitiesLink() throws IOException {
        driverHelper.waitElementToBeClickable(TabsComponent.IDENTITIES_TAB);

        driverHelper.actionClick(tabsComponent.identitiesTab());
    }

    public void clickTabPoiGroup() throws IOException {
        driverHelper.waitElementToBeClickable(TabsComponent.POI_GROUP_TAB);

        driverHelper.actionClick(tabsComponent.poiGroupTab());
    }

    public void clickButtonCreateInModal() throws IOException {
        driverHelper.actionClick(modalCommons.modalDialogButtonCreate());
    }

    public void clickButtonCancelInModal() throws IOException {
        driverHelper.actionClick(modalCommons.modalDialogButtonCancel());
    }

    public void selectElementInList(String elementName){
        driverHelper.waitElementToBePresent(ListComponent.LIST);

        driverHelper.actionClick(listComponent.listContent().stream().filter(x->x.getText().startsWith(elementName))
                .findFirst().orElseThrow(()-> new NoSuchElementException("The "+elementName+" is not found in list")));
    }

    public void selectElementInList(){
        driverHelper.waitElementToBePresent(ListComponent.LIST);

        driverHelper.actionClick(getFirstElementInList());
    }

    private WebElement getFirstElementInList() {
        return listComponent.listContent().stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("Any element present in list"));
    }

    private WebElement getFirstColumnInList() {
        return listComponent.columns().stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("Any column present in list"));
    }

    private WebElement getTextFirstStoreInList() {
        return listComponent.columns().get(1);
    }

    private WebElement getFirstHeaderColumnInList() {
        return listComponent.listHeaderColumns().stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("Any header column present in list"));
    }

    public String getTextFirstElementInList(){
        return driverHelper.webElementGetText(getFirstElementInList());
    }

    public String getTextFirstListHeader(){
        return driverHelper.webElementGetText(listComponent.listHeader().stream().findFirst().orElseThrow(
                ()-> new NoSuchElementException("Any table found")));
    }

    public String replaceNewLineForSpace(String text){
        return text.replace("\n"," ");
    }

    public String getTextLastListHeader(){
        return driverHelper.webElementGetText(listComponent.listHeader().stream().reduce((first, second) -> second)
                .orElseThrow(()-> new NoSuchElementException("The last table is not found")));
    }

    public String getTextPoiHeader() throws IOException {
        return driverHelper.webElementGetText(listComponent.poiListHeader());
    }

    public String getTextFirstColumnInList() throws IOException {
        driverHelper.waitElementToBePresent(ListComponent.COLUMNS);

        return driverHelper.webElementGetText(listComponent.nameColumn());
    }

    public int getListSize(){
        driverHelper.waitElementToBePresent(ListComponent.LIST);

        return listComponent.list().size();
    }

    public int getListContentSize(){
        driverHelper.waitElementToBePresent(ListComponent.LIST_CONTENT);

        return listComponent.listContent().size();
    }

    public int getRequiredFieldsListSize() {
        driverHelper.waitElementToBePresent(CommonMessagesComponent.REQUIRED_FIELD_MESSAGE);

        return messagesComponent.requiredFieldMessageList().size();
    }

    public String getTextFirstHeaderColumnInList(){
        driverHelper.waitElementToBePresent(ListComponent.LIST_HEADER_COLUMNS);

        return driverHelper.webElementGetText(getFirstHeaderColumnInList());
    }

    public void clickFirstHeaderColumnInList(){
        driverHelper.actionClick(getFirstHeaderColumnInList());
    }

    public void clickDivStore() throws IOException {
        driverHelper.actionClick(driverHelper.createWebElement(By.xpath("//div[text()='Store']")));
    }

    public void clickDivStoreGroup() throws IOException {
        driverHelper.actionClick(driverHelper.createWebElement(By.xpath("//div[text()='Store Group']")));
    }

    public void clickFirstColumnInList() throws InterruptedException, IOException {
        iHelpers.sleepFiveSeconds();

        driverHelper.actionClick(listComponent.nameColumn());
    }

    public boolean validateDateIsBeforeNow(WebElement element) throws ParseException {
        String [] divDate = driverHelper.webElementGetText(element)
                .split("\n");

        String createdDate = divDate[1];

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        Date date = format.parse(createdDate);

        return date.before(Date.from(Instant.now()));
    }

    public boolean validateSearchComponent(String expectedLabel) throws IOException {
        driverHelper.waitElementToBePresent(SearchComponent.BUTTON_SEARCH);

        return driverHelper.validateElementDisplayed(searchComponent.buttonSearch()) &&
                driverHelper.validateDataAttribute(searchComponent.inputSearch(),
                        "placeholder","Research") &&
                driverHelper.webElementGetText(searchComponent.labelSearchOnFleetDetails()).equals(expectedLabel) &&
                driverHelper.validateDataAttribute(searchComponent.buttonSearch(),"disabled", "true");
    }

    public boolean validateSearchComponentOnStoreGroupDetails(String expectedLabel) throws IOException {
        driverHelper.waitElementToBePresent(SearchComponent.BUTTON_SEARCH);

        return driverHelper.validateElementDisplayed(searchComponent.buttonSearch()) &&
                driverHelper.validateDataAttribute(searchComponent.inputSearch(),
                        "placeholder","Research") &&
                driverHelper.webElementGetText(searchComponent.labelSearchOnStoreGroupDetails()).equals(expectedLabel) &&
                driverHelper.validateDataAttribute(searchComponent.buttonSearch(),"disabled", "true");
    }

    public boolean validateButtonDeleteFleetsDisplayed() throws IOException {
        driverHelper.waitElementToBePresent(ButtonRemoveComponent.BUTTON_DELETE_FLEETS);

        return driverHelper.validateElementDisplayed(buttonRemove.buttonDeleteFleets());
    }

    public boolean validateButtonDeleteStoreIsDisplayed() throws IOException {
        driverHelper.waitElementToBePresent(ButtonRemoveComponent.BUTTON_DELETE_STORE);

        return driverHelper.validateElementDisplayed(buttonRemove.buttonDeleteStore());
    }

    public boolean validateButtonDeleteStoreGroupIsDisplayed() throws IOException {
        driverHelper.waitElementToBePresent(ButtonRemoveComponent.BUTTON_DELETE_STORE_GROUP);

        return driverHelper.validateElementDisplayed(buttonRemove.buttonDeleteStoreGroup());
    }

    public boolean validateButtonDeleteStoreGroupIsDisplayedAndEnabled() throws IOException {
        driverHelper.waitElementToBePresent(ButtonRemoveComponent.BUTTON_DELETE_STORE_GROUP);

        return driverHelper.validateElementDisplayedAndEnabled(buttonRemove.buttonDeleteStoreGroup());
    }

    public boolean validateButtonDisplayedAndEnabledWithExpectedText(WebElement element, String expectedText){
        return driverHelper.validateElementDisplayedAndEnabled(element) && driverHelper.webElementGetText(element)
                .equals(expectedText);
    }

    public List<String> getContentFirstList(){
        return driverHelper.getTextFromListWebElements(listComponent.listContentChildElements(listComponent.list() //listContent
                .stream().findFirst().orElseThrow(()-> new NoSuchElementException("Any list found"))));

    }

    public List<String> getContentLastList(){
        return driverHelper.getTextFromListWebElements(listComponent.listContentChildElements(listComponent.list()
                .stream().reduce((first, second) -> second)
                .orElseThrow(()-> new NoSuchElementException("Any list found"))));
    }

    public boolean validateResultsInList(List<String> list, String value1, String value2){
        return list.stream().allMatch(x->x.contains(value1) || x.contains(value2));
    }

    public boolean validateWebElementText(WebElement element, String text){
        return driverHelper.webElementGetText(element).equals(text);
    }

    public boolean validateButtonDeleteFleetNotPresent(){
        return driverHelper.invisibilityElementLocated(ButtonRemoveComponent.BUTTON_DELETE_FLEETS);
    }

    public void clickFirstPoiInList() throws InterruptedException, IOException {
        iHelpers.sleepTwoSeconds();

        driverHelper.actionClick(listComponent.divFirstPoi());
    }

    public void clickConfirmationChangesSpanYes() throws IOException {
        driverHelper.actionClick(modalConfirmationChanges.confirmationChangesSpanYes());
    }

    public void clickConfirmationChangesSpanNo() throws IOException {
        driverHelper.actionClick(modalConfirmationChanges.confirmationChangesSpanNo());
    }

    public void clickConfirmationChangesSpanSave() throws IOException {
        driverHelper.waitElementToBePresent(ModalConfirmationChanges.CONFIRMATION_CHANGES_SPAN_SAVE);

        driverHelper.actionClick(modalConfirmationChanges.confirmationChangesSpanSave());
    }

    public void clickConfirmationChangesSpanCancel() throws IOException {
        driverHelper.waitElementToBePresent(ModalConfirmationChanges.CONFIRMATION_CHANGES_SPAN_CANCEL);

        driverHelper.actionClick(modalConfirmationChanges.confirmationChangesSpanCancel());
    }

    public boolean validateSpanSaveAndCancelNotPresent(){
        return driverHelper.invisibilityElementLocated(ModalConfirmationChanges.CONFIRMATION_CHANGES_SPAN_CANCEL) &&
                driverHelper.invisibilityElementLocated(ModalConfirmationChanges.CONFIRMATION_CHANGES_SPAN_SAVE);
    }

    public boolean validateSpanSaveAndCancelArePresent() throws IOException {
        return driverHelper.validateElementDisplayed(modalConfirmationChanges.confirmationChangesSpanCancel()) &&
                driverHelper.validateElementDisplayed(modalConfirmationChanges.confirmationChangesSpanSave());
    }
    public boolean validateNoRequiredFieldPresent(){
        return driverHelper.invisibilityElementLocated(CommonMessagesComponent.REQUIRED_FIELD_MESSAGE);
    }

    public void clickBreadCrumbElement(String element) throws IOException {
        driverHelper.actionClick(driverHelper.createWebElement(By.xpath("//a[text()='"+element+"']")));
    }

    public void clickOnStore() throws InterruptedException {
        iHelpers.sleepFiveSeconds();

        driverHelper.actionClick(listComponent.columns().get(2));
    }

    public void clickOnButtonClosePoiGroup() throws IOException {
        driverHelper.waitElementToBePresent(ModalCommonsComponent.MODAL_DIALOG_BUTTON_CLOSE_POI_GROUP);

        driverHelper.actionClick(modalCommons.modalDialogButtonClosePoiGroup());
    }

    public String getTextFirstStoreList(){
        driverHelper.waitElementToBePresent(ListComponent.COLUMNS);

        return driverHelper.webElementGetText(getTextFirstStoreInList());
    }

    public void clickOnDivSelectAll() throws IOException {
        driverHelper.waitElementToBePresent(ListComponent.SPAN_SELECT_ALL);

        driverHelper.actionClick(listComponent.spanSelectAll());
    }

    public int numberOfCheckboxesAreSelected() {
        return (int) listComponent.inputCheckboxes().stream().filter(WebElement::isSelected).count();
    }

    public void clickFirstCheckbox() {
        driverHelper.waitElementToBePresent(ListComponent.INPUT_CHECKBOX);

        listComponent.inputCheckboxes().stream().findFirst()
                .orElseThrow(() -> new NoSuchElementException("There is no checkbox in list")).click();
    }

    public void clickSecondCheckbox() {
        driverHelper.waitElementToBePresent(ListComponent.INPUT_CHECKBOX);

        listComponent.inputCheckboxes().get(1).click();
    }

    public boolean validateButtonRemoveIsPresent() throws IOException {
        driverHelper.waitElementToBePresent(ButtonRemoveComponent.BUTTON_REMOVE);

       return driverHelper.validateElementDisplayed(buttonRemove.buttonRemove());
    }

    public String getTextFromPopUp() throws IOException {
        driverHelper.waitElementToBePresent(ModalCommonsComponent.DIV_POP_UP_TEXT);

        return driverHelper.webElementGetText(modalCommons.divPopUpText());
    }
}